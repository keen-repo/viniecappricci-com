<?php
/*
Plugin Name: WooCommerce Endeavour Payment Gateway
Plugin URI: https://www.cardpaydirect.com/
Description: Endeavour Payment gateway for woocommerce
Version: 0.1
Author: Keen
Author URI: http://www.keen.com.mt
*/

add_action('plugins_loaded', 'woocommerce_endeavour_init', 0);

function woocommerce_endeavour_init(){
	
  if(!class_exists('WC_Payment_Gateway')) return;
 
	class WC_Endeavour extends WC_Payment_Gateway{
		
	  public function __construct(){
		$this->id = 'endeavour';
		$this->method_title = 'Endeavour';
		
		$this->has_fields = false;

		$this->init_form_fields();
		$this->init_settings();

		$this->title			= $this->settings['title'];
		$this->description		= $this->settings['description'];
		$this->redirect_page_id = $this->settings['redirect_page_id'];
		$this->liveurl			= 'https://secure.payu.in/_payment';

		$this->msg['message'] = "";
		$this->msg['class'] = "";

		add_action('init', array(&$this, 'check_endeavour_response'));
		
		if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ) {
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
		 } else {
			add_action( 'woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
		}
		
		add_action('woocommerce_receipt_endeavour', array(&$this, 'receipt_page'));
	 }
	 
	 function init_form_fields(){
 
		$this->form_fields = array(
			'enabled' => array(
				'title' => 'Enable/Disable',
				'type' => 'checkbox',
				'label' => 'Enable Endeavour Payment Module.',
				'default' => 'no'
			),
			'title' => array(
				'title' => 'Title:',
				'type'=> 'text',
				'description' => 'This controls the title which the user sees during checkout.',
				'default' => 'Endeavour', 'mrova'
			),
			'description' => array(
				'title' => 'Description:',
				'type' => 'textarea',
				'description' => 'This controls the description which the user sees during checkout.',
				'default' => 'Pay securely by Credit or Debit card.'
			),
			'redirect_page_id' => array(
				'title' => 'Return Page',
				'type' => 'select',
				'options' => $this->get_pages('Select Page'),
				'description' => "URL of success page"
			)
		);
		
    }
 
    public function admin_options(){
        echo '<h3>Endeavour Payment Gateway', 'mrova</h3>';
        echo '<p></p>';
        echo '<table class="form-table">';
			// Generate the HTML For the settings form.
			$this ->generate_settings_html();
        echo '</table>';
 
    }
	
	/**
     *  There are no payment fields for endeavour, but we want to show the description if set.
     **/
    function payment_fields(){
        if($this -> description) echo wpautop(wptexturize($this -> description));
    }
    /**
     * Receipt Page
     **/
    function receipt_page($order){
        echo '<p>Thank you for your order, please fill in the form below to pay with Credit Card or Debit Card.</p>';
        echo $this->generate_endeavour_form($order);
    }
	
	public function generate_endeavour_form($order_id) {
 
        global $woocommerce;
 
        $order = new WC_Order($order_id);
        $txnid = $order_id.'_'.date("ymds");
 
        $redirect_url = 'https://www.viniecapricci.com/endeavour/';
 
        $productinfo = "Order $order_id";
		
		$qs = array(
			'amount'		=> $order->order_total,
			'redirectUrl'	=> $redirect_url,
			'bID'			=> $order_id,
			'name'			=> $order->billing_first_name . ' ' . $order->billing_last_name,
			'email'			=> $order->billing_email,
			'desc'			=> '',
			'TermsURL'		=> 'https://www.viniecapricci.com/terms-conditions/'
		);
		
		$base_url = 'https://cardpaydirect.com/MerchantPages/secure/Keen/Abrahams/index.jsp?';
		
		$payment_gateway_url =  $base_url . http_build_query($qs);
		
		return '<iframe style="width: 620px;height: 430px;" frameborder="0" src="' . $payment_gateway_url . '"></iframe>';
 
		/*
        $str = "$this->merchant_id|$txnid|$order->order_total|$productinfo|$order->billing_first_name|$order->billing_email|||||||||||$this->salt";
        $hash = hash('sha512', $str);
 
        $payu_args = array(
          'key' => $this -> merchant_id,
          'txnid' => $txnid,
          'amount' => $order -> order_total,
          'productinfo' => $productinfo,
          'firstname' => $order -> billing_first_name,
          'lastname' => $order -> billing_last_name,
          'address1' => $order -> billing_address_1,
          'address2' => $order -> billing_address_2,
          'city' => $order -> billing_city,
          'state' => $order -> billing_state,
          'country' => $order -> billing_country,
          'zipcode' => $order -> billing_zip,
          'email' => $order -> billing_email,
          'phone' => $order -> billing_phone,
          'surl' => $redirect_url,
          'furl' => $redirect_url,
          'curl' => $redirect_url,
          'hash' => $hash,
          'pg' => 'NB'
          );
		 *
		 */
 
		
 
 
    }
	
	 /**
     * Process the payment and return the result
     **/
    function process_payment($order_id){
        $order = new WC_Order($order_id);
        return array('result' => 'success', 'redirect' => add_query_arg('order',
            $order->id, add_query_arg('key', $order->order_key, get_permalink(6) . 'order-received/'))
        );
    }
 
    /**
     * Check for valid payu server callback
     **/
    function check_endeavour_response(){
		
		global $woocommerce;
		
		/*
		$remote_addr = $_SERVER['REMOTE_ADDR'];
		
		$identifier = $_REQUEST['identifier'];
		
		$headers = 'From: Developer <developer@keen.com.mt>' . "\r\n";
		wp_mail('developer@keen.com.mt', 'test response', 'running resose', $headers );

		wp_mail('developer@keen.com.mt', 'Request Data', json_encode($_REQUEST), $headers );
		wp_mail('developer@keen.com.mt', 'SERVER DATA', json_encode($_SERVER), $headers );
		
		if (strpos($remote_addr, '213.95.27') !== false) {
			
			$order = new WC_Order($identifier);
			
			$transauthorised = false;
			
			if($order->status !=='completed'){
				 
				if($_REQUEST['AcceptReject'] == 'T'){
					
					$transauthorised = true;
					
					$this->msg['message'] = "Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon.";
					$this->msg['class'] = 'woocommerce_message';
					
					if($order->status == 'processing'){

					}else{
						$order->payment_complete();
						$order->add_order_note('Endeavour payment successful<br/>Unnique Id from Endeavour: '.$_REQUEST['ReceiptCode']);
						$order->add_order_note($this->msg['message']);
						$woocommerce->cart->empty_cart();
					}
				} else {
					$this->msg['class'] = 'woocommerce_error';
					$this->msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
					$order->add_order_note('Transaction Declined');
					//Here you need to put in the routines for a failed
					//transaction such as sending an email to customer
					//setting database status etc etc
				}
				
				 if($transauthorised==false){
					$order->update_status('failed');
					$order->add_order_note('Failed');
					$order->add_order_note($this->msg['message']);
				}
 
			}
				
			wp_mail('developer@keen-advertising.com', 'Server Call', 'booking' . $_REQUEST['Identifier'] . ' ' . $_REQUEST['AcceptReject'] . ' IP ' . $remote_addr, $headers );

		} else {
			
			if($_REQUEST['AcceptReject'] == 'T'){

				$this->msg['message'] = "Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon.";
				$this->msg['class'] = 'woocommerce_message';

			} else {
				$this->msg['class'] = 'woocommerce_error';
				$this->msg['message'] = "Thank you for shopping with us. However, the transaction has been declined.";
				
			}
			
			add_action('the_content', array(&$this, 'showMessage'));
			
		}
		*/
 
    }
	
	function showMessage($content){
		return '<div class="box '.$this->msg['class'].'-box">'.$this->msg['message'].'</div>'.$content;
	}
	
	  // get all pages
    function get_pages($title = false, $indent = true) {
        $wp_pages = get_pages('sort_column=menu_order');
        $page_list = array();
        if ($title) $page_list[] = $title;
        foreach ($wp_pages as $page) {
            $prefix = '';
            // show indented child pages?
            if ($indent) {
                $has_parent = $page->post_parent;
                while($has_parent) {
                    $prefix .=  ' - ';
                    $next_page = get_page($has_parent);
                    $has_parent = $next_page->post_parent;
                }
            }
            // add to page list array array
            $page_list[$page->ID] = $prefix . $page->post_title;
        }
        return $page_list;
    }
	 
  }
   /**
	* Add the Gateway to WooCommerce
	**/
	function woocommerce_add_endeavour_gateway($methods) {
	   $methods[] = 'WC_Endeavour';
	   return $methods;
	}
 
    add_filter('woocommerce_payment_gateways', 'woocommerce_add_endeavour_gateway' );
	
}
?>