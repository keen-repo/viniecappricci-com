<?php
/*
Plugin Name: WooCommerce Malta Regions
Plugin URI: https://www.keen.com/
Description: WooCommerce Malta Regions
Version: 0.1
Author: Keen
Author URI: http://www.keen.com.mt
*/

add_filter('woocommerce_states', 'maltaregion_states');

function maltaregion_states($states) {

	$states['MT'] = array(
		'Malta' => __( 'Malta', 'woocommerce' ),
		'Gozo' => __( 'Gozo', 'woocommerce' ),
	);

	return $states;
	
}
?>