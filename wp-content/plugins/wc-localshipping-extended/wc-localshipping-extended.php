<?php
/*
Plugin Name: Woocommerce Local Delivery Extended
Plugin URI: https://www.keen.com.mt/
Description: Woocommerce Local Delivery Extended
Version: 0.1
Author: Keen
Author URI: http://www.keen.com.mt
*/

/**
 * Local Delivery Extended Shipping Method
 *
 * A simple shipping method allowing local delivery as a shipping method
 *
 * @class 		WC_Shipping_Local_Delivery_Extended
 * @version		2.0.0
 * @package		WooCommerce/Classes/Shipping
 * @author 		WooThemes
 */

/**
* Check if WooCommerce is active
*/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	
	function woocommerce_local_delivery_extended_main() {
		

		if ( ! class_exists( 'WC_Shipping_Local_Delivery_Extended' ) ) {


			class WC_Shipping_Local_Delivery_Extended extends WC_Shipping_Method {

				/**
				 * __construct function.
				 *
				 * @access public
				 * @return void
				 */
				function __construct() {
					$this->id					= 'local_delivery_extended';
					$this->title				= __( 'Local Delivery Extended' );
					$this->method_title			= __( 'Local Delivery Extended' );
					$this->method_description	= __( 'Local Delivery Extended' ); // 
					$this->enabled				= 1; // This can be added as an setting but for this example its forced enabled
					$this->init();
				}

				/**
				 * init function.
				 *
				 * @access public
				 * @return void
				 */
				function init() {

					// Load the settings.
					$this->init_form_fields();
					$this->init_settings();

					// Define user set variables
					$this->enabled	= $this->get_option( 'enabled' );
					$this->title		= $this->get_option( 'title' );
					$this->type 		= $this->get_option( 'type' );
					$this->fee			= $this->get_option( 'fee' );
					$this->type			= $this->get_option( 'type' );
					$this->codes		= $this->get_option( 'codes' );
					$this->availability	= $this->get_option( 'availability' );
					$this->countries	= $this->get_option( 'countries' );

					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
				}

				/**
				 * calculate_shipping function.
				 *
				 * @access public
				 * @param array $package (default: array())
				 * @return void
				 */
				function calculate_shipping( $package = array() ) {
					global $woocommerce;

					$fee = ( trim( $this->fee ) == '' ) ? 0 : $this->fee;
					
					if ( isset( $woocommerce->cart->cart_contents_total ) ) {

						if ( $woocommerce->cart->prices_include_tax )
							$total = $woocommerce->cart->tax_total + $woocommerce->cart->cart_contents_total;
						else
							$total = $woocommerce->cart->cart_contents_total;
						
					}

					/* If total is less than 50 and state is malta or empty */
					// if ($total < 50 && in_array($package['destination']['state'], array('Malta', ''))) {
					if ($total <= 50) {
						
						$shipping_total = $fee;
						
					} else {
						$rateTitle = $this->title . ': FREE';
					}
					
					/* Allow only gift vouchers to have free shipping - logic */
					$product_counts = array(
						'not_gift_voucher' => 0,
						'gift_voucher' => 0
					);
					
					foreach ($woocommerce->cart->cart_contents as $cart_item) {
						
						$pid = $cart_item['product_id'];
						
						$terms = get_the_terms( $pid, 'product_cat' );
						
						$is_gift_voucher = false;
						
						foreach ($terms as $term) {
							
							// is gift voucher free shipping
							if ($term->term_id == 4027) {
								
								$is_gift_voucher = true;

								break;
							}
							
						}
						
						if ($is_gift_voucher === false)
							$product_counts['not_gift_voucher']++;
						else
							$product_counts['gift_voucher']++;
						
					}
					
					if ($product_counts['gift_voucher'] > 0 && $product_counts['not_gift_voucher'] === 0) {
						
						$shipping_total = 0;
						
					}

					$rate = array(
						'id' 		=> $this->id,
						'label' 	=> !isset($rateTitle) ? $this->title : $rateTitle,
						'cost' 		=> $shipping_total
					);

					

					$this->add_rate($rate);
					
				}

				/**
				 * init_form_fields function.
				 *
				 * @access public
				 * @return void
				 */
				function init_form_fields() {
					global $woocommerce;
					$this->form_fields = array(
						'enabled' => array(
							'title' 		=> __( 'Enable', 'woocommerce' ),
							'type' 			=> 'checkbox',
							'label' 		=> __( 'Enable local delivery', 'woocommerce' ),
							'default' 		=> 'no'
						),
						'title' => array(
							'title' 		=> __( 'Title', 'woocommerce' ),
							'type' 			=> 'text',
							'description' 	=> __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
							'default'		=> __( 'Local Delivery', 'woocommerce' ),
							'desc_tip'      => true,
						),
						'type' => array(
							'title' 		=> __( 'Fee Type', 'woocommerce' ),
							'type' 			=> 'select',
							'description' 	=> __( 'How to calculate delivery charges', 'woocommerce' ),
							'default' 		=> 'fixed',
							'options' 		=> array(
								'fixed' 	=> __( 'Fixed amount', 'woocommerce' ),
								'percent'	=> __( 'Percentage of cart total', 'woocommerce' ),
								'product'	=> __( 'Fixed amount per product', 'woocommerce' ),
							),
							'desc_tip'      => true,
						),
						'fee' => array(
							'title' 		=> __( 'Delivery Fee', 'woocommerce' ),
							'type' 			=> 'number',
							'custom_attributes' => array(
								'step'	=> 'any',
								'min'	=> '0'
							),
							'description' 	=> __( 'What fee do you want to charge for local delivery, disregarded if you choose free. Leave blank to disable.', 'woocommerce' ),
							'default'		=> '',
							'desc_tip'      => true,
							'placeholder'	=> '0.00'
						),
						'codes' => array(
							'title' 		=> __( 'Zip/Post Codes', 'woocommerce' ),
							'type' 			=> 'textarea',
							'description' 	=> __( 'What zip/post codes would you like to offer delivery to? Separate codes with a comma. Accepts wildcards, e.g. P* will match a postcode of PE30.', 'woocommerce' ),
							'default'		=> '',
							'desc_tip'      => true,
							'placeholder'	=> '12345, 56789 etc'
						),
						'availability' => array(
										'title' 		=> __( 'Method availability', 'woocommerce' ),
										'type' 			=> 'select',
										'default' 		=> 'all',
										'class'			=> 'availability',
										'options'		=> array(
											'all' 		=> __( 'All allowed countries', 'woocommerce' ),
											'specific' 	=> __( 'Specific Countries', 'woocommerce' )
										)
									),
						'countries' => array(
										'title' 		=> __( 'Specific Countries', 'woocommerce' ),
										'type' 			=> 'multiselect',
										'class'			=> 'chosen_select',
										'css'			=> 'width: 450px;',
										'default' 		=> '',
										'options'		=> $woocommerce->countries->countries
									)
					);
				}

				/**
				 * admin_options function.
				 *
				 * @access public
				 * @return void
				 */
				function admin_options() {
					global $woocommerce; ?>
					<h3><?php echo $this->method_title; ?> </h3> 
					<p><?php _e( 'Local delivery extended is a simple shipping method for delivering orders locally.', 'woocommerce' ); ?></p>
					<table class="form-table">
						<?php $this->generate_settings_html(); ?>
					</table> <?php
				}


				/**
				 * is_available function.
				 *
				 * @access public
				 * @param array $package
				 * @return bool
				 */
				function is_available( $package ) {
					global $woocommerce;



					if ($this->enabled=="no") return false;

					// If post codes are listed, let's use them.
					$codes = '';
					if ( $this->codes != '' ) {
						foreach( explode( ',', $this->codes ) as $code ) {
							$codes[] = $this->clean( $code );
						}
					}

					if ( is_array( $codes ) ) {

						$found_match = false;

						if ( in_array( $this->clean( $package['destination']['postcode'] ), $codes ) )
							$found_match = true;

						// Wildcard search
						if ( ! $found_match ) {

							$customer_postcode = $this->clean( $package['destination']['postcode'] );
							$customer_postcode_length = strlen( $customer_postcode );

							for ( $i = 0; $i <= $customer_postcode_length; $i++ ) {

								if ( in_array( $customer_postcode, $codes ) )
									$found_match = true;

								$customer_postcode = substr( $customer_postcode, 0, -2 ) . '*';
							}
						}

						if ( ! $found_match )
							return false;
					}

					// Either post codes not setup, or post codes are in array... so lefts check countries for backwards compatibility.
					$ship_to_countries = '';
					if ($this->availability == 'specific') :
						$ship_to_countries = $this->countries;
					else :
						if (get_option('woocommerce_allowed_countries')=='specific') :
							$ship_to_countries = get_option('woocommerce_specific_allowed_countries');
						endif;
					endif;

					if (is_array($ship_to_countries))
						if (!in_array( $package['destination']['country'] , $ship_to_countries))
							return false;

					// Yay! We passed!
					return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true );
				}


				/**
				 * clean function.
				 *
				 * @access public
				 * @param mixed $code
				 * @return string
				 */
				function clean( $code ) {
					return str_replace( '-', '', sanitize_title( $code ) ) . ( strstr( $code, '*' ) ? '*' : '' );
				}

			}

		}
		
	}
	
	add_action( 'woocommerce_shipping_init', 'woocommerce_local_delivery_extended_main' );
	
	function add_local_delivery_extended_method( $methods ) {
		$methods[] = 'WC_Shipping_Local_Delivery_Extended'; 
		return $methods;
	}

	add_filter( 'woocommerce_shipping_methods', 'add_local_delivery_extended_method' );
	
}



