<?php 
/**
 * Register Custom Post Type Partner
 * Key: partners
 */
function viniecappricci_cpt_partners() {

	$labels = array(
		'name'                  => _x( 'Partners', 'Post Type General Name', THEMELANG ),
		'singular_name'         => _x( 'Partner', 'Post Type Singular Name', THEMELANG ),
		'menu_name'             => __( 'Partners', THEMELANG ),
		'name_admin_bar'        => __( 'Partners', THEMELANG ),
		'archives'              => __( 'Partners Archives', THEMELANG ),
		'parent_item_colon'     => __( 'Parent Item:', THEMELANG ),
		'all_items'             => __( 'All Partners', THEMELANG ),
		'add_new_item'          => __( 'Add New Partner', THEMELANG ),
		'add_new'               => __( 'Add New Partner', THEMELANG ),
		'new_item'              => __( 'New Partner', THEMELANG ),
		'edit_item'             => __( 'Edit Partner', THEMELANG ),
		'update_item'           => __( 'Update Partner', THEMELANG ),
		'view_item'             => __( 'View Partner', THEMELANG ),
		'search_items'          => __( 'Search Partners', THEMELANG ),
		'not_found'             => __( 'Not found', THEMELANG ),
		'not_found_in_trash'    => __( 'Not found in Trash', THEMELANG ),
		'featured_image'        => __( 'Featured Image', THEMELANG ),
		'set_featured_image'    => __( 'Set featured image', THEMELANG ),
		'remove_featured_image' => __( 'Remove featured image', THEMELANG ),
		'use_featured_image'    => __( 'Use as featured image', THEMELANG ),
		'insert_into_item'      => __( 'Insert into item', THEMELANG ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', THEMELANG ),
		'items_list'            => __( 'Items list', THEMELANG ),
		'items_list_navigation' => __( 'Items list navigation', THEMELANG ),
		'filter_items_list'     => __( 'Filter items list', THEMELANG ),
	);
	$args = array(
		'label'                 => __( 'Partner', THEMELANG ),
		'description'           => __( 'Partners product and location', THEMELANG ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'taxonomies'            => array( ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'partners', $args );

}
add_action( 'init', 'viniecappricci_cpt_partners', 0 );


// Register Custom Taxonomy
add_action( 'init', 'viniecappricci_custom_tax_reg_ab_partner_cats', 0 );
function viniecappricci_custom_tax_reg_ab_partner_cats() {

	$labels = array(
		'name'                       => _x( 'Ranges', 'Taxonomy General Name', THEMELANG ),
		'singular_name'              => _x( 'Range', 'Taxonomy Singular Name', THEMELANG ),
		'menu_name'                  => __( 'Range', THEMELANG ),
		'all_items'                  => __( 'All Items', THEMELANG ),
		'parent_item'                => __( 'Parent Item', THEMELANG ),
		'parent_item_colon'          => __( 'Parent Item:', THEMELANG ),
		'new_item_name'              => __( 'New Item Name', THEMELANG ),
		'add_new_item'               => __( 'Add New Item', THEMELANG ),
		'edit_item'                  => __( 'Edit Item', THEMELANG ),
		'update_item'                => __( 'Update Item', THEMELANG ),
		'view_item'                  => __( 'View Item', THEMELANG ),
		'separate_items_with_commas' => __( 'Separate items with commas', THEMELANG ),
		'add_or_remove_items'        => __( 'Add or remove items', THEMELANG ),
		'choose_from_most_used'      => __( 'Choose from the most used', THEMELANG ),
		'popular_items'              => __( 'Popular Items', THEMELANG ),
		'search_items'               => __( 'Search Items', THEMELANG ),
		'not_found'                  => __( 'Not Found', THEMELANG ),
		'no_terms'                   => __( 'No items', THEMELANG ),
		'items_list'                 => __( 'Items list', THEMELANG ),
		'items_list_navigation'      => __( 'Items list navigation', THEMELANG ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'					 => array( 'slug' => 'ab_partner_cats' , 'with_front' => false )
	);
	register_taxonomy( 'ab_partner_cats', array( 'partners' ), $args );

}
