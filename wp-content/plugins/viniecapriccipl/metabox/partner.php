<?php
/*
 * Our Partners page
 */

add_filter( 'rwmb_meta_boxes', 'mb_partner' );
if ( !function_exists( 'mb_partner ' ) ) {
	function mb_partner( $meta_boxes )
	{

		$prefix = 'mb_partner_';
		$meta_boxes[] = array(
		'title'      => __( 'Partners', THEMELANG ),
		// Register this meta box for posts matched below conditions
        // 'include' => array(
        //     'relation'        => 'OR',
        //     'slug'            => array( 'partners'),
        //     // List of page templates. Can be array or comma separated. Optional.
        //     // 'template'        => array( 'page-gozo.php' ),
        //     // Custom condition. Optional.
        //     // Format: 'custom' => 'callback_function'
        //     // The function will take 1 parameter which is the meta box itself
        //     'custom'          => 'manual_include',
        // ),
		'post_types' => array( 'partners' ),
		'autosave' => true,
		'fields' => array(
	            array(
					'id'   => $prefix.'url',
					'name' => __( 'Website', THEMELANG ),
					'type' => 'url',
					'size' => 60
					
				),
				array(
					'id'   => $prefix.'product_link',
					'name' => __( 'Product Page Link', THEMELANG ),
					'type' => 'url',
					'size' => 60
					
				),
	            array(
	                'id'   => $prefix.'address',
	                'name' => __( 'Address', THEMELANG ),
	                'type' => 'text',
	                'size'    => 50,
	                'std'  => __( 'Gozitano Agricultural Village, Mgarr Road, Xewkija, Gozo', THEMELANG ),
	            ),
	            array(
	                'id'            => $prefix.'map',
	                // 'name'          => __( 'Map Location', THEMELANG ),
	                'type'          => 'map',
	                // Default location: 'latitude,longitude[,zoom]' (zoom is optional)
	                'std'           => '36.03672, 14.26183',
	                // Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
	                'address_field' => $prefix.'address',
	                'api_key'      => 'AIzaSyB-IR0wNdQ7AD7G7k96ww9CE0iXMd7C4KE',
	            ),
			),
		);
		return $meta_boxes;
	}
}