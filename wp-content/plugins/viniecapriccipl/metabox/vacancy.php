<?php
/*
 * Our Partners page
 */

add_filter( 'rwmb_meta_boxes', 'mb_vacancy' );
if ( !function_exists( 'mb_vacancy ' ) ) {
	function mb_vacancy( $meta_boxes )
	{

		$prefix = 'mb_vacancy_';
		$meta_boxes[] = array(
		'title'      => __( 'Additional Info', THEMELANG ),
		'post_types' => array( 'vacancy' ),
		'autosave' => true,
		'fields' => array(
	            array(
					'id'   => $prefix.'description',
					// 'name' => __( 'Website', THEMELANG ),
					'type' => 'text',
					'size' => 60
					
				),
				
			),
		);
		return $meta_boxes;
	}
}