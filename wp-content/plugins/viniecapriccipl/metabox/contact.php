<?php 
/*
 * Contact Numbers and Map Location Metaboxes
 */

add_filter( 'rwmb_meta_boxes', 'mb_contact_info' );

if ( !function_exists( 'mb_contact_info' )) {
	function mb_contact_info( $meta_boxes )
	{
		$prefix = 'mb_contact_';
		$meta_boxes[] = array(
			'title'  => __( 'Contact Information', THEMELANG ),
			// Register this meta box for posts matched below conditions
	        'include' => array(
	            'relation'        => 'OR',
	            'slug'            => array( 'contact-us'),
	            // List of page templates. Can be array or comma separated. Optional.
	            // 'template'        => array( 'page-contact.php' ),
	            // Custom condition. Optional.
	            // Format: 'custom' => 'callback_function'
	            // The function will take 1 parameter which is the meta box itself
	            'custom'          => 'manual_include',
	        ),
			'post_types' => array( 'page' ),
	        'autosave' => true,
			'fields' => array(
				 // Address
	            array(
					'id'     => $prefix . 'info',
					// Group field
					'type'   => 'group',
					// Clone whole group?
					'clone'  => false,
					// Drag and drop clones to reorder them?
					'sort_clone' => true,
					// Sub-fields
					'fields' => array(
						 array(
			                'name'  => __( 'Company Name', THEMELANG ),
			                'id'    => $prefix.'companyname',
			                'placeholder' => __( 'Company Name', THEMELANG ),
			                'type'  => 'text',

			            ),
					 	//Contact Number
			            array(
			                'name'  => __( 'Contact Number', THEMELANG ),
			                'id'    => $prefix.'telno',
			                'placeholder' => __( 'Telephone Number', THEMELANG ),
			                'type'  => 'text',

			            ),
			            array(
			                'name'  => __( 'E-mail', THEMELANG ),
			                'id'    => $prefix.'email',
			                'type'  => 'email',

			            ),
					),
				),
			),
		);
		return $meta_boxes;
	}
}

add_filter( 'rwmb_meta_boxes', 'mb_map' );

if ( !function_exists( 'mb_map' )) {
		function mb_map( $meta_boxes )
		{
			$prefix = 'mb_contact_';
			$meta_boxes[] = array(
				'title'  => __( 'Address', THEMELANG ),
				// Register this meta box for posts matched below conditions
		        'include' => array(
		            'relation'        => 'OR',
		            'slug'            => array( 'contact-us'),
		            // List of page templates. Can be array or comma separated. Optional.
		            // 'template'        => array( 'page-contact.php' ),
		            // Custom condition. Optional.
		            // Format: 'custom' => 'callback_function'
		            // The function will take 1 parameter which is the meta box itself
		            'custom'          => 'manual_include',
		        ),
				'post_types' => array( 'page' ),
		        'autosave' => true,
				'fields' => array(
					array(
		                'id'   => $prefix.'address',
		                // 'name' => __( 'Address', THEMELANG ),
		                'type' => 'text',
		                'size'    => 50,
		                'std'  => __( 'Gozitano Agricultural Village, Mgarr Road, Xewkija, Gozo', THEMELANG ),
		            ),
		            array(
		                'id'            => $prefix.'map',
		                // 'name'          => __( 'Map Location', THEMELANG ),
		                'type'          => 'map',
		                // Default location: 'latitude,longitude[,zoom]' (zoom is optional)
		                'std'           => '36.03672, 14.26183',
		                // Name of text field where address is entered. Can be list of text fields, separated by commas (for ex. city, state)
		                'address_field' => $prefix.'address',
		                'api_key'      => 'AIzaSyB-IR0wNdQ7AD7G7k96ww9CE0iXMd7C4KE',
		            ),
				),
			);
			return $meta_boxes;
		}
	}
