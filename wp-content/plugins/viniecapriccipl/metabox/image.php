<?php
/*
 * Image Metaboxes
 */

//Featured Image
add_filter( 'rwmb_meta_boxes', 'mb_slider' );
if ( !function_exists( 'mb_slider ' ) ) {
	function mb_slider( $meta_boxes )
	{

		$prefix = 'mb_banner_';
		$meta_boxes[] = array(
		'title'      => __( 'Page Banner', THEMELANG ),
		// Register this meta box for posts matched below conditions
        // 'include' => array(
        //     'relation'        => 'OR',
        //     // 'slug'            => array( 'hosting'),
        //     // List of page templates. Can be array or comma separated. Optional.
        //     // 'template'        => array( 'page-gozo.php' ),
        //     // Custom condition. Optional.
        //     // Format: 'custom' => 'callback_function'
        //     // The function will take 1 parameter which is the meta box itself
        //     'custom'          => 'manual_include',
        // ),
		'post_types' => array( 'page' ),
		'autosave' => true,
		'fields' => array(
			array(
				'id'     => $prefix . 'images',
				// Group field
				'type'   => 'group',
				// Clone whole group?
				'clone'  => true,
				// Drag and drop clones to reorder them?
				'sort_clone' => true,
				// Sub-fields
				'fields' => array(
						array(
					        'id'   => $prefix .'image',
					        'name' => __( 'Upload Image', THEMELANG ),
					        // 'clone'   => true,
					        'type' => 'image_advanced',
					        // Delete image from Media Library when remove it from post meta?
					        // Note: it might affect other posts if you use same image for multiple posts
					        'force_delete'     => false,
					        // Maximum image uploads
					        'max_file_uploads' => 1,
					    ),
					    array(
			                'name'  => __( 'Text', THEMELANG ),
			                'id'    => $prefix.'text',
			                'type'  => 'textarea'
						),
			            array(
							'id'   => $prefix.'url',
							'name' => __( 'Button 1: Link to', THEMELANG ),
							'type' => 'url',
							'size' => 50
							
						),
						array(
			                'name'  => __( 'Button 1: Link Label', THEMELANG ),
			                'id'    => $prefix.'btn_label',
			                'type'  => 'text'
						),
			            array(
							'id'   => $prefix.'url_2',
							'name' => __( 'Button 2: Link to', THEMELANG ),
							'type' => 'url',
							'size' => 50
							
						),
						array(
			                'name'  => __( 'Button 2: Link Label', THEMELANG ),
			                'id'    => $prefix.'btn_label_2',
			                'type'  => 'text'
			            ),
					),
				)
			)
		);
		return $meta_boxes;
	}
}


//Post Banner
add_filter( 'rwmb_meta_boxes', 'mb_banner_post' );
if ( !function_exists( 'mb_banner_post ' ) ) {
	function mb_banner_post( $meta_boxes )
	{

		$prefix = 'mb_banner_post_';
		$meta_boxes[] = array(
		'title'      => __( 'Page Banner', THEMELANG ),
		// Register this meta box for posts matched below conditions
        // 'include' => array(
        //     'relation'        => 'OR',
        //     // 'slug'            => array( 'hosting'),
        //     // List of page templates. Can be array or comma separated. Optional.
        //     // 'template'        => array( 'page-gozo.php' ),
        //     // Custom condition. Optional.
        //     // Format: 'custom' => 'callback_function'
        //     // The function will take 1 parameter which is the meta box itself
        //     'custom'          => 'manual_include',
        // ),
		'post_types' => array( 'post','event' ),
		'autosave' => true,
		'fields' => array(
			array(
				'id'     => $prefix . 'images',
				// Group field
				'type'   => 'group',
				// Clone whole group?
				'clone'  => false,
				// Drag and drop clones to reorder them?
				'sort_clone' => true,
				// Sub-fields
				'fields' => array(
						array(
					        'id'   => $prefix .'image',
					        'name' => __( 'Upload Image', THEMELANG ),
					        // 'clone'   => true,
					        'type' => 'image_advanced',
					        // Delete image from Media Library when remove it from post meta?
					        // Note: it might affect other posts if you use same image for multiple posts
					        'force_delete'     => false,
					        // Maximum image uploads
					        'max_file_uploads' => 1,
					    ),
					),
				)
			)
		);


		//Virtual Tour Icon
		$meta_boxes[] = array (
			'title' => esc_html__( 'Virtual Tour Icon', 'vini' ),
			'id' => 'virtual-tour-icon',
			'post_types' => array(
				0 => 'page',
			),
			'context' => 'normal',
			'priority' => 'high',
			'fields' => array(
				array (
					'id' =>  'concept_icon',
					'type' => 'image_upload',
					'name' => esc_html__( 'Image icon', 'vini' ),
					'max_status' => false,
					'max_file_uploads' => 1,
				),
				array(
					'id' => 'concept_label',
					'type' => 'text',
					'name' => esc_html__( 'Label', 'vini' ),
				),
				array(
					'id' => 'concept_url',
					'type' => 'url',
					'name' => esc_html__( 'URL', 'vini' ),
				),
			),
			'text_domain' => 'vini',
			'include' => array(
				'relation' => 'OR',
				'ID' => array(
					0 => 67127,
				),
			),
		);

		return $meta_boxes;
	}
}

