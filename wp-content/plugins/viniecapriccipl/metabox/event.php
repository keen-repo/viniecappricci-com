<?php

add_action( 'wp_ajax_vini_add_event_to_cart', 'vini_add_event_to_cart' );
add_action('wp_ajax_nopriv_vini_add_event_to_cart', 'vini_add_event_to_cart');

function vini_add_event_to_cart() {
	global $woocommerce;
	$product_id = $_POST['product_id'];	
	$quantity = $_POST['quantity'];	

	if ( WC()->cart->add_to_cart( $product_id, $quantity) !== false ){
		echo json_encode(true);
		exit;
	}
	else{
		wc_clear_notices();
		echo json_encode(false);
		exit;
	}
}

add_filter( 'rwmb_meta_boxes', 'mb_events' );
if ( !function_exists( 'mb_events ' ) ) {
	function mb_events( $meta_boxes )
	{

		$options2   = event_listSold();		
		$pending    = event_listPending();		
		$cancelled  = event_listCancelled();		
	
		$prefix = 'mb_event_';
		$meta_boxes[] = array(
		'title'      => __( 'Event Information', THEMELANG ),
		// Register this meta box for posts matched below conditions
        // 'include' => array(
        //     'relation'        => 'OR',
        //     'slug'            => array( 'partners'),
        //     // List of page templates. Can be array or comma separated. Optional.
        //     // 'template'        => array( 'page-gozo.php' ),
        //     // Custom condition. Optional.
        //     // Format: 'custom' => 'callback_function'
        //     // The function will take 1 parameter which is the meta box itself
        //     'custom'          => 'manual_include',
        // ),
		'post_types' => array( 'event' ),
		'autosave' => true,
		'fields' => array(
				array(
					'name' => __( 'Event From Date', THEMELANG ),
					'id'   => $prefix . 'date',
					'type' => 'date',
					// jQuery date picker options. See here http://jqueryui.com/demos/datepicker
					'js_options' => array(
						'appendText'      => __( '(dd-mm-yyyy)', THEMELANG ),
						'autoSize'        => true,
						'buttonText'      => __( 'Select start date', THEMELANG ),
						'dateFormat'      => __( 'dd-mm-yy', THEMELANG ),
						'numberOfMonths'  => 1,
						'showButtonPanel' => true,
						'autoclose'=> true,
					),
					'inline' => false,
				),

				array(
					'name' => __( 'Event End Date', THEMELANG ),
					'id'   => $prefix . 'end_date',
					'type' => 'date',
					// jQuery date picker options. See here http://jqueryui.com/demos/datepicker
					'js_options' => array(
						'appendText'      => __( '(dd-mm-yyyy)', THEMELANG ),
						'autoSize'        => true,
						'buttonText'      => __( 'Select end date', THEMELANG ),
						'dateFormat'      => __( 'dd-mm-yy', THEMELANG ),
						'numberOfMonths'  => 1,
						'showButtonPanel' => true,
						'autoclose'=> true,
					),
					'inline' => false,
				),

				array(
					'name' => __( 'Start Time', THEMELANG ),
					'id'   => $prefix . 'start_time',
					'type' => 'time',
					// jQuery datetime picker options. See here http://trentrichardson.com/examples/timepicker/
					'js_options' => array(
						'stepMinute' => 5,
						'showSecond' => false,
						'stepSecond' => 10,
					),
				),

				array(
					'name' => __( 'End Time', THEMELANG ),
					'id'   => $prefix . 'end_time',
					'type' => 'time',
					// jQuery datetime picker options. See here http://trentrichardson.com/examples/timepicker/
					'js_options' => array(
						'stepMinute' => 5,
						'showSecond' => false,
						'stepSecond' => 10,
					),
				),

				array(
					'id' => $prefix . 'available_days',
					'name' => esc_html__( 'Day(s) of Event', '_s' ),
					'type' => 'checkbox_list',
					'options' => array(
						'monday' => esc_html__( 'Monday', '_s' ),
						'tuesday' => esc_html__( 'Tuesday', '_s' ),
						'wednesday' => esc_html__( 'Wednesday', '_s' ),
						'thursday' => esc_html__( 'Thursday', '_s' ),
						'friday' => esc_html__( 'Friday', '_s' ),
						'saturday' => esc_html__( 'Saturday', '_s' ),
						'sunday' => esc_html__( 'Sunday', '_s' ),
					),
				),



				array(
					'name'    => __( 'Sell ticket for this event?', THEMELANG ),
					'id'      => $prefix.'sell',
					'type'    => 'radio',
					// Array of 'value' => 'Label' pairs for radio options.
					// Note: the 'value' is stored in meta field, not the 'Label'
					'options' => array(
						'1' => __( 'Yes', THEMELANG ),
						'0' => __( 'No', THEMELANG ),
					),
				),
 				array(
					'id'          => $prefix.'stock',
					'name'        => __( 'Max stock of tickets (sold + not sold)', THEMELANG),
					'type'        => 'number',					
					'step'        => '1',					
					'min'         => 0,					
					'placeholder' => __( 'Enter number:', THEMELANG ),
				),				
 				array(
		                'id'     => 'eprices',
						'name'        => __( 'TICKET DETAILS', THEMELANG),
		                'type'   => 'group',
		                'clone'  => true,
		                'sort_clone' => true,
		                'fields' => array(
		                	array(
								'id'          => 'mb_event_price_desc',
								'name'        => __( 'Description', THEMELANG),
								'type'        => 'float',	
								'min'         =>  0,					
								'placeholder' => __( '', THEMELANG ),
								'required' => true, 
							),
	                		array(
								'id'          => 'mb_event_price',
								'name'        => __( 'Price €', THEMELANG),
								'type'        => 'float',	
								'min'         =>  0,					
								'placeholder' => __( '', THEMELANG ),
							), 	
		                ),
		            ), 	
				array(
						'id'   => $prefix.'event_product_html',
						'name' => __( 'Event on Woocommerce ', THEMELANG),						
						'type' => 'custom_html',
						'std'  => event_listEvents(),	
				),				
			)
		);
		
		$meta_boxes[] = array(
			'title'      => __( 'Tickets Sold ('.$options2['qty'].')', THEMELANG ),
			'post_types' => array( 'event' ),
			'fields' => array(
							array(
									'id'   => 'custom_html',								
									'type' => 'custom_html',
									'std'  => mb_htmlSold($options2),									
									// 'callback' => 'display_warning',
							),
						)	
		);

		$meta_boxes[] = array(
			'title'      => __( 'Tickets Pending ('.$pending['qty'].')', THEMELANG ),
			'post_types' => array( 'event' ),
			'fields' => array(
							array(
									'id'   => 'custom_html',								
									'type' => 'custom_html',
									'std'  => mb_htmlSold($pending),									
									
							),
						)	
		);

		$meta_boxes[] = array(
			'title'      => __( 'Tickets Cancelled ('.$cancelled['qty'].')', THEMELANG ),
			'post_types' => array( 'event' ),
			'fields' => array(
							array(
									'id'   => 'custom_html',								
									'type' => 'custom_html',
									'std'  => mb_htmlSold($cancelled),									
									
							),
						)	
		);	

		
		return $meta_boxes;
	}
	
	
	/* THESE FUNCTIONS ALLOWS TO SHOW THE EVENTS PRODUCTS OF WOOCOMMERCE */
 	function event_listEvents(){
        if ( isset( $_GET['post'] ) ){
			$post_meta = get_post_meta( $_GET['post'] );
			$product_id = $post_meta[ 'mb_event_product' ]; 
			$args = array( 'p' => $product_id[0], 'post_type' => 'product');
			$products = new WP_Query( $args );
			$products = ($products->posts);
			//$options = array();		
			foreach ($products as $product){			
				$stock = get_post_meta( $product->ID, '_stock', true );
				$stock = round($stock);
				
				//$options[$product->ID] = ucfirst($product->post_title.' ('.$stock.')');
				//$options = $product->ID.' '.ucfirst($product->post_title.' ('.$stock.')');;
			}
			$options = $post_meta[ 'mb_event_product' ][0];
			return $options;
		}	
	}

	/* THESE FUNCTIONS SHOW THE CLIENTS AND THE NUMBER OF TICKETS SOLD*/
 	function event_listSold(){
        if ( isset( $_GET['post'] ) ){
			$post_meta = get_post_meta( $_GET['post'] );
			
			$product_id = $post_meta[ 'mb_event_product' ]; 
 			$args = array( 
				'post_type'   => 'shop_order',
				'post_status' => array('wc-completed', 'wc-processing'),
			);
			$productsSold = new WP_Query( $args ) ;			
			$productsSold = $productsSold->posts;				
	
			$options = array();	
			$cantidad =	0;		
			foreach ($productsSold as $order){			
				$order2 =  wc_get_order( $order->ID );								
				$items = $order2->get_items();						
				foreach ($items as $item){					 
					if ($item['item_meta']['_product_id'] == $product_id){			
						$options['solds'][] = $order2->shipping_first_name.' '.$order2->shipping_last_name.' - '.$order2->shipping_address_1.' - '.$order2->shipping_city.' ('.$order2->shipping_postcode.') '.$order2->shipping_country.' / '.$order2->order_date.' ('.$item['item_meta']['_qty'][0].' ticket/s bought) <a href="'.get_edit_post_link($order->ID).'" target="_blank">See order </a>';	
						$cantidad = $cantidad + $item['item_meta']['_qty'][0];
					}					
				}
								
			}
			$options['qty'] = $cantidad;
			return $options;
		}	
	}	
	
	
	/* THESE FUNCTIONS SHOW THE CLIENTS AND THE NUMBER OF TICKETS SOLD*/
 	function event_listPending(){
        if ( isset( $_GET['post'] ) ){
			$post_meta = get_post_meta( $_GET['post'] );
			
			$product_id = $post_meta[ 'mb_event_product' ]; 
 			$args = array( 
				'post_type'   => 'shop_order',
				'post_status' => array('wc-pending'),
			);
			$productsSold = new WP_Query( $args ) ;			
			$productsSold = $productsSold->posts;				
	
			$options = array();	
			$cantidad =	0;		
			foreach ($productsSold as $order){			
				$order2 =  wc_get_order( $order->ID );								
				$items = $order2->get_items();						
				foreach ($items as $item){					 
					if ($item['item_meta']['_product_id'] == $product_id){			
						$options['solds'][] = $order2->shipping_first_name.' '.$order2->shipping_last_name.' - '.$order2->shipping_address_1.' - '.$order2->shipping_city.' ('.$order2->shipping_postcode.') '.$order2->shipping_country.' / '.$order2->order_date.' ('.$item['item_meta']['_qty'][0].' ticket/s pending of payment) <a href="'.get_edit_post_link($order->ID).'" target="_blank">See order </a>';	
						$cantidad = $cantidad + $item['item_meta']['_qty'][0];
					}					
				}
								
			}
			$options['qty'] = $cantidad;
			return $options;
		}	
	}



	/* THESE FUNCTIONS SHOW THE CLIENTS AND THE NUMBER OF TICKETS SOLD*/
 	function event_listCancelled(){
        if ( isset( $_GET['post'] ) ){
			$post_meta = get_post_meta( $_GET['post'] );
			
			$product_id = $post_meta[ 'mb_event_product' ]; 
 			$args = array( 
				'post_type'   => 'shop_order',
				'post_status' => array('wc-cancelled', 'wc-refunded', 'wc-failed'),
			);
			$productsCancelled = new WP_Query( $args ) ;			
			$productsCancelled = $productsCancelled->posts;				
	
			$options = array();	
			$cantidad =	0;		
			foreach ($productsCancelled as $order){			
				$order2 =  wc_get_order( $order->ID );								
				$items = $order2->get_items();						
				foreach ($items as $item){					 
					if ($item['item_meta']['_product_id'] == $product_id){			
						$options['solds'][] = $order2->shipping_first_name.' '.$order2->shipping_last_name.' - '.$order2->shipping_address_1.' - '.$order2->shipping_city.' ('.$order2->shipping_postcode.') '.$order2->shipping_country.' / '.$order2->order_date.' ('.$item['item_meta']['_qty'][0].' ticket/s cancelled) <a href="'.get_edit_post_link($order->ID).'" target="_blank">See order </a>';	
						$cantidad = $cantidad + $item['item_meta']['_qty'][0];
					}					
				}
								
			}
			$options['qty'] = $cantidad;
			return $options;
		}	
	}



	/* THESE FUNCTIONS SHOW THE CLIENTS AND THE NUMBER OF TICKETS SOLD*/
 	function soldProduct($id){     

			$product_id = $id; 
			$args = array( 
					'post_type' => 'shop_order',
                    'post_status' => array('wc-completed', 'wc-processing')
					);
			$productsSold = new WP_Query( $args );			
			$productsSold = $productsSold->posts;
			
			$options = array();	
			$cantidad =	0;		
			foreach ($productsSold as $order){			
				$order2 =  wc_get_order( $order->ID );
				$orderDetails = get_post_meta( $order->ID );				
				$items = $order2->get_items();						
				foreach ($items as $item){		
					if  ( $item['item_meta']['_product_id'][0] == $product_id ){	
						
						$cantidad = $cantidad + $item['item_meta']['_qty'][0];						
					}						
				}
								
			}
			$options = $cantidad;
			return $options;		
	}
	
	
	
	/* WE CREATE AN EVENT AUTOMATICALLY WHEN WE SAVE AN EVENT */	
	add_action('save_post_event','mb_save_event_post_callback', 11);
	
	function mb_save_event_post_callback( $post_id ){	
			$post = get_post( $post_id ); 
			
			$post_meta = get_post_meta( $post_id );	
		
			$user = wp_get_current_user();	
			
			 if ( !current_user_can( 'edit_post', $licenseID ) ) return;

			 if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

			 if (defined('DOING_AJAX') && DOING_AJAX) return;
			
			//Create product in Woocommerce only if is creating
			
			//CHECK IF PRICE IS SINGLE/MULTIPLE
			$prices = get_post_meta( $post->ID , 'eprices');
			
		 	if  (!isset( $post_meta["mb_event_product"] ) ){ //New Product	 
				
				$post_data = array(
					'post_author' => $user->id,
					'post_content' => $post->post_content,
					'post_status' => "publish",
					'post_title' => $post->post_title,
					'post_parent' => '',
					'post_type' => "product",
				);


				if (count($prices[0]) <= 1) {
					//INSERT POST
					$post_id2 = wp_insert_post( $post_data, $wp_error );

					if($post_id2){
						$attach_id = get_post_meta($post->ID, "mb_banner_post_images", true);add_post_meta($post_id2, '_thumbnail_id', $attach_id["mb_banner_post_image"][0]);
					}

					//SET PRODUCT CATEGORY
					wp_set_object_terms( $post_id2, 'events', 'product_cat' );
					//SET PRODUCT TYPE
					wp_set_object_terms($post_id2, 'simple', 'product_type');

					update_post_meta( $post_id2, '_visibility', 'hidden' );
					update_post_meta( $post_id2, '_stock_status', 'instock');
					update_post_meta( $post_id2, 'total_sales', '0');
					update_post_meta( $post_id2, '_downloadable', 'yes');
					update_post_meta( $post_id2, '_virtual', 'yes');
					update_post_meta( $post_id2, '_regular_price', floatval( $prices[0][0]['mb_event_price'] ) );
					update_post_meta( $post_id2, '_sale_price', "" );
					update_post_meta( $post_id2, '_purchase_note', "" );
					update_post_meta( $post_id2, '_featured', "no" );
					update_post_meta( $post_id2, '_weight', "" );
					update_post_meta( $post_id2, '_length', "" );
					update_post_meta( $post_id2, '_width', "" );
					update_post_meta( $post_id2, '_height', "" );
					update_post_meta( $post_id2, '_sku', "");
					update_post_meta( $post_id2, '_product_attributes', array());
					update_post_meta( $post_id2, '_sale_price_dates_from', "" );
					update_post_meta( $post_id2, '_sale_price_dates_to', "" );
					update_post_meta( $post_id2, '_price', floatval( $prices[0][0]['mb_event_price'] ) );
					update_post_meta( $post_id2, '_sold_individually', "" );
					update_post_meta( $post_id2, '_manage_stock', "yes" );
					update_post_meta( $post_id2, '_backorders', "no" );
					update_post_meta( $post_id2, '_stock', (int)$post_meta['mb_event_stock'][0] );

				/* ADD WOOCOMMERCE ID AS EVENT META */
				add_post_meta( $post_id, 'mb_event_product', $post_id2 );	

				}else{
					$post_data['event_id'] = $post->ID;
					$post_data['categories'] = 'events';
					$post_data['type'] = 'variable';
					$post_data['stock_qty'] = (int)$post_meta['mb_event_stock'][0];
					$post_data['stock_status'] = 'instock';

					// INSERT EVENT RATES ATTRIBUTES
					$attributes = array( 'event-rates');
					$post_data['available_attributes'] = $attributes;

					//VARIATIONS DATA
					$variations = array();
					foreach ($prices[0] as $key => $data) {

						$variations[] = array(
							"regular_price"=>$data['mb_event_price'],
							"price"=>$data['mb_event_price'],
							"sku"=> '00' . $key,
							"attributes" => array("event-rates"=>$data['mb_event_price_desc']),
							"manage_stock" =>"no",
							"stock_quantity"=> "",
						);
					}

					$post_data['variations'] = $variations;

					//CREATE WOOCOMMERCE PRODUCT
					insert_product( $post_data );
				}
				
			}elseif ( isset($post_meta["mb_event_product"] ) )  {    
			//Editing the product
				$post_meta = get_post_meta( $post_id );
				
				$product_id = $post_meta['mb_event_product'][0];
				$product_details = get_post_meta( $product_id );

				$prices = get_post_meta( $post->ID , 'eprices');

				// STOCK UPDATE
				$currentProduct = get_post_meta( $product_id[0] );
					$currentStock = $currentProduct['_stock'];
					$currentStock = $currentProduct['_stock'][0];
			
					$postStock = $_POST['mb_event_stock'];			
					
					$sold = (soldProduct($product_id));	
					
					if ($postStock > $currentStock + $sold){			
						$update =  $postStock - $sold;
						update_post_meta( $product_id, '_stock', $update );
						update_post_meta( $product_id, '_stock_status', 'instock' );
					}
					if ($postStock < $currentStock + $sold){
						if ($postStock < $sold){
							update_post_meta( $product_id, '_stock', 0 );
							update_post_meta( $post_id, 'mb_event_stock', (int)$sold );
						}
						if ($postStock < $currentStock ){
							$update =  $postStock - $sold;
							if ($update <= 0){
								update_post_meta( $product_id, '_stock', 0 );
							}
							else {
								update_post_meta( $product_id, '_stock', $update );
								update_post_meta( $product_id, '_stock_status', 'instock' );
							}
						}
					}
					if ($postStock == $currentStock + $sold ){					
						//update_post_meta( $product_id, '_stock',      (int)$currentStock );
						//update_post_meta( $post_id, 'mb_event_stock', (int)$currentStock );
					}

				//UPDATE SIMPLE PRODUCT TYPE
				if (count($prices[0]) <= 1 ) {

					wp_set_object_terms( $product_id, 'simple', 'product_type' );

					update_post_meta( $product_id, '_price', floatval( $prices[0][0]['mb_event_price'] )  );
					update_post_meta( $product_id, '_regular_price', floatval( $prices[0][0]['mb_event_price'] )  );
					
				}else{
					// UPDATE VARIABLE PRODUCT
					
					wp_set_object_terms( $product_id, 'variable', 'product_type' );

					$variableProduct = new WC_Product_Variable( $product_id );
					$available_variations = $variableProduct->get_available_variations();

					// Delete All Variations before insert
					if (!empty($available_variations) ) 
					{
						foreach($available_variations as $v) 
						{
							wp_delete_post($v['variation_id']);
						}
					}


					// INSERT EVENT RATES ATTRIBUTES
					$attributes = array( 'event-rates');
					//VARIATIONS DATA
					$variations = array();
					foreach ($prices[0] as $pkey => $pdata) {
						$variations[] = array(
							"regular_price"=>$pdata['mb_event_price'],
							"price"=>$pdata['mb_event_price'],
							"sku"=> '00' . $pkey,
							"attributes" => array("event-rates"=>$pdata['mb_event_price_desc']),
							"manage_stock" =>"no",
							"stock_quantity"=> "",
						);
							
					}

					insert_product_attributes($product_id, $attributes, $variations); 
					insert_product_variations ($product_id, $variations);
				}
			}
		}		
		
		
		function mb_htmlSold($solds2 = null){ 			
			
			if (($solds2 !== NULL)):
				if(isset($solds2['solds'])){					
					$html = '<div class="listSolds">';
							foreach ($solds2['solds'] as $sold){
								$html .= $sold.'<br>';					
							}
					$html .= '</div>';
				}	
			endif;
		return $html;	
		}
}

function insert_product ($product_data)
{
    $post = array( // Set up the basic post data to insert for our product

        'post_author'  => $product_data['post_author'],
        'post_content' => $product_data['post_content'],
        'post_status'  => 'publish',
        'post_title'   => $product_data['post_title'],
        'post_parent'  => '',
        'post_type'    => 'product'
    );

    $post_id = wp_insert_post($post); // Insert the post returning the new post id

    if (!$post_id) // If there is no post id something has gone wrong so don't proceed
    {
        return false;
    }

    /* ADD WOOCOMMERCE PRODUCT ID */
	add_post_meta( $product_data['event_id'], 'mb_event_product', $post_id );

  
    update_post_meta( $post_id,'_visibility','hidden'); // Set the product to visible, if not it won't show on the front end
    update_post_meta( $post_id, '_sold_individually', "" );
	update_post_meta( $post_id, '_manage_stock', "yes" );
	update_post_meta( $post_id, '_backorders', "no" );
	update_post_meta( $post_id, '_stock_status', $product_data['stock_status'] );
	update_post_meta( $post_id, '_stock', $product_data['stock_qty'] );
	update_post_meta( $post_id, '_downloadable', 'yes');

    wp_set_object_terms($post_id, $product_data['categories'], 'product_cat'); // Set up its categories
    wp_set_object_terms($post_id, $product_data['type'], 'product_type'); // Set it to a variable product type


    insert_product_attributes($post_id, $product_data['available_attributes'], $product_data['variations']); // Add attributes passing the new post id, attributes & variations
    insert_product_variations($post_id, $product_data['variations']); // Insert variations passing the new post id & variations
}


function insert_product_attributes ($post_id, $available_attributes, $variations)
{
    foreach ($available_attributes as $attribute) // Go through each attribute
    {
        $values = array(); // Set up an array to store the current attributes values.

        foreach ($variations as $variation) // Loop each variation in the file
        {
            $attribute_keys = array_keys($variation['attributes']); // Get the keys for the current variations attributes

            foreach ($attribute_keys as $key) // Loop through each key
            {
                if ($key === $attribute) // If this attributes key is the top level attribute add the value to the $values array
                {
                    $values[] = $variation['attributes'][$key];
                }
            }
        }
        $values = array_unique($values); // Filter out duplicate values

        wp_set_object_terms($post_id, $values, 'pa_' . $attribute);
    }

    $product_attributes_data = array(); // Setup array to hold our product attributes data

    foreach ($available_attributes as $attribute) // Loop round each attribute
    {
        $product_attributes_data['pa_'.$attribute] = array( // Set this attributes array to a key to using the prefix 'pa'

            'name'         => 'pa_'.$attribute,
            'value'        => '',
            'is_visible'   => '1',
            'is_variation' => '1',
            'is_taxonomy'  => '1'

        );
    }

    update_post_meta($post_id, '_product_attributes', $product_attributes_data); // Attach the above array to the new posts meta data key '_product_attributes'
}

function insert_product_variations ($post_id, $variations)
{
    foreach ($variations as $index => $variation)
    {
        $variation_post = array( // Setup the post data for the variation

            'post_title'  => 'Variation #'.$index.' of '.count($variations).' for product#'. $post_id,
            'post_name'   => 'product-'.$post_id.'-variation-'.$index,
            'post_status' => 'publish',
            'post_parent' => $post_id,
            'post_type'   => 'product_variation',
            'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
        );

        $variation_post_id = wp_insert_post($variation_post); // Insert the variation

        foreach ($variation['attributes'] as $attribute => $value) // Loop through the variations attributes
        {
            $attribute_term = get_term_by('name', $value, 'pa_'.$attribute); // We need to insert the slug not the name into the variation post meta

            update_post_meta($variation_post_id, 'attribute_pa_'.$attribute, $attribute_term->slug);
        }

        update_post_meta($variation_post_id, '_price', $variation['price']);
        update_post_meta($variation_post_id, '_regular_price', $variation['price']);
    }
}