<?php


function product_badge( $meta_boxes ) {
	$prefix = 'product_badge_';

	$meta_boxes[] = array(
		'id' => 'product-badge',
		'title' => esc_html__( 'Product Badge', 'vini' ),
		'post_types' => array('product' ),
		'context' => 'after_editor',
		'priority' => 'default',
		'autosave' => 'true',
		'fields' => array(
			array(
				'id' => $prefix . 'new',
				'name' => esc_html__( 'New', 'vini' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Tick the box if you want the New Badge to be shown on the upper right of the product column', 'vini' ),
			),
			array(
				'id' => $prefix . 'bestseller',
				'name' => esc_html__( 'Best Seller', 'vini' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Tick the box if you want the Best Seller Badge to be shown on the upper right of the product column', 'vini' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'product_badge' );