<?php
/**
 * Register Custom Post Type
 * Post Type Key: event
 */
function viniecappricci_cpt_events() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'vini' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'vini' ),
		'menu_name'             => __( 'Events', 'vini' ),
		'name_admin_bar'        => __( 'Events', 'vini' ),
		'archives'              => __( 'Event Archives', 'vini' ),
		'parent_item_colon'     => __( 'Parent Item:', 'vini' ),
		'all_items'             => __( 'All Events', 'vini' ),
		'add_new_item'          => __( 'Add New Event', 'vini' ),
		'add_new'               => __( 'Add New Event', 'vini' ),
		'new_item'              => __( 'New Event', 'vini' ),
		'edit_item'             => __( 'Edit Event', 'vini' ),
		'update_item'           => __( 'Update Event', 'vini' ),
		'view_item'             => __( 'View Event', 'vini' ),
		'search_items'          => __( 'Search Event', 'vini' ),
		'not_found'             => __( 'Not found', 'vini' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'vini' ),
		'featured_image'        => __( 'Featured Image', 'vini' ),
		'set_featured_image'    => __( 'Set featured image', 'vini' ),
		'remove_featured_image' => __( 'Remove featured image', 'vini' ),
		'use_featured_image'    => __( 'Use as featured image', 'vini' ),
		'insert_into_item'      => __( 'Insert into item', 'vini' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'vini' ),
		'items_list'            => __( 'Items list', 'vini' ),
		'items_list_navigation' => __( 'Items list navigation', 'vini' ),
		'filter_items_list'     => __( 'Filter items list', 'vini' ),
	);
	$args = array(
		'label'                 => __( 'event', 'vini' ),
		'description'           => __( 'Vini e Capricci events', 'vini' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'viniecappricci_cpt_events', 0 );