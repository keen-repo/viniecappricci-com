<?php 
/*
Plugin Name: Vini e Capricci Custom Plugin
Plugin URI:
Description: Custom Plugin for  Vini e Capricci
Version: 1.0.0
Author: Keen
Author URI:http://www.keen.com.mt
*/

define('THEMELANG', 'vini'); 

require_once dirname( __FILE__ ) . '/class-register-cpt-partner.php';
require_once dirname( __FILE__ ) . '/class-register-cpt-event.php';
require_once dirname( __FILE__ ) . '/class-register-cpt-vacancies.php';



require_once dirname( __FILE__ ) . '/metabox/image.php';
require_once dirname( __FILE__ ) . '/metabox/contact.php';
require_once dirname( __FILE__ ) . '/metabox/partner.php';
require_once dirname( __FILE__ ) . '/metabox/event.php';
require_once dirname( __FILE__ ) . '/metabox/vacancy.php';
require_once dirname( __FILE__ ) . '/metabox/product.php';


require_once dirname( __FILE__ ) . '/template/tpl-banner.php';
require_once dirname( __FILE__ ) . '/template/tpl-contact.php';
require_once dirname( __FILE__ ) . '/template/tpl-event.php';

/**
 * Enqueue Styles and Scripts
 * @return [type] [description]
 */
add_action('wp_enqueue_scripts', 'viniecapricci_pl_scripts');
function viniecapricci_pl_scripts(){
	// wp_enqueue_style( 'abrahams-pl-css', plugin_dir_url(__FILE__) . 'css/style.css' );
	// wp_enqueue_script( 'custom-plugin-js',  plugin_dir_url(__FILE__) . '/assets/js/abraham.js', array('jquery'), false , true );
}


