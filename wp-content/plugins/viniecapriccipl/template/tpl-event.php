<?php
/*
 * Show Events List
 * [show_events]
 */

add_shortcode('show_events', 'tpl_show_events');

if ( !function_exists( 'tpl_show_events' ) ) {
	function tpl_show_events( $atts )
	{
		global $wpdb;
		$events = $wpdb->get_results( 
				"SELECT $wpdb->postmeta.*,  $wpdb->posts.*
				FROM ( $wpdb->posts
				INNER JOIN $wpdb->postmeta
				ON $wpdb->posts.ID = $wpdb->postmeta.post_id)
				WHERE $wpdb->posts.post_type LIKE 'event' 
				AND $wpdb->posts.post_status LIKE 'publish'
				AND $wpdb->postmeta.meta_key LIKE 'mb_event_date'
				ORDER BY $wpdb->postmeta.meta_value ASC"
			);

		$eventArr = array();
		foreach ( $events as $event ) 
		{	
			$id = $event->post_id;
			$title = $event->post_title;

			$startTime = rwmb_meta( 'mb_event_start_time' , 'type=time', $id );
			$endTime = rwmb_meta( 'mb_event_end_time' , 'type=time', $id );

			$convertedDate =  strtotime($event->meta_value);
			$formattedDate = date('l, jS F Y', $convertedDate);
			$month = date("m", $convertedDate);
			$year = date("Y", $convertedDate);
			$day = date("d", $convertedDate);

			$eventArr[$id] = array( 'title' => $title,
									'year' => $year,
									'month' => $month,
									'day' => $day,
									'start_time' => $startTime,
									'end_time' => $endTime
								);
		}

		$yearArr = array();
		$monthByYearArr = array();
		$postByMonthArr = array();

		$current_year = date("Y");  
		$current_month = date("m");  
		  
		echo $current_year;  

		foreach ($eventArr as $e => $att) {
			if (! in_array($att['year'], $yearArr)) 
			{
				$yearArr[] = $att['year'];
			}

			if (! in_array( $att['month'], $yearArr  ) ) 
			{
				$monthByYearArr[$att['year']][]  = $att['month'];
			}

			$postByMonthArr[$att['year']][$att['month']][]  = $e;

		}
		echo '<div class="box-event">';
		echo  '<h1 class="widget-title">' . __('Upcoming Events' , THEMELANG ) . '</h1>';

		krsort($postByMonthArr);
		foreach ($postByMonthArr as $year => $months) {
			
				echo '<div class="box-year">';
			
				ksort($months);

				/**
				 * Check year and hide past events
				 */
				if ( $year >= $current_year) :
					
				echo '<div class="box-months">';

				foreach ($months as $month => $post) 
				{	
					
					if ($month >= $current_month) :
						
					$monthName = date("F", mktime(0, 0, 0, $month, 10));
					
						echo '<ul>';
						foreach ($post as $id) {
							foreach ($eventArr as $e => $att) {
								if ($e == $id ) 
								{	
									$convertedDate = strtotime($att['day'] . '-' .  $att['month'] . '-' . $att['year']);
									$dateWithYear = date('l, jS F Y', $convertedDate);
									$dateWithOutYear = date('jS F', $convertedDate);

									$between = __('between ', THEMELANG);
									if (  $att['start_time'] != 0  ) {
										if ($att['end_time'] != 0) {
											$startTime = $between . date('h:i A', strtotime($att['start_time']));
										}else{
											$startTime = date('h:i A', strtotime($att['start_time']));
										}
										
									}else{
										$startTime = '';
									}

									if ( $att['end_time'] != 0 ) {
										$endTime = ' & ' . date('h:i A', strtotime($att['end_time']));
									}else{
										$endTime = '';
									}
									
									
									echo '<li><a href="' .get_post_permalink($id) .'">
									<span>'. $dateWithYear .'</span><span>' 
										. $att['title'] . '</a></li>';
								}
							}
						}

						echo "</ul>";
					endif;

				}

				echo '</div>';
			endif;
				echo '</div>';

		


		}
		echo '</div>';
	}
}
