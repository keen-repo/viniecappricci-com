
<?php
/*
 * Show Banner
 * [show_banner]
 */

add_shortcode('show_banner', 'tpl_show_banner');
if ( !function_exists( 'tpl_show_banner' ) ) {
	function tpl_show_banner( $atts ){

		extract(shortcode_atts(
			array(
			       'size' => 'banner-large',
			       'id' =>''
			    ), 
		$atts));

		$images = get_post_meta( $id, 'mb_banner_images', true );

		if ( !empty ( $images )) {
			
			$tpl = '<div class="wrap-banner"> <div class="banner">';

			foreach ( $images as $image ) {
				$btn_label = !empty( $image['mb_banner_btn_label'] ) ? $image['mb_banner_btn_label'] : __( 'Read More', THEMELANG );
				foreach ($image['mb_banner_image'] as $img) {
					$tpl .= '<div class="image" style="background-image: url(\''.wp_get_attachment_image_url( $img, $size ) .'\')">';

					if ( !empty($image['mb_banner_text']) ) {
						$tpl .='<div class="content"><div class="text"><h1>' . nl2br($image['mb_banner_text']) . '</h1>';

						if ( ! empty($image['mb_banner_url']) ) {
							$tpl .= '<a href="' .$image['mb_banner_url']. '" class="btn btn-gold">' . $btn_label . '</a>';
						}

						if ( ! empty($image['mb_banner_url_2']) ) {
							$tpl .= '<a href="' .$image['mb_banner_url_2']. '" class="btn btn-gold">' . $image['mb_banner_btn_label_2'] . '</a>';
						}

						$tpl .= '</div></div>';

					}


					$tpl .= '</div>';
				}
			}

			$tpl .= '</div>';

			if (is_page('concept')) 
			{
				$icon = rwmb_meta('concept_icon', array( 'limit' => 1 ) );
				$label = rwmb_meta('concept_label', 'type=text' );
				$url = rwmb_meta('concept_url', 'type=url' );
				$icon = reset( $icon );
				$tpl .= '<div class="wrap-icon"><a href="'. $url .'" target="_blank"><img src="'. $icon['full_url'].'";?alt=""><span>'. $label .'</span></a></div>';
			}
			
			$tpl .= '</div>';
			return $tpl;

			}
		}
		
}
