<?php 
/**
 * Show contact information
 * email, contact number, etc..
 * 
 */

	add_shortcode( 'show_contact_info', 'tpl_contact_info' );

	function tpl_contact_info ( ){

		$address= rwmb_meta( 'mb_contact_address', 'type=text');
		$contact_info = rwmb_meta( 'mb_contact_info' );
		$click_to_call_no = preg_replace( "/[^0-9]/", "", $contact_info['mb_contact_telno'] );

		$tpl  = '<div class="box-contact">';
			$tpl .= '<p class="text"><span class="label-strong">' .$contact_info['mb_contact_companyname'] . '</span> ' . $address . '</p>';
			$tpl .= '<p class="text"> <span>'. __('Telephone: ', THEMELANG) .'<a href="tel:+' . $click_to_call_no . '">' . $contact_info['mb_contact_telno'] . '</a></span></p>';
			$tpl .= '<p class="text"> <span>'. __('Email: ', THEMELANG) .'<a href="mailto:' . $contact_info['mb_contact_email'] . '">' . $contact_info['mb_contact_email'] . '</a></span> <span></p>';
		$tpl .= '</div>';
		return $tpl;
	}

	/**
 * Show Map
 * [show_map map_height="" map_width="" marker_title="" info_window=""]
 * marker_title and info_window - can accept html tags
 */
	
	add_shortcode( 'show_map', 'tpl_map' );

	/**
	 * Display map
	 * @param  [type] $atts [description]
	 * @return [type]       [description]
	 */
	function tpl_map( $atts ) 
	{
		$address = rwmb_meta( 'mb_contact_address', 'type=text');
		$contact_info = rwmb_meta( 'mb_contact_info' );
		$telno = $contact_info['mb_contact_telno'];
		$fax = $contact_info['mb_contact_faxno'];

		extract(shortcode_atts(array(
	      'height' => '430px',
	      'width' => '100%',
	      'marker_title' => '<p>' . $address . '</p><p>' . $telno . '</p><p>' . $faxno . '</p>',
	      'info_window' => '<div class="map-info-window"><h1>' . get_bloginfo('name') . '</h1><p>' . $address . '<br />' . $telno . '<br />' . $faxno . '</p></div>',
	      'marker_icon' => ''

	   ), $atts));

		$map_args = array(
		    'type'         => 'map',   // Required
		    'width'        => $width, // Map width, default is 640px. Can be '%' or 'px'
		    'height'       => $height , // Map height, default is 480px. Can be '%' or 'px'
		    'zoom'         => 14,      // Map zoom, default is the value set in admin, and if it's omitted - 14
		    'marker'       => true,    // Display marker? Default is 'true',
		    'marker_title' => $marker_title ,      // Marker title when hover
		    'marker_icon'  => $marker_icon,
		    'info_window'  => $info_window , // Info window content, can be anything. HTML allowed.
		    'styles'   => '[{featureType: "all",stylers: [{ saturation: -70 } ]}]',
		    'js_options'   => array(),
		    'api_key'      => 'AIzaSyB-IR0wNdQ7AD7G7k96ww9CE0iXMd7C4KE',
		);

		$map = rwmb_meta( 'mb_contact_map', $map_args );


		$tpl = '<div class="box-map">';
		$tpl .= $map;
		$tpl .= '</div>';

		return $tpl;
	}