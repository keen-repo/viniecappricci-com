<?php
/**
 * The template for displaying Partners pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header();

$categories = get_categories();
$iconsDir = get_bloginfo('template_url'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
			$terms = get_terms( 'ab_partner_cats' , array(
				'hide_empty' => true,
				'orderby' 	 => 'name',
				'order'		 => 'ASC'

			) );
		?>

			<div class="section group partner-list">
				<div class="col partner-label">
					<span>
						<?php _e( 'Select the product range below to discover where our products are from:' , 'vini' ); ?>
					</span>
				</div>

				<div class="action-buttons">
					<a class="previous" onclick='prevTab()'><</a>
					<a class="next" onclick='nextTab()'>></a>
				</div>


				<ul class="tabs col partner-prod-list">
					<?php
						foreach ( $terms as $term ) {
						    printf( '<li class="tab-link %3$s" data-tab="tab-%1$s" data-id="%1$s" data-category="%3$s">%2$s</li>',
						        $term->term_id,
						        esc_html( $term->name ),
						        $term->slug
						    );
						}
					?>
				</ul>
			</div>

			<?php

				$index = 0;
				foreach ($terms as $term) {

					$termID= $term->term_id;

					echo '<div id="tab-'.$termID.'" class="tab-content">';
						echo '<div id="map_wrapper" class="'.$term->slug.'">';
    					echo	'<div id="map_canvas_' . $termID . '" class="mapping remove-google-map"></div>';
						echo '</div>';

						$args = array(
						'post_type' => 'partners',
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'tax_query' => array(
						    array(
							    'taxonomy' => 'ab_partner_cats',
							    'field' => 'slug',
							    'terms' => $term->slug,
							    'operator' => 'IN'
						     )
						  )
						);
						$query = new WP_Query( $args );

						$LatLngArr = [];
						$markers = [];
						$counter = 1;
						$bounds = [];
						$latLngBoundsArr = [];
						if ( $query->have_posts() ) :
							while ($query->have_posts()) : $query->the_post();

								$address = get_post_meta( get_the_ID(), 'mb_partner_address', true );
								$location = get_post_meta( get_the_ID(), 'mb_partner_map', true );
								if ( empty( $location )) {
									$location = '36.03672, 14.26183';
								}
								$url = get_post_meta( get_the_ID(), 'mb_partner_url', true );
								$product_link = get_post_meta( get_the_ID(), 'mb_partner_product_link', true );

								$markers = explode(',', $location);

								$latLngBounds = [$markers[0], $markers[1]];
								array_push($latLngBoundsArr, $latLngBounds);
								$LatLngBounds_to_JS = json_encode($latLngBoundsArr);

								$arr = [get_the_title(), (float) $markers[0], (float) $markers[1], $markers[2], $address, $product_link, $url];
								array_push($LatLngArr, $arr);
								$LatLngArr_to_JS = json_encode($LatLngArr);

								$index++;


								/*echo '<div class="locations"  data-address="' . $address .'" data-latlng="' . $location . '">
										<div class="info-window">
											<h2><a href="' . $product_link  . '">' . get_the_title() . '</a></h2>
											<p class="address">'. $address.'</p><p><a href="' . $url . '" target="_blank">'. $url .'</a></p>
											<p><a href="' . $product_link . '" target="_blank">'. __('View products', THEMELANG ) .'</a></p>
										</div>
									</div>';*/
							endwhile; ?>
							<div id="map_<?=  $index; ?>" style="height: 650px;"></div>
							<!-- MAP - End -->
							<script>
								jQuery(document).ready(function($) {
									var planes = <?= $LatLngArr_to_JS; ?>;
									var bounds = <?= $LatLngBounds_to_JS; ?>;

									var map_<?=  $index; ?> = L.map('map_<?=  $index; ?>').setView([37.9415768, 14.4113351], 11);
									var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';

									setTimeout(function() {
										map_<?=  $index; ?>.invalidateSize(false);
									}, 400);

									$( '.tabs .tab-link' ).on( 'click', function() {
										setTimeout(function() {
											map_<?=  $index; ?>.invalidateSize(false).fitBounds(bounds);
										}, 400);
									});


									L.tileLayer(
										'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
											attribution: '&copy; ' + mapLink + ' Contributors',
											 minZoom: 3, maxZoom: 15
										}).addTo(map_<?=  $index; ?>);

									var agIcon = L.icon({
										iconUrl: '<?= $iconsDir; ?>/assets/build/images/map-marker-yellow.png',
										iconSize: [31,43],
										iconAnchor: [16,42]
									});

									for (var i = 0; i < planes.length; i++) {
										let title = planes[i][0];
										let address = planes[i][4];
										let prod = planes[i][5];
										let url = planes[i][6];

									let popup = '<div class="locatios">'
													+'<div class="info-window">'
														+'<h2><a href="' + prod + '">' + title + '</a></h2>'
															+'<p class="address">' + address + '</p><p><a href="' + url + '" target="_blank">' + url + '</a></p>'
															+'<p><a href="' + prod + '" target="_blank"><?= __('View products', THEMELANG ) ?></a></p>'
													+'</div>'
												+'</div>';

										marker = new L.marker([planes[i][1],planes[i][2]], {icon: agIcon})
										.bindPopup(popup)
										.addTo(map_<?=  $index; ?>);
									}
								});
							</script>
							<?php
						endif;
				 	echo '</div>';
				 } // foreach ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<style>
	#map_wrapper {
		display: none;
	}
</style>
<?php get_footer(); ?>