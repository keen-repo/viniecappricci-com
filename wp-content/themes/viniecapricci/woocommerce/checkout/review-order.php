<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



?>
<table class="shop_table woocommerce-checkout-review-order-table">
	<thead>
		<tr>
			<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>

		<?php
			do_action( 'woocommerce_review_order_before_cart_contents' ); ?>

		<!-- check if coupon code is applied  -->
		<?php if ( empty( WC()->cart->get_coupons()) ): ?>

			<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					
					$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						?>
						<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
							<td class="product-name">
								<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
								<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
								<?php echo WC()->cart->get_item_data( $cart_item ); ?>
							</td>
							<td class="product-total">
								<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
								<?php 
								?>
							</td>
						</tr>
						<?php
					}
				}

			?>
		<!-- if coupon code is applied -->
		<?php else: ?>
			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>

			<?php 
					$coupon_code = $coupon->code;
					$coupon_applied = new WC_Coupon($coupon_code);

			endforeach;

			if ( $coupon_applied->discount_type == 'percent_product' ) : 

					$discount = $coupon_applied->amount / 100;

					foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ): 

							$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
							$is_valid_coupon = $coupon_applied->is_valid_for_product( $_product, $cart_item);
						?>
						
						<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
								<td class="product-name">

									<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>

								<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
									<?php echo WC()->cart->get_item_data( $cart_item ); ?>
								</td>

							<!-- Check if Hamper Package -->
							<?php if ($cart_item['_is_hamper'] == 1 ):
								$hamper_items_arr = [];
								$hamper_discount = 0;
							 ?>


								<!-- Loop through hanper items -->
									<td class="product-total">
										<?php $hamper_package_amount = $cart_item['line_subtotal'] * $cart_item['quantity']; ?>
								<?php foreach ( $cart_item['_hamper_items'] as $hamper_item ) : ?>
									<?php	
										$hamper_product = get_product( $hamper_item['id'] );
										$is_valid_coupon = $coupon->is_valid_for_product( $hamper_product );
										$total_hamper_item_amount = $hamper_product->get_price() * $hamper_item['quantity'];


										if ( $is_valid_coupon ) {
											$total_coupon_discount =  $total_hamper_item_amount * $discount;
											
											if ( $total_coupon_discount > 0 ) {
												$hamper_discount += $total_coupon_discount;
												$newsubtotal = wc_price( $total_hamper_item_amount - $total_coupon_discount  );

												$hamper_items_arr[] = sprintf( $hamper_item['title'].': <s>%s</s> - %1s <span style="color: red;">Save: %2s</span>', wc_price( $total_hamper_item_amount ) , $newsubtotal , wc_price( $total_coupon_discount ) );
											}
										} else {
											$hamper_items_arr[] = sprintf( $hamper_item['title'].': %s', wc_price( $total_hamper_item_amount ) );
										}



										?> <?php

								endforeach;
								?>

								<span style="font-weight: bold; display: block;">
									Hamper Price: 
									<s>
										<!-- Original Price:  -->
										<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); 
									 ?>
										<?php //echo apply_filters( 'woocommerce_cart_item_subtotal', wc_price( $cart_item['line_subtotal']) ); ?>
									</s> - 
									<!-- Discounted Price -->
									<?php 
								$total_hamper_discount = $hamper_discount * $cart_item['quantity'];
									echo apply_filters( 'woocommerce_cart_item_subtotal', wc_price( $cart_item['line_subtotal'] - $total_hamper_discount ) );  ?>
									<!-- Total Discount -->
									<span style="color: red;">Save: 
										<?php echo wc_price ($total_hamper_discount ); ?>
									</span>
								</span>
								
								<br>

								<?php 
								// Show Hamper Items
								echo implode('<br />', $hamper_items_arr );

								?>


								</td>


								<?php else: ?>
									<!-- Not Hamper Package  and If coupon is valid to cart item -->
									<?php if ( $is_valid_coupon ): ?>

										<?php 
											$total_item_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
											$total_coupon_discount =  $total_item_amount * $discount ;
											$new_subtotal = $total_item_amount - $total_coupon_discount;
										?>
											<td class="product-total">
												<span>
												<s>
													<?php echo apply_filters( 'woocommerce_cart_item_subtotal', wc_price( $total_item_amount) ); ?>
												</s> - 

												<?php echo apply_filters( 'woocommerce_cart_item_subtotal', wc_price( $new_subtotal) ); ?></span>
												
												<span style="color: red;">Save: <?php echo  $total_coupon_discount; ?> </span>
											</td>

										<!-- if cart item not valid on entered coupon code -->
										<?php else: ?>
										
												<td class="product-total">
													<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
												</td>
										
									<?php endif; ?>

							<?php endif; ?>
					</tr>
				<?php
				endforeach;

			else: ?>
				<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
					
					$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

					if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
						?>
						<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
							<td class="product-name">
								<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>
								<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
								<?php echo WC()->cart->get_item_data( $cart_item ); ?>
							</td>
							<td class="product-total">
								<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
								<?php 
								?>
							</td>
						</tr>
						<?php
					}
				}
			?>
			<?php endif; // $coupon_applied->discount_type == 'percent_product'
		endif; ?>
		
		<!-- End Fix -->

				
	<?php do_action( 'woocommerce_review_order_after_cart_contents' );  ?>

	</tbody>
	<tfoot>

		<tr class="cart-subtotal">
			<th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_subtotal_html();  //echo WC()->cart->get_cart_subtotal(); ?></td>
		</tr>

		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>

			<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
				<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
				<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

		<?php endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<tr class="fee">
				<th><?php echo esc_html( $fee->name ); ?></th>
				<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
			<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
					<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
						<th><?php echo esc_html( $tax->label ); ?></th>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
					<td><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

		<tr class="order-total">
			<th><?php _e( 'Total', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</tfoot>
</table>
