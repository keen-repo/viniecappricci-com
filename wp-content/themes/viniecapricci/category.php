<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<header>
			<h1 class="page-title"><?php _e('Blog', 'vini'); ?></h1>
		</header>

		<?php 
		echo do_shortcode('[ajax_load_more container_type="ul" css_classes="new-list" post_type="post" category="blog" button_label="Load More" button_loading_label="Loading More" scroll="false" transition_container="false"]');
		 ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
