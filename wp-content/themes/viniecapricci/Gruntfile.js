/*global module:false*/
module.exports = function(grunt) {
  // load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %> \n' +
      '* Homepage <%= pkg.homepage ? ":" + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>; */',

    // Task configuration.

    // Adds a simple banner to files
    usebanner: {
      dist: {
        options: {
          position: 'bottom',
          banner: '<%= banner %>'
        },
        files: {
          src: ['<%= dirs.dest %>/css/*.css', '<%= dirs.dest %>/js/*.js']
        }
      }
    },

    dirs: {
      src: 'assets/source',
      dest: 'assets/build',
    },

    // watch for changes and trigger sass, jshint, uglify and livereload
    watch: {
      sass: {
        files: ['<%= dirs.src %>/styles/**/*.{scss,sass}'],
        tasks: ['sass', 'postcss']
      },
      js: {
        files: '<%= jshint.all %>',
        tasks: ['jshint', 'uglify']
      },

      images: {
        files: ['<%= dirs.src %>/images/**/*.{png,jpg,gif}'],
        tasks: ['imagemin']
      },

    },

    // sass
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: [{
          expand: true,
          cwd: '<%= dirs.src %>/styles',
          src: [
            '<%= pkg.title || pkg.name %>.scss'
          ],
          dest: '<%= dirs.dest %>/css/',
          ext: '.css'
        }]
      }
    },

    // postcss
    postcss: {
      options: {
        map: true, // inline sourcemaps
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({
            browsers: 'last 4 versions'
          }), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: '<%= dirs.dest %>/css/*.css',
        dest: '<%= dirs.dest %>/css/<%= pkg.title || pkg.name %>.css'
      }
    },

    // uglify to concat, minify, and make source maps
    uglify: {
      plugins: {
        options: {
          sourceMap: '<%= dirs.src %>/js/plugins.js.map',
          sourceMappingURL: 'plugins.js.map',
          sourceMapPrefix: 2
        },
        files: {
          '<%= dirs.dest %>/js/plugins.min.js': [
            '<%= dirs.src %>/js/plugins.js',
            '<%= dirs.src %>/vendor/*.js', '!<%= dirs.src %>/vendor/customizer.js',
            '!<%= dirs.src %>/vendor/jquery/dist/*.js', '<%= dirs.src %>/vendor/jquery/dist/jquery.slim.min.js',
            '!<%= dirs.src %>/vendor/**/dist/*.js', ' <%= dirs.src %>/vendor/scrollreveal/dist/*.min.js',
            '!<%= dirs.src %>/vendor/**/slick/*.js', '<%= dirs.src %>/vendor/**/slick/*.min.js'
          ]
        }
      },
      main: {
        options: {
          sourceMap: '<%= dirs.src %>/js/main.js.map',
          sourceMappingURL: 'main.js.map',
          sourceMapPrefix: 2
        },
        files: {
          '<%= dirs.dest %>/js/main.min.js': [
            '<%= dirs.src %>/js/main.js'
          ]
        }
      },
      customizer: {
        options: {
          sourceMap: '<%= dirs.src %>/js/customizer.js.map',
          sourceMappingURL: 'customizer.js.map',
          sourceMapPrefix: 2
        },
        files: {
          '<%= dirs.dest %>/js/customizer.min.js': [
            '<%= dirs.src %>/vendor/customizer.js'
          ]
        }
      }
    },

    // javascript linting with jshint
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        "force": true
      },
      all: [
        'Gruntfile.js',
        '<%= dirs.src %>/js/*.js'
      ]
    },

    imagemin: {
      dist: {
        options: {
          optimizationLevel: 7,
          progressive: true,
          interlaced: true,
          svgoPlugins: [{
            removeViewBox: false
          }],
        },
        files: [{
          expand: true,
          cwd: '<%= dirs.src %>/images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: '<%= dirs.dest %>/images/'
        }]
      }
    },

    copy: {
      main: {
        expand: true,
        cwd: '<%= dirs.src %>/fonts',
        src: '**',
        dest: '<%= dirs.dest %>/fonts',
      },
    }

  });

  //Register Task

  // Default task.
  grunt.registerTask('default', ['sass', 'postcss', 'jshint', 'uglify', 'imagemin', 'watch']);


};