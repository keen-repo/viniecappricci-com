<?php

add_action( 'init', 'custom_taxonomy_Item', 99 );
function custom_taxonomy_Item()  {

    $labels = array(
        'name'                       => 'Wine Packaging Types',
        'singular_name'              => 'Wine Packaging Type',
        'menu_name'                  => 'Wine Packaging Types',
        'all_items'                  => 'All Wine Packaging Types',
        'parent_item'                => 'Parent Wine Packaging Type',
        'parent_item_colon'          => 'Parent Wine Packaging Type:',
        'new_item_name'              => 'New Item Name',
        'add_new_item'               => 'Add New Wine Packaging Type',
        'edit_item'                  => 'Edit Wine Packaging Type',
        'update_item'                => 'Update Wine Packaging Type',
        'separate_items_with_commas' => 'Separate Item with commas',
        'search_items'               => 'Search Items',
        'add_or_remove_items'        => 'Add or remove Wine Packaging Types',
        'choose_from_most_used'      => 'Choose from the most used Wine Packaging Types',
    );

    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
    );

    register_taxonomy( 'wine_packaging', 'product', $args );
    register_taxonomy_for_object_type( 'wine_packaging', 'product' );

}

add_action('woocommerce_before_cart_item_quantity_zero', 'vec_remove_item_from_cart', 10, 1);

function vec_remove_item_from_cart($cart_item_key) {

    $cart_items = WC()->cart->get_cart();

    $the_item = $cart_items[$cart_item_key];

    $is_wine_box = false;

    $wine_packages = WC()->session->get('wine_packages', array());

    /* check if removed item is box */
    foreach ($wine_packages as $i=>$wine_packaging_key) {

        if ($cart_item_key == $wine_packaging_key) {
            unset($wine_packages[$i]);
            $is_wine_box = true;
        }

    }

    WC()->session->set('wine_packages', $wine_packages);

    if ($is_wine_box === false) {

        /* check if item has been removed so we remove it from any wine packages */
        $old_quantity = $the_item['quantity'];

        $removal_count = 0;

        foreach ($wine_packages as $i=>$wine_packaging_key) {

            if ($removal_count == $old_quantity) break;

             $the_box = $cart_items[$wine_packaging_key];

             $products_in_slots = explode(',', $the_box['variation']['slots']);

             $new_slots = array();

             foreach ($products_in_slots as $product_id) {
                if ($product_id != $the_item['product_id']) {
                    $new_slots[] = $product_id;
                    $removal_count++;
                }

             }

             if (empty($new_slots)) {

                /* seems like all items were removed from this box so we should remove it from the cart */
                WC()->cart->set_quantity($wine_packaging_key, 0);

             } else {

                WC()->cart->cart_contents[$wine_packaging_key]['variation']['slots'] = implode(',', $new_slots);

            }


        }

    }

    /* Check if product is wine hamper if it is we to count how many there was and then remove boxes that have this effect */

   

    $allowed_product_categories = array(4194);

    $product_cats = wp_get_post_terms( $the_item['product_id'], 'product_cat' );

    foreach ($product_cats as $product_cat) {

        if (in_array($product_cat->term_id, $allowed_product_categories)) {

            /* we need to loop through all wine hamper boxes */
            foreach ($cart_items as $cart_item_id=>$cart_data) {
                if (isset($cart_data['variation']['Packaging']) &&  $the_item['product_id'] == $cart_data['variation']['Packaging']) {
                    WC()->cart->set_quantity($cart_item_id, 0);
                }

            }

        }

    }

    $old_quantity = $the_item['quantity'];

}


add_action('woocommerce_after_cart_item_quantity_update', 'vec_update_quantity_in_cart', 10, 2);

function vec_update_quantity_in_cart($cart_item_key, $quantity) {

    $cart_items = WC()->cart->get_cart();

    $the_item = $cart_items[$cart_item_key];

    $wine_packages = WC()->session->get('wine_packages', array());

    $taken_quantity = 0;

    foreach ($cart_items as $cart_key=>$cart_data) {

        if (in_array($cart_key, $wine_packages)) {
            /* This is a box */

            $products_in_slots = explode(',', $cart_data['variation']['slots']);

            $new_slots = array();

            foreach ($products_in_slots as $pslot) {

                if ($taken_quantity >= $quantity) continue;

                $new_slots[] = $pslot;
                
                if ($the_item['product_id'] == $pslot) {
                    $taken_quantity++;
                }

            }

            if (empty($new_slots)) {
                WC()->cart->set_quantity($cart_key, 0);
            } else {
                WC()->cart->cart_contents[$cart_key]['variation']['slots'] = implode(',', $new_slots);
            }

        }

    }

     /* Wine Hamper Packing Removal */
    $allowed_product_categories = array(4194);

    $product_cats = wp_get_post_terms( $the_item['product_id'], 'product_cat' );

    $taken_quantity = 0;

    foreach ($product_cats as $product_cat) {

        if (in_array($product_cat->term_id, $allowed_product_categories)) {

            foreach ($cart_items as $cart_item_id=>$cart_data) {
                if (isset($cart_data['variation']['Packaging']) &&  $the_item['product_id'] == $cart_data['variation']['Packaging']) {

                    if ($taken_quantity >= $quantity) {
                         WC()->cart->set_quantity($cart_item_id, 0);
                         continue;
                    }

                    $taken_quantity++;
                }

            }

        }

    }


}



function prepare_wine_package_session($cart) {

    $wine_packages = WC()->session->get('wine_packages', array());

    $cart_items = $cart->get_cart();

    foreach($cart_items as $cart_item_key=>$cart_data) {

        if (isset($cart_data['variation']['slots']) && empty($wine_packages)) {
            // this means we have a box in the cart however the wine packaging session has been cleared at this point
            $wine_packages[] = $cart_item_key;
        }

    }

    WC()->session->set('wine_packages', $wine_packages);


}

add_action('woocommerce_cart_loaded_from_session', 'prepare_wine_package_session', 99, 1);

/*
function vec_add_wine_packaging_details_to_cart_list($cart_item_key, $values) {

    if (isset($values['variation']['Packaging'])) {

        ?>
        <div>
            <strong>Item in Package:</strong><br />
            <?php echo get_the_title($values['variation']['Packaging']); ?>
        </div>
        <?php
    }

    
    if (isset($values['variation']['slots'])) {

        $pslots = explode(',', $values['variation']['slots']);

        ?>
        <div>
            <strong>Items in Package:</strong>
            <ul>
            <?php
            foreach ($pslots as $pslot) {
            ?>
            <li><?php echo get_the_title($pslot); ?></li>
            <?php
            }
            ?>
            </ul>
        </div>
        <?php

    }

}

add_action('vec_product_cart_list_item_extra','vec_add_wine_packaging_details_to_cart_list',1,3); */

apply_filters( 'woocommerce_get_item_data', $item_data, $cart_item );



add_action('woocommerce_get_item_data', 'vini_wine_packaging_woocommerce_get_item_data', 10, 2);


function vini_wine_packaging_woocommerce_get_item_data($item_data, $cart_item) {

    if (isset($cart_item['variation']['slots'])) {

      
        $pslots = explode(',', $cart_item['variation']['slots']);

        $titles = array();

        foreach ($pslots as $pid) {
            $titles[] = get_the_title($pid);
        }

        $item_data[] = array(
            'key' => 'Items in Package',
            'value' => implode('<br />', $titles)
        );


    }

    return $item_data;


}




/* Add wine package list items into order */
function vec_add_wine_package_info_to_order($item_id, $values) {

     if (isset($values['variation']['slots'])) {

        $pslots = explode(',', $values['variation']['slots']);

        $plist = array();

        foreach ($pslots as $pslot) {
            $plist[] = get_the_title($pslot);
        }

        $wine_packages = WC()->session->get('wine_packages', array());
        WC()->session->set( 'wine_popup_shown', 0 );
        
        wc_add_order_item_meta($item_id,'vec_wine_package_list_items', implode(PHP_EOL, $plist));  


    }

     if (isset($values['variation']['Packaging'])) {

        $product_title = get_the_title( $values['variation']['Packaging']);

        wc_add_order_item_meta($item_id,'vec_wine_hamper_items', $product_title);  


    }

}

add_action('woocommerce_add_order_item_meta','vec_add_wine_package_info_to_order',1,2);



add_action( 'wp_ajax_vec_add_wine_packaging_to_cart',           'ajax_vec_add_wine_packaging_to_cart' );
add_action( 'wp_ajax_nopriv_vec_add_wine_packaging_to_cart',    'ajax_vec_add_wine_packaging_to_cart' );

function ajax_vec_add_wine_packaging_to_cart() {
   
    vec_add_packaging_to_cart($_POST['product_id'], false);

}

function vec_add_packaging_to_cart($product_id, $add_product = true) {

        $quantity = (int)$_POST['quantity'];

        if ($quantity < 1) $quantity = 1;

        $packaging_id = (int)$_POST['packaging_id'];

        if ($add_product) {

            for ($i = 1;$i <= $quantity;$i++) {
                WC()->cart->add_to_cart($product_id, 1, '');
            }

        }

        if ($packaging_id > 0) {

            for ($i = 1;$i <= $quantity;$i++) {
                WC()->cart->add_to_cart($packaging_id, 1, '', array('unqk' => mt_rand(), 'Packaging' => $product_id));
            }

        }


    }
?>