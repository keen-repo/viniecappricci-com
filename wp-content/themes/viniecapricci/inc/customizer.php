<?php
/**
 * vini Theme Customizer.
 *
 * @package vini
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function vini_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Site Logo
	$wp_customize->add_section( 'themeslug_logo_section' , array(
    'title'       => __( 'Site Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the header',
	) );

	$wp_customize->add_setting( 'themeslug_logo' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
	    'label'    => __( 'Header Logo', 'themeslug' ),
	    'section'  => 'themeslug_logo_section',
	    'settings' => 'themeslug_logo',
	) ) );

	//  Logo in Footer
	$wp_customize->add_section( 'themeslug_logo_footer_section' , array(
    'title'       => __( 'Footer Site Logo', 'themeslug' ),
    'priority'    => 30,
    'description' => 'Upload a logo to replace the default site name and description in the footer',
	) );

	$wp_customize->add_setting( 'themeslug_logo_footer' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo_footer', array(
	    'label'    => __( 'Footer Logo', 'themeslug' ),
	    'section'  => 'themeslug_logo_footer_section',
	    'settings' => 'themeslug_logo_footer',
	) ) );
}
add_action( 'customize_register', 'vini_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function vini_customize_preview_js() {
	wp_enqueue_script( 'vini_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'vini_customize_preview_js' );
