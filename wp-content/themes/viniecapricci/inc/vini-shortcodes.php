<?php
/**
 * Add link buttons using shortcode
 * [button url="#" class="btn"] Show [/button]
 * @param  [type] $atts    [Replace target url and class name]
 * @param  [type] $content [Button label]
 * @return [type]          [description]
 */
add_shortcode('button', 'button_link');
function button_link( $atts, $content = null ) 
{
	extract(shortcode_atts(array(
		"url" => '#',
		"class" => 'btn-primary',
		"target" => 'blank'
	), $atts));

   return '<a href="'. $url .'" class="btn '. $class .'" target="'. '_' . $target .'">'. do_shortcode($content) .'</a>';
}

/**
 * Show Recent News and Events 
 * Order by Published Date in Descending order
 * shortcode: [show_recent_news]
 */

add_shortcode( 'show_recent_news','vini_show_recent_news' );
function vini_show_recent_news( ){

    $args = array(
            'post_type' => array( 'post', 'event' ),
            'post_status' => 'publish',
            'orderby' => 'date',
            'order'   => 'DESC',
            'posts_per_page' => 2,
        );

    $posts = new WP_Query( $args );

    if ( $posts->have_posts() ) :
        $output .= '<div class="box-recent-news">';
        $output .= '<ul class="recent-news-ul">'; 

        while ( $posts->have_posts() ) : $posts->the_post();

            $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
            // $content = get_the_content( );
            $content = wp_strip_all_tags( get_the_excerpt(), true );

            $output .= '<li class="recent-news-li">';
            $output .= '<a class="news-img" href="' . esc_url(get_permalink()) . '" style="background-image:url(\'' . $image[0]  . '\'); background-size:cover;">';
            
            $output .= '<div class="news-summary">';
            $output .= '<h3 class="news-title">'. get_the_title() .'</h3>';
                $excerpt = strip_tags( $content );
                if ( mb_strlen( $excerpt ) > 100 ) {
                   $excerpt  = mb_substr( $excerpt, 0 , 100 );
                }

                //If post type is 'event' show event date
                if ( get_post_type( get_the_ID() ) == 'event' ) {
                    $event_date = get_post_meta( get_the_ID() , 'mb_event_date', true );
                    $event_end_date = get_post_meta( get_the_ID() , 'mb_event_end_date', true );

                    if (!empty($event_date) && $event_end_date != $event_date )
                    {
                        $output .= '<br/>Start Date:' ;
                    }

                    if (!empty($event_date)) {
                        $output .= date('jS F Y', strtotime( $event_date ) );
                    }

                    if (!empty($event_end_date) && $event_end_date != $event_date) {
                        $output .= '<br/>End Date: ' .date('jS F Y', strtotime( $event_end_date ) );
                    }
                    
                }else{
                    $output .= $excerpt; 
                }
            
            $output .= '</div>';//news-summary
            $output .= '<a href="' . esc_url( get_permalink() ) . '" class="more-link">Read More</a>';
            $output .= '</a></li>';

        endwhile;

        $output .= '<li class="box-calendar recent-news-li" >';
        $output .= '<h3 class="news-title">Calendar of Events</h3><div id="calendar"></div><div class="clear"></div>';
        $output .= '<a href="' . home_url( 'news-events' ) . '" class="more-link">See All Events</a>';
        $output .= '</li>';

        $output .= '</ul>'; //recent-news-ul
        $output .= '</div>'; //box-recent-news
    endif;

   return $output;
}

/**
* Show posts
*/

add_shortcode( 'show_vini_posts','vini_show_posts' );
function vini_show_posts( $atts ) {


    extract( shortcode_atts(
        array(
            'type'=> 'event',
            'limit' => '-1',
            'category' => '1',
        ), $atts  ) );

    $atts['type'] = array_map( 'trim', str_getcsv( $atts['type'], ',' ) );
    $atts['category'] = array_map( 'trim', str_getcsv( $atts['category'], ',' ) );

    $args = array(
            'post_type' =>  $atts['type'],
            'post_status' => 'publish',
            'orderby' => 'date',
            'order'   => 'DESC',
            'posts_per_page' => $atts['limit'],
            'cat' => $atts['category']
        );

    $posts = new WP_Query( $args );


    if ( $posts->have_posts() ) :
        $output .= '<div class="widget-area-full"><div class="box-recent-news">';
        $output .= '<ul class="recent-news-ul">'; 

        while ( $posts->have_posts() ) : $posts->the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
            // $content = get_the_content( );
            $content = wp_strip_all_tags( get_the_excerpt(), true );

            $output .= '<li class="recent-news-li">';
            $output .= '<a class="news-img" href="' . esc_url(get_permalink()) . '" style="background-image:url(\'' . $image[0]  . '\'); background-size:cover;">';
            
            $output .= '<div class="news-summary">';
            $output .= '<h3 class="news-title">'. get_the_title() .'</h3>';
                $excerpt = strip_tags( $content );
                if ( mb_strlen( $excerpt ) > 100 ) {
                   $excerpt  = mb_substr( $excerpt, 0 , 100 );
                }

                //If post type is 'event' show event date
                if ( get_post_type( get_the_ID() ) == 'event' ) {
                    $event_date = get_post_meta( get_the_ID() , 'mb_event_date', true );
                    $output .= date('jS F Y', strtotime( $event_date ) );
                }else{
                    $output .= $excerpt; 
                }
            
            $output .= '</div>';//news-summary
            $output .= '<a href="' . esc_url( get_permalink() ) . '" class="more-link">Read More</a>';
            $output .= '</a></li>';

        endwhile;

        $output .= '</ul>'; //recent-news-ul
        $output .= '</div></div>'; //box-recent-news
    endif;
   return $output;
}