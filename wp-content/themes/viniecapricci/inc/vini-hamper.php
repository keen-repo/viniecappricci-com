<?php

add_action('init', 	'vini_hamper_init');
add_action( 'template_redirect', 'vini_template_redirect');

function vini_sub_account_pages() {

	return array('hamper-creator');

}

function vini_hamper_init() {

	$sap = vini_sub_account_pages();

	foreach ($sap as $page) {
		add_rewrite_endpoint($page, EP_PAGES);
	}

}

function vini_is_hamper_creator() {

	global $wp_query;

	if (is_page_template( 'page-templates/hamper.php' )) {

		$sap = vini_sub_account_pages();

		foreach ($sap as $page) {
			if (isset($wp_query->query_vars[$page])) {

				$step = $wp_query->query_vars[$page];

				if (in_array($step, array('step-1','step-4','step-5'))) {

					return $step;

				}

			}

		}

	}

	return false;

}

function vini_template_redirect() {

	global $wp_query;

	$sap = vini_sub_account_pages();

	$step = vini_is_hamper_creator();

	if ($step) {

		add_filter('body_class', 'vini_hamper_body_class');

		include get_template_directory() . '/template-parts/hamper/' . $step . '.php';

		exit();

	} else {

		/* We are not on my account page but lets make sure that no other page can access our endpoints */
		foreach ($sap as $page) {
			if (isset($wp_query->query_vars[$page])) {
				wp_redirect(get_permalink(), 301);
				exit();
			}
		}

	}

}

function vini_hamper_body_class($classes) {

	$step = vini_is_hamper_creator();

	$classes[] = 'in_hamper_mode';
	$classes[] = $step;

	if ($step === 'step-5') {
		$classes[] = 'woocommerce';
	}

	return $classes;

}

function vini_populate_hamper_add_to_cart_data($product) {

	$total_possible_items = (int)get_post_meta($product->id, 'total_possible_items', true);

	$aid = get_post_thumbnail_id($product->id);

	$thumbnail = wp_get_attachment_image_src($aid, 'shop_catalog');

	$response = array(
		'id' 			=> $product->id,
		'title' 		=> $product->get_title(),
		'sku' 			=> $product->get_sku(),
		'price' 		=> $product->get_price(),
		'thumbnail' 	=> str_replace( 'https://www.viniecapricci.com/wp-content/uploads/', '', $thumbnail[0]),
		'is_basket' 	=> false,
		'is_gift'		=> false,
		'basket_space' 	=> 0,
		'item_space' 	=> 0
	);

	$product_categories = wp_get_object_terms( $product->id,  'product_cat' );

	foreach ($product_categories as $category) {
		if ($category->slug == 'acces') {
			$response['is_gift'] = true;
		}
	}

	if ($total_possible_items) {
		$response['is_basket'] = true;
		$response['basket_space'] = $total_possible_items;
	} else {
		$hamper_space = (int)get_post_meta($product->id, 'hamper_space', true);

		if ($hamper_space < 1) $hamper_space = 1;

		$response['item_space'] = $hamper_space;
		
	}

	return $response;

}

add_action('woocommerce_after_shop_loop_item', 'vini_woocommerce_after_shop_loop_item');

function vini_woocommerce_after_shop_loop_item() {

	global $product;

	$response = vini_populate_hamper_add_to_cart_data($product);
	?>
	<script type="text/javascript">
		var productHamperData_<?php echo $product->id; ?> = <?php echo json_encode($response); ?>;
	</script>
	<?php

}


add_action('woocommerce_before_shop_loop', 'vini_woocommerce_before_shop_loop');

function vini_woocommerce_before_shop_loop() {
	require(get_template_directory() . '/template-parts/hamper/steps.php');
}

add_action('woocommerce_after_shop_loop', 'vini_woocommerce_after_shop_loop');

function vini_woocommerce_after_shop_loop() {
	if (is_tax('product_cat', 5495)) {
		?>
		<a href="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>" class="button">Next Step</a>
		<?php
	} else {
		?>
		<a href="<?php echo get_term_link(5495, 'product_cat'); ?>" class="button">Next Step</a>
		<?php
	}
}


add_filter('woocommerce_get_item_data', 'vini_woocommerce_get_item_data', 10, 2);

function vini_woocommerce_get_item_data($item_data, $cart_item){

	if (isset($cart_item['_is_hamper'])) {

		$item_data[] = array(
			'key' => 'Hamper Contents',
			'value' => $cart_item['hamper_contents']
		);

	}

	return $item_data;

}




add_action('woocommerce_add_order_item_meta','vini_woocommerce_add_order_item_meta',10,2);

function vini_woocommerce_add_order_item_meta($item_id, $values) {

	if(isset($values['_is_hamper'])) {

		wc_add_order_item_meta($item_id, '_is_hamper', 1);
		wc_add_order_item_meta($item_id, '_hamper_items', $values['_hamper_items']);
		wc_add_order_item_meta($item_id, 'hamper_contents', $values['hamper_contents']);

	}
	
}

add_action('woocommerce_attribute_label', 'vini_woocommerce_attribute_label', 10, 2);

function vini_woocommerce_attribute_label($label, $name) {

	if ($name == 'hamper_contents') {
		return 'Hamper Contents';
	}
	
	return $label;

}

/* Hamper Step 1 */
add_filter('woocommerce_product_add_to_cart_text', 'vini_woocommerce_product_single_add_to_cart_text');

function vini_woocommerce_product_single_add_to_cart_text($text) {

	if (vini_is_hamper_creator()) {
		return 'Select';
	}

	return $text;
}

/* catalogue flipbook shortcode */

function catalogue_func( $atts ){

	return '<iframe width="100%" height="500" src="http://abrahams.com.mt/hampers-catalogue-v2/" frameborder="0" scrolling="no"></iframe><div class="note">A recent communication has been issued with the incorrect visual representation of the <i>Moonstone Hamper</i>. We apologise for the inconvenience. Kindly view the <i>Moonstone Hamper</i> with the correct details on page 14.</div>';
}
add_shortcode( 'catalogue', 'catalogue_func' );

/**
 * Product Visibility hidden
 */
// add_filter('woocommerce_shortcode_products_query', 'vini_woocommerce_shortcode_products_query', 99, 3);
function vini_woocommerce_shortcode_products_query($query_args, $atts, $loop_name) {

	$step = vini_is_hamper_creator();

	if ($step == 'step-1') {
		$query_args['meta_query']['visibility']['value'][] = 'hidden';
	}

	return $query_args;

}


add_filter('woocommerce_product_is_visible', 'vini_woocommerce_product_is_visible');

function vini_woocommerce_product_is_visible($visible) {

	$step = vini_is_hamper_creator();

	if ($step == 'step-1') {
		$visible = true;
	}

	return $visible;

}



/* Using cart_item_data change the price */
add_action('woocommerce_before_calculate_totals', 'vini_woocommerce_before_calculate_totals' );

function vini_woocommerce_before_calculate_totals( $cart_object ) {

	global $woocommerce;

	foreach ( $cart_object->cart_contents as $key => $cart_item ) {

		if (isset($cart_item['_is_hamper'])) : 

			$total_price = 0;
			
			foreach ($cart_item['_hamper_items'] as $hamper_item) : 
				$total_price += $hamper_item['price'] * $hamper_item['quantity'];
			endforeach;

			$cart_item['data']->set_price($total_price);

		endif;
	}
}

/**
 * FINAL FIX: recalculate totals to show the correct price in checkout order received page too
 */
add_action( 'woocommerce_after_calculate_totals', 'vini_woocommerce_after_calculate_totals', 30 );
function vini_woocommerce_after_calculate_totals( $cart ) {
    
    if ( !empty( $cart->get_coupons() ) ) {

		$applied_coupon = $cart->get_coupons();

		foreach($applied_coupon as $coupon) : 
			$new_coupon = new WC_Coupon($coupon->code);
   	endforeach;

   	if ( $new_coupon->discount_type == 'percent_product' ) {
   		// start
   		$total_cart_discount = 0;
   		$discount = $new_coupon->amount / 100;

			foreach ($cart->cart_contents as $key => $cart_item ) : 
			
				$product = $cart_item['data'];
				$is_valid_coupon = $new_coupon->is_valid_for_product($product, $cart_item);


				if ( $cart_item['_is_hamper'] == 1) {

						$hamper_items_arr = [];
						$hamper_total_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
						$hamper_discount = 0;

						// Hamper Item cart
						foreach ( $cart_item['_hamper_items'] as $hamper_item ) : 

								$hamper_product = get_product( $hamper_item['id'] );
								$is_valid_coupon = $coupon->is_valid_for_product( $hamper_product );
								$total_hamper_item_amount = $hamper_product->get_price() * $hamper_item['quantity'];

								if ( $is_valid_coupon ) {
									$total_coupon_discount =  $total_hamper_item_amount * $discount;

									if ( $total_coupon_discount > 0 ) {
										$hamper_discount += $total_coupon_discount;
									}
								} 

						endforeach;

						if ( $hamper_discount > 0) {
							$total_hamper_discount = $hamper_discount * $cart_item['quantity'];
							$total_cart_discount += $total_hamper_discount;
						}

				} else {
					if ( $is_valid_coupon) : 

						$total_item_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
						$total_coupon_discount =  $total_item_amount * $discount ;

						$total_cart_discount += $total_coupon_discount;

					endif; // $is_valid_coupon
				}//is_hamper

			endforeach;

		
			$cart->coupon_discount_amounts = [];
			$cart->total = 0;
	
			$coupon_disc = array( $new_coupon->code => $total_cart_discount );
			$cart->coupon_discount_amounts = $coupon_disc;
			
			$cart->total = ( $cart->subtotal - $total_cart_discount ) + $cart->shipping_total + $cart->tax_total;
				

   	}//$new_coupon->discount_type == 'percent_product'


  }//!empty( $cart->get_coupons()

}

 

/**
 * FINAL FIX:  Cart Page  
 * Cart Item Subtotal if coupon type percent product is applied
 */

add_filter( 'woocommerce_cart_item_subtotal', 'calculate_cart_item_subtotal', 10, 2 );
function calculate_cart_item_subtotal( $subtotal, $cart_item ){
	global $woocommerce;

	// Note: use your own coupon code here 
	$coupon_code = $_POST['coupon_code'];
	$coupon = new WC_Coupon($coupon_code);

	$product = $cart_item['data'];

	switch ( $coupon->discount_type) {
 		case 'percent_product':
 			$discount = $coupon->amount / 100;
 	}

 	//Applies only if the coupon type is 'percent_product'
	if ( $woocommerce->cart->has_discount( $coupon_code ) && $coupon->discount_type == 'percent_product' ) {

		$is_valid_coupon = $coupon->is_valid_for_product($product, $cart_item);

		//Check if cart item is Hamper type of product
		if ($cart_item['_is_hamper'] == 1 ) {

			$hamper_items_arr = [];
			$hamper_total_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
			$hamper_discount = 0;

			// Hamper Item cart
			foreach ( $cart_item['_hamper_items'] as $hamper_item ) : 

					$hamper_product = get_product( $hamper_item['id'] );
					$is_valid_coupon = $coupon->is_valid_for_product( $hamper_product );
					$total_hamper_item_amount = $hamper_product->get_price() * $hamper_item['quantity'];

					if ( $is_valid_coupon ) {
						$total_coupon_discount =  $total_hamper_item_amount * $discount;

						if ( $total_coupon_discount > 0 ) {
							$hamper_discount += $total_coupon_discount;
							$newsubtotal = wc_price( $total_hamper_item_amount - $total_coupon_discount  );
							$hamper_items_arr[] = sprintf( $hamper_item['title'].': <s>%s</s> - %1s <span style="color: red;">Save: %2s</span>', wc_price( $total_hamper_item_amount ) , $newsubtotal , wc_price( $total_coupon_discount ) );
						}
			
					} else {
							$hamper_items_arr[] = sprintf( $hamper_item['title'].': %s', wc_price( $total_hamper_item_amount ) );
					}//$is_valid_coupon

			endforeach;
			

			if ( $hamper_discount > 0) {
				$total_hamper_discount = $hamper_discount * $cart_item['quantity'];
				$newsubtotal = wc_price(  $hamper_total_amount -  $total_hamper_discount );

				$subtotal = sprintf( '<s>%s</s> - %1s <span style="color: red">Save: %2s</span>', wc_price( $hamper_total_amount ), $newsubtotal, wc_price( $total_hamper_discount )  );


			}//$hamper_discount;
			
			return 'Hamper Price: <b>'. $subtotal  .'</b><br /><br />'. implode('<br />', $hamper_items_arr );
			
		} else {

			if ( $is_valid_coupon) : 

				$total_item_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
				$total_coupon_discount =  $total_item_amount * $discount ;

				//Discounted Cart Item
				$newsubtotal = wc_price( $total_item_amount - $total_coupon_discount  ); 
				$subtotal = sprintf( '<s>%s</s> - %1s <span style="color: red">Save: %2s</span> ', $subtotal, $newsubtotal , wc_price( $total_coupon_discount ) );

			endif; // $is_valid_coupon

			return $subtotal ;

		}//is_hamper


	} else {

		return $subtotal;
			 
	} // $woocommerce->cart->has_discount( $coupon_code )

}

//FINAL FIX:  Recalculate totals if percent product coupon code is applied and hamper items are present
add_filter( 'woocommerce_cart_subtotal', 'calculate_checkout_total_percent_product', 10, 3 );
function calculate_checkout_total_percent_product( $subtotal, $compound, $cart ) {

	if ( !empty( $cart->get_coupons() ) ) {

		$applied_coupon = $cart->get_coupons();

		foreach($applied_coupon as $coupon) : 
			$new_coupon = new WC_Coupon($coupon->code);
   	endforeach;

   	if ( $new_coupon->discount_type == 'percent_product' ) {
   		// start
   		$total_cart_discount = 0;
   		$discount = $new_coupon->amount / 100;

			foreach ($cart->cart_contents as $key => $cart_item ) : 
			
				$product = $cart_item['data'];
				$is_valid_coupon = $new_coupon->is_valid_for_product($product, $cart_item);


				if ( $cart_item['_is_hamper'] == 1) {

						$hamper_items_arr = [];
						$hamper_total_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
						$hamper_discount = 0;

						// Hamper Item cart
						foreach ( $cart_item['_hamper_items'] as $hamper_item ) : 

								$hamper_product = get_product( $hamper_item['id'] );
								$is_valid_coupon = $coupon->is_valid_for_product( $hamper_product );
								$total_hamper_item_amount = $hamper_product->get_price() * $hamper_item['quantity'];

								if ( $is_valid_coupon ) {
									$total_coupon_discount =  $total_hamper_item_amount * $discount;

									if ( $total_coupon_discount > 0 ) {
										$hamper_discount += $total_coupon_discount;
									}
								} 

						endforeach;

						if ( $hamper_discount > 0) {
							$total_hamper_discount = $hamper_discount * $cart_item['quantity'];
							$total_cart_discount += $total_hamper_discount;
						}

				} else {
					if ( $is_valid_coupon) : 

						$total_item_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
						$total_coupon_discount =  $total_item_amount * $discount ;

						$total_cart_discount += $total_coupon_discount;

					endif; // $is_valid_coupon
				}//is_hamper

			endforeach;

		
			$cart->coupon_discount_amounts = [];
			$cart->total = 0;
	
			$coupon_disc = array( $new_coupon->code => $total_cart_discount );
			$cart->coupon_discount_amounts = $coupon_disc;
			
			$cart->total = ( $cart->subtotal - $total_cart_discount ) + $cart->shipping_total + $cart->tax_total;
				

   	}//$new_coupon->discount_type == 'percent_product'


  }//!empty( $cart->get_coupons()
		
	return $subtotal ;

}


//Recalculate totals if coupon applied is a percent_product type
//add_action( 'woocommerce_cart_calculate_fees', 'calculate_total_percent_product', 30 );
function calculate_total_percent_product( $cart ) {

	global $woocommerce;
	$coupon_code = $_POST['coupon_code'];
	$coupon = new WC_Coupon($coupon_code);

	switch ( $coupon->discount_type) {
 		case 'percent_product':
 			$discount = $coupon->amount / 100;
 	}

	if ( $woocommerce->cart->has_discount( $coupon_code ) && $coupon->discount_type == 'percent_product'  ) {

		$total_cart_discount = 0;

		foreach ($cart->cart_contents as $key => $cart_item ) : 
		
			$product = $cart_item['data'];
			$is_valid_coupon = $coupon->is_valid_for_product($product, $cart_item);

			if ( $cart_item['_is_hamper'] == 1) {

					$hamper_items_arr = [];
					$hamper_total_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
					$hamper_discount = 0;

					// Hamper Item cart
					foreach ( $cart_item['_hamper_items'] as $hamper_item ) : 

							$hamper_product = get_product( $hamper_item['id'] );
							$is_valid_coupon = $coupon->is_valid_for_product( $hamper_product );
							$total_hamper_item_amount = $hamper_product->get_price() * $hamper_item['quantity'];

							if ( $is_valid_coupon ) {
								$total_coupon_discount =  $total_hamper_item_amount * $discount;

								if ( $total_coupon_discount > 0 ) {
									$hamper_discount += $total_coupon_discount;
								}
							} 

					endforeach;

					if ( $hamper_discount > 0) {
						$total_hamper_discount = $hamper_discount * $cart_item['quantity'];
						$total_cart_discount += $total_hamper_discount;
					}

			} else {
				if ( $is_valid_coupon) : 

					$total_item_amount = $cart_item['data']->get_price() * $cart_item['quantity'];
					$total_coupon_discount =  $total_item_amount * $discount ;

					$total_cart_discount += $total_coupon_discount;

				endif; // $is_valid_coupon
			}//is_hamper
		endforeach;

	}
}


?>
