<?php

$prefix = '_vini_';

require get_template_directory() . '/inc/vini-meta.php';
require get_template_directory() . '/inc/vini-ajax.php';
require get_template_directory() . '/inc/vini-shortcodes.php';
require get_template_directory() . '/inc/vini-hamper.php';
require get_template_directory() . '/inc/vini-wine-packaging.php';

add_image_size( 'post-thumbnail-large', 1400, 315, true);
add_image_size( 'post-thumbnail-medium', 525, 345, true);
add_image_size( 'recent-news-thumbnail', 800, 800, true);

/**
 * Change Post label in Admin Menu
 * @return [type] [description]
 */
function vini_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
/**
 * Change Post object in Admin Menu
 * @return [type] [description]
 */
function vini_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'vini_change_post_label' );
add_action( 'init', 'vini_change_post_object' );

/**
 * Enqueue map-js only in partner page
 * @return [type] [description]
 */
function enq_mapjs_in_partner_page( )
{
   if ( is_post_type_archive('partners') ) {
        wp_enqueue_script( 'map-js', TEMPL_DIR . '/js/map.js', array('jquery'), false , true );

        $temp_dir = array( 'stylesheet_uri' => TEMPL_DIR );
        wp_localize_script( 'map-js', 'template_dir', $temp_dir );

    }
}
add_action( 'wp_enqueue_scripts', 'enq_mapjs_in_partner_page' );

/**
 * Remove empty p tags
 * https://gist.github.com/ninnypants/1668216
 *
 */
function remove_empty_p( $content ){
    // clean up p tags around block elements
    $content = preg_replace( array(
        '#<p>\s*<(div|aside|section|article|header|footer)#',
        '#</(div|aside|section|article|header|footer)>\s*</p>#',
        '#</(div|aside|section|article|header|footer)>\s*<br ?/?>#',
        '#<(div|aside|section|article|header|footer)(.*?)>\s*</p>#',
        '#<p>\s*</(div|aside|section|article|header|footer)#',
    ), array(
        '<$1',
        '</$1>',
        '</$1>',
        '<$1$2>',
        '</$1',
    ), $content );
    return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);
}
add_filter( 'the_content', 'remove_empty_p', 20, 1 );


/**
 * Add Cart icon and count to header if Woocommerce is active
 */
function sws_wc_cart_items_count() {
 
    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
        ?><a class="cart-contents " href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
        if ( $count > 0 ) {
            ?>
            <span class="cart-contents-count fade-in"><?php echo esc_html( $count ); ?></span>
            <?php
        }
                ?></a><?php
    }
 
}
add_action( 'your_theme_header_top', 'sws_wc_cart_items_count' );

/**
 * Update cart counts when products are added to cart via AJAX
 */
function sws_wc_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count fade-in"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'sws_wc_add_to_cart_fragment' );


/*
* Replace default image path
*
**/
add_action( 'init', 'vini_custom_prodcut_thumbnail' );
 
function vini_custom_prodcut_thumbnail() {
  add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
   
    function custom_woocommerce_placeholder_img_src( $src ) {
        $upload_dir = wp_upload_dir();
        $uploads = untrailingslashit( $upload_dir['baseurl'] );
        $src = $uploads . '/2017/02/vini-image-placeholder.png';

        return $src;
    }
}

/**
 * Hide hidden products from Wordpress search results
 */
if ( ! function_exists( 'sws_hidden_products_search_result' ) ){
  function sws_hidden_products_search_result( $query = false ) {
    if( !is_admin() && is_search() && $query->is_main_query()){
	  $query->set( 'post_type', 'product' );
	  $query->set( 'posts_per_page', 50 );  	
	  $query->set( 'orderby', 'title' );
      $query->set( 'order', 'ASC' );
      $query->set( 'meta_query', array(
        'relation' => 'OR',
        array(
          'key' => '_visibility',
          'value' => 'hidden',
          'compare' => 'NOT EXISTS',
        ),
        array(
          'key' => '_visibility',
          'value' => 'hidden',
          'compare' => '!=',
        ),
      ));
    }
  }
}
add_action( 'pre_get_posts', 'sws_hidden_products_search_result' );


/**
 * Create json data of events to show in calendar
 */
add_action( 'wp_ajax_ajax_request', 'ajax_request' );
add_action( 'wp_ajax_nopriv_ajax_request', 'ajax_request' );

function ajax_request() {

    // $arr_data = array();
    $response   = array();

    global $wpdb;
    $events = $wpdb->get_results( 
            "SELECT $wpdb->postmeta.*,  $wpdb->posts.*
            FROM ( $wpdb->posts
            INNER JOIN $wpdb->postmeta
            ON $wpdb->posts.ID = $wpdb->postmeta.post_id)
            WHERE $wpdb->posts.post_type LIKE 'event' 
            AND $wpdb->posts.post_status LIKE 'publish'
            AND $wpdb->postmeta.meta_key LIKE 'mb_event_date'
            ORDER BY $wpdb->postmeta.meta_value DESC"
        );

    foreach ($events as $key => $event) {
        $id = $event->post_id;
        $event_date = rwmb_meta('mb_event_date','type=date', $id);
        $convert_event_date =  strtotime($event_date);
        $format_event_date = date('Y-m-d', $convert_event_date). 'T08:00:00';

        $end_date = rwmb_meta('mb_event_end_date','type=date', $id);
        if (!empty($end_date)) {
            $convert_event_end_date =  strtotime($end_date);
            $format_event_end_date = date('Y-m-d', $convert_event_end_date).'T23:00:00';
        }else {
            $format_event_end_date = '';
        }

        $response[] = array(
            // 'allDay' => false,
            'title' => $event->post_title,
            'start' => $format_event_date,
            'end' => $format_event_end_date,
            'url'=> get_permalink($id),
            'backgroundColor' => '#a22b56',
        );
    }

  wp_send_json($response);
  exit;

}

/**
 * Hide Product Categories that are set to hidden
 */
add_filter( 'get_terms', 'get_product_category', 10, 3 );
function get_product_category( $terms, $taxonomies, $args ) {
    
    $new_terms  = array();
    $hidden_cat_ids  = array(); // Ids of the category you don't want to display on the shop page

    foreach ($terms as $key => $term) : 
        $isHidden = rwmb_meta( 'product_category_show_hide', array( 'object_type' => 'term' ), $term->term_id );

        if ( $isHidden) {
            array_push( $hidden_cat_ids ,$term->term_id );
        }
    endforeach;
    
    
    if ( in_array( 'product_cat', $taxonomies ) && !is_admin() && is_shop() ) {

        foreach ( $terms as $key => $term ) {

        if ( ! in_array( $term->term_id, $hidden_cat_ids ) ) { 
            $new_terms[] = $term;
        }
        }
        $terms = $new_terms;
    }
  return $terms;
}

/**
 * Add a Hide checkbox on product category 
 */
add_filter( 'rwmb_meta_boxes', 'product_register_meta_boxes' );    
function product_register_meta_boxes( $meta_boxes )
{
    $prefix = 'product_';

    $meta_boxes[] = array(
        'id'         => 'mb_product_category_show_hide',
        'title'      => __( 'Hide Category', 'vini' ),
        'post_types' => array( 'product'),
        'taxonomies' => array('product_cat'),  //'taxonomy'   => array( 'product_cat'),
        'context'    => 'normal',
        'priority'   => 'low',
        'fields'     => array(
            array(
                    'id' => $prefix . 'category_show_hide',
                    'type' => 'checkbox',
                    'desc' => esc_html__( 'Tick the checkbox to hide this category', 'text-domain' ),
                ),

            )
    );  

    return $meta_boxes;
} 

