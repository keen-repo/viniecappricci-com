<?php


add_filter( 'rwmb_meta_boxes', 'vini_register_meta_boxes' );

function vini_register_meta_boxes( $meta_boxes ) {

    global $prefix;

   //Hamper Pages Banner
    $meta_boxes[] = array(
        'title'  => __( 'Hamper Page Banner', THEMELANG ),
        'include' => array(
            'relation'        => 'OR',
            'slug'            => array( 'hamper' ),
            'custom'          => 'manual_include',
        ),
        'post_types' => array( 'page' ),
        'autosave' => true,
        'fields' => array(
            array(
                'id'   =>  $prefix.'banner_hamper_image',
                'name' => __( 'Upload Image(s)', THEMELANG ),
                // 'clone'   => true,
                'type' => 'image_advanced',
                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,
                // Maximum image uploads
                'max_file_uploads' => 1,
            ),
        ),
    );


    $meta_boxes[] = array(
        'id' => 'meta_front_page',
        'title' => esc_html__( 'About Us', 'vini' ),
        'post_types' => array('page' ),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => $prefix . 'about_section_title',
                'type' => 'text',
                'name' => esc_html__( 'About', 'vini' ),
            ),
            array(
                'id' => $prefix . 'about_intro_text',
                'type' => 'textarea',
                'name' => esc_html__( 'Introduction Text', 'vini' ),
            ),

            // array(
            //     'name'             => 'Video',
            //     'id'               => $prefix. 'about_video',
            //     'type'             => 'video',
            //     'max_file_uploads' => 1,
            //     'force_delete'     => false,
            //     'max_status'       => true,
            // ),

             array(
                'name'             => 'Video Link',
                'id'               => $prefix. 'about_video_url',
                'type'             => 'oembed',
            ),

        ),
        'include' => array(
            'template'        => array( 'front-page.php')
        ) 
    );

    $meta_boxes[] = array(
        'id' => 'text_banner_ads',
        'title' => esc_html__( 'Text Banner Ads', 'vini' ),
        'post_types' => array('page' ),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => $prefix . 'text_ads',
                'type' => 'textarea',
                'name' => esc_html__( 'Description', 'vini' ),
            ),

        ),
        'include' => array(
            'template'        => array( 'front-page.php')
        ) 
    );


    $meta_boxes[] = array(
        'id' => 'meta_site_page',
        'title' => esc_html__( 'Site Meta', 'vini' ),
        'post_types' => array('page' ),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => 'false',
        'fields' => array(
            array(
                'id' => $prefix . 'fm_delivery_header',
                'type' => 'textarea',
                'name' => esc_html__( 'Delivery Text in Header', 'vini' ),
            ),
            array(
                'id' => $prefix . 'fm_membership',
                'type' => 'textarea',
                'name' => esc_html__( 'Membership Card', 'vini' ),
            ),

            array(
                'id' => $prefix . 'fm_delivery',
                'type' => 'textarea',
                'name' => esc_html__( 'Delivery', 'vini' ),
            ),

            array(
                'id' => $prefix . 'fm_call',
                'type' => 'textarea',
                'name' => esc_html__( 'Contact Info', 'vini' ),
            ),

            array(
                'id' => $prefix . 'fm_insta',
                'type' => 'url',
                'name' => esc_html__( 'Instagram Link', 'vini' ),
            ),

            array(
                'id' => $prefix . 'fm_facebook',
                'type' => 'url',
                'name' => esc_html__( 'Facebook Link', 'vini' ),
            ),

           

        ),
        'include' => array(
            'template'        => array( 'front-page.php')
        ) 
    );


    return $meta_boxes;
}


add_filter( 'rwmb_meta_boxes', 'concept_register_meta_boxes' );

function concept_register_meta_boxes( $meta_boxes ) {
    $prefix = 'concept_';

    $meta_boxes[] = array (
        'title' => esc_html__( 'Gallery', 'vini' ),
        'id' => 'gallery',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'fields' => array(
            array (
                'id' => $prefix . 'gallery',
                'type' => 'image_advanced',
                'name' => esc_html__( 'Upload Images', 'vini' ),
                'max_status' => false,
            ),
        ),
        'text_domain' => 'vini',
        'include' => array(
            'relation' => 'OR',
            'parent' => array(
                0 => 67127,
            ),
        ),
    );

   

    return $meta_boxes;
}
?>