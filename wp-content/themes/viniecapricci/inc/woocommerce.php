<?php

/**
 * Change "Default Sorting" to "Sort By" on Shop Page
 * 
 */
function vini_change_default_sorting_name( $catalog_orderby ) {
    $catalog_orderby = str_replace("Default sorting", "Sort By", $catalog_orderby);
    return $catalog_orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'vini_change_default_sorting_name' );
// add_filter( 'woocommerce_default_catalog_orderby_options', 'vini_change_default_sorting_name' );


add_action( 'woocommerce_before_shop_loop', 'woocommerce_output_content_wrapper', 20 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_output_content_wrapper_end', 20 );

/**
 * Add wrapper to product image thumbnail
 */

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    }
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $woocommerce;
        

        if ( has_post_thumbnail() ) {
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'large' );
           $output = '<div class="wrap-product-thumbnail" style="background-image:url(' .$image[0]. ');background-size:contain; background-repeat:no-repeat; background-position:center;">';
        }else{
           $output = '<div class="wrap-product-thumbnail" style="background-image:url('. get_bloginfo('template_directory').'/assets/build/images/vini-image-placeholder.png);background-size:contain; background-repeat:no-repeat; background-position:center;">';

            // $output .= '<img src="'. get_bloginfo('template_directory').'/assets/build/images/vini-image-placeholder.png" alt="Vini e Cappricci" />';
        }
        $output .= '</div>';
        return $output;
    }
}

/**
 * Custom  template single title
 * Wrap title in a div
 */
function vini_woocommerce_template_single_title() {
    global $product;
    if($product->id!="77831"){
        $output = '<div class="box-woo-two-column section group"><div class="wrap-title col span_1_of_2">';
        $output .= '<h1 itemprop="name" class="product-title" >' . get_the_title() . '</h3>';
        $output .= '</div>';

        echo $output;
    }
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'vini_woocommerce_template_single_title', 5 );

/**
 * Custom template single price
 */
function vini_woocommerce_template_single_price(){
    global $product;
    if($product->id!="77831"){
    $output = '<div class="wrap-price col span_1_of_2">';
	$output .= '<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
		$output .= '<div class="price">' . $product->get_price_html() . '</div>';
		$output .= '<meta itemprop="price" content="'.esc_attr( $product->get_display_price() ).'" />';
		$output .= '<meta itemprop="priceCurrency" content="'.esc_attr( get_woocommerce_currency() ).'" />';
	$output .= '</div>';
    echo $output;
    }
}

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'vini_woocommerce_template_single_price', 10 );

/**
 * Custom template single add to cart button with quantity input
 */
function vini_woocommerce_template_single_add_to_cart(){
    global $product;
	if($product->id!="77831"){
    if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
        $output = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
        $output .= woocommerce_quantity_input( array(), $product, false );

        $output .= '<button type="submit" data-product_id="' . $product->id . '" data-quantity="1" class="button add_to_cart_button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';

        $response = vini_populate_hamper_add_to_cart_data($product);

        $output .= '<script type="text/javascript">';
        $output .= 'var productHamperData_' . $product->id . ' = ' . json_encode($response) . ';';
        $output .= '</script>';

        $output .= '</form>';
    }else{
        echo '<div class="stock out-of-stock"><span>Out of Stock</span></div>';
    }
    echo $output . '</div></div>';
	}else{
		do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' );
	}
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'vini_woocommerce_template_single_add_to_cart', 30 );

/**
 * Remove Default Product Tabs 
 * Then Output Description , SKU and Additional Information but not inside Tab
 * 
 */
function vini_woocommerce_template_product_description() {
    global $product;
    echo '<div class="box-woo-two-column"><div class="description col span_1_of_2">';
    woocommerce_get_template( 'single-product/tabs/description.php' );
    if ( !empty( $product->get_sku() ) ) {
        echo '<p class="sku"> SKU: ' . $product->get_sku() . '</p>';
    }
    echo '</div>';
    // echo '<div class="addtl-info col span_1_of_2">';
    // woocommerce_get_template( 'single-product/tabs/additional-information.php' );
    // echo '</div>';
    echo '</div>';

}
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs',  10 );
add_action( 'woocommerce_single_product_summary', 'vini_woocommerce_template_product_description', 35 );

/**
 * Change max number of products to show and how many columns in related products section
 * 
 */
function vini_change_related_products_args( $args ) {
    $args['posts_per_page'] = 6; 
    $args['columns'] = 6;
    return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'vini_change_related_products_args' );




/**
 * Remove product single meta
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * Remove Rating and Popularity options
 * @param  [type] $orderby [description]
 * @return [type]          [description]
 */
function vini_woocommerce_catalog_orderby( $orderby ) {
    unset($orderby["rating"]);
    unset($orderby["popularity"]);
    return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "vini_woocommerce_catalog_orderby", 20 );

/**
 * Change "Related Products" text
 */
function vini_change_text_related_products( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Related Products' :
            $translated_text = __( 'You might also be interested in', 'vini' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'vini_change_text_related_products', 20, 3 );



/** Changed the title for SEO Purposes **/
remove_action( 'woocommerce_shop_loop_item_title',     'woocommerce_template_loop_product_title', 10 );
add_action   ( 'woocommerce_shop_loop_item_title',   'woocommerce_template_loop_product_title', 10 );

function woocommerce_template_loop_product_title() {
		echo '<h3 itemprop="name">' . get_the_title() . '</h3>';
		echo '<div itemprop="description" class="product-desc-hidden">';
			echo get_the_content();
		echo '</div>';
}

/* Add additional shipping fields (email, phone) in FRONT END (i.e. My Account and Order Checkout) */
/* Note:  $fields keys (i.e. field names) must be in format: "shipping_" */
add_filter( 'woocommerce_shipping_fields' , 'my_additional_shipping_fields' );
function my_additional_shipping_fields( $fields ) {
    $fields['shipping_phone'] = array(
        'label'         => __( 'Recipient\'s Phone', 'woocommerce' ),
        'required'      => true,
        'class'         => array( 'form-row-first' ),
        'clear'         => true,
        'validate'      => array( 'phone' ),
    );
    return $fields;
}
/* Display additional shipping fields (email, phone) in ADMIN area (i.e. Order display ) */
/* Note:  $fields keys (i.e. field names) must be in format:  WITHOUT the "shipping_" prefix (it's added by the code) */
add_filter( 'woocommerce_admin_shipping_fields' , 'my_additional_admin_shipping_fields' );
function my_additional_admin_shipping_fields( $fields ) {
        $fields['phone'] = array(
            'label' => __( 'Order Ship Phone', 'woocommerce' ),
        );
        return $fields;
}
/* Display additional shipping fields (email, phone) in USER area (i.e. Admin User/Customer display ) */
/* Note:  $fields keys (i.e. field names) must be in format: shipping_ */
add_filter( 'woocommerce_customer_meta_fields' , 'my_additional_customer_meta_fields' );
function my_additional_customer_meta_fields( $fields ) {
        $fields['shipping']['fields']['shipping_phone'] = array(
            'label' => __( 'Telephone', 'woocommerce' ),
            'description' => '',
        );
        return $fields;
}
/* Add CSS for ADMIN area so that the additional shipping fields (email, phone) display on left and right side of edit shipping details */
add_action('admin_head', 'my_custom_admin_css');
function my_custom_admin_css() {
  echo '<style>
    #order_data .order_data_column ._shipping_phone_field {
        float: left;
    }
  </style>';
}

/* Add additional shipping fields in order email */
add_filter('woocommerce_email_order_meta', 'my_additional_order_meta', 10, 3 );
function my_additional_order_meta( $order_obj, $sent_to_admin, $plain_text ) {

    $ship_phone = get_post_meta( $order_obj->id, '_shipping_phone', true );

    if ( $plain_text === false ) {
 
        echo '<h2>Order details</h2>
        <ul>
        <li><strong>Order Ship Phone:</strong> '. $ship_phone .'</li>
        </ul>';
 
    } else {
 
        echo "GIFT INFORMATION\n
        Order Ship Phone: $ship_phone";   
 
    }
}


/**
 * Exclude products from a hamper basket category on the shop page
 */
function custom_pre_get_posts_query( $q ) {

    if ( !is_admin()  && $q->is_main_query() ) 
    {
        if ( is_shop() || is_product_category() ) {
            $tax_query = (array) $q->get( 'tax_query' );

            $tax_query[] = array(
                   'taxonomy' => 'product_cat',
                   'field' => 'slug',
                   'terms' => array( 'hamper-gift-boxes' ),
                   'operator' => 'NOT IN'
            );

            $q->set( 'tax_query', $tax_query );

        }
    }

}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' ); 

/* Re-order ordering section */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action( 'woocommerce_archive_description', 'woocommerce_catalog_ordering', 30 );

/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
  global $product;
    
    $args['posts_per_page'] = 6;
    return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'vini_change_num_related_posts', 20 );
  function vini_change_num_related_posts( $args ) {
    $args['posts_per_page'] = 5; 
    $args['columns'] = 5;
    return $args;
}

add_filter( 'woocommerce_order_button_html', 'vini_custom_button_html' );
 
function vini_custom_button_html( $button_html ) {
    $button_html = str_replace( 'Place order', 'Confirm and Pay', $button_html );
    return $button_html;
}

/**
 * Change SALE text
 */
add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
function woocommerce_custom_sale_text($text, $post, $_product)
{
    //return '<div class="box"><div class="ribbon ribbon-top-right"><span>Sale</span></div></div>';
    return;
}

/**
 * Badge on New and Best Seller Products
 */

//add_action( 'woocommerce_before_shop_loop_item_title', 'add_badge_new', 3 );
          
function add_badge_new() {

   global $product;
   $meta = get_post_meta( $product->id );

   if($meta['product_badge_new'][0] == 1 ) 
   {
      echo '<span class="new onsale">' . esc_html__( 'New', 'woocommerce' ) . '</span>';
   }
   elseif($meta['product_badge_bestseller'][0] == 1 ) 
   {
    echo '<span class="bestseller onsale">' . '<span class="best">Best</span><span class="seller">Seller</span></span>';
   }

}

/**
* Change number of products that are displayed per page (shop page)
*/

add_filter( 'loop_shop_per_page', 'no_of_products_on_shop_page', 20 );
function no_of_products_on_shop_page( $cols ) 
{
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 20;

    return $cols;
}

/**
 *  WooCommerce admin order email 
 *  Add customer information: [Phone,email]
 */
function woo_order_email_customer_details( $order )
{
    $phone= $order->get_billing_phone();
    $email=$order->get_billing_email();
    $notes = $order->get_customer_note();
    ?>

    <h2><?php _e( 'Customer details', 'woocommerce' ); ?></h2>
    <ul>
        <li><strong><?php echo wp_kses_post( 'Email address'); ?>:</strong> <span class="text"><?php echo wp_kses_post( $email ); ?></span></li>
        <!-- Phone -->
        <?php if($phone) { ?>
         <li><strong><?php echo wp_kses_post( 'Phone'); ?>:</strong> <span class="text"><?php echo wp_kses_post( $phone ); ?></span></li>
        <?php } ?>

        <!-- Customer Notes -->
        <?php if($notes) { ?>
         <li><strong><?php echo wp_kses_post( 'Customer Notes'); ?>:</strong> <span class="text"><?php echo wp_kses_post( $notes ); ?></span></li>
        <?php } ?>
     </ul>
    <?php
};

add_action( 'woocommerce_email_customer_details', 'woo_order_email_customer_details', 10, 4 );

/**
 * Change Woocommerce State Label
 * */
function woo_change_label_state( $fields ) 
{
   $fields['state']['label'] = 'State <i>(Kindly select Malta or Gozo for delivery rate purposes.)</i>';
    $fields['state']['required'] = true;
    return $fields;

}
add_filter( 'woocommerce_default_address_fields' , 'woo_change_label_state', 9999 );

// // PHP: Remove "(optional)" from our non required fields
add_filter( 'woocommerce_form_field' , 'remove_checkout_optional_fields_label', 10, 4 );
function remove_checkout_optional_fields_label( $field, $key, $args, $value ) {
    // Only on checkout page
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
        $field = str_replace( $optional, '', $field );
    }
    return $field;
}

// JQuery: Needed for checkout fields to Remove "(optional)" from our non required fields
add_filter( 'wp_footer' , 'remove_checkout_optional_fields_label_script' );
function remove_checkout_optional_fields_label_script() {
    // Only on checkout page
    if( ! ( is_checkout() && ! is_wc_endpoint_url() ) ) return;

    $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
    ?>
    <script>
    jQuery(function($){
        // On "update" checkout form event
        $(document.body).on('update_checkout', function(){
           $('#billing_state_field label > .optional').text('*').removeClass('optional').addClass('required');
           $('#shipping_state_field label > .optional').text('*').removeClass('optional').addClass('required');
        });
    });
    </script>
    <?php
}

/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function woo_hide_shipping_when_free_is_available( $rates ) 
{
    $free = array();
    foreach ( $rates as $rate_id => $rate ) {
        if ( 'free_shipping' === $rate->method_id ) {
            $free[ $rate_id ] = $rate;
            break;
        }
    }
    return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'woo_hide_shipping_when_free_is_available', 100 );
