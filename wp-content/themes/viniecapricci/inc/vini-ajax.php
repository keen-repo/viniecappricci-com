<?php


add_action( 'wp_ajax_vini_add_hamper_to_cart', 			'vini_add_hamper_to_cart' );
add_action( 'wp_ajax_nopriv_vini_add_hamper_to_cart', 	'vini_add_hamper_to_cart' );

function vini_add_hamper_to_cart() {

	$state = $_POST['state'];

	// print_r($state); die();

	if (!empty($state['basket']) && !empty($state['products'])) {

		$cart_item_data = array(
			'_is_hamper' => true, 
			'_hamper_items' => array(), 
			'hamper_contents' => ''
		);

		$hamper_contents = array();

		foreach ($state['products'] as $product) {

			$cart_item_data['_hamper_items'][] = array(
				'id' => $product['id'],
				'title' => stripslashes($product['title']),
				'price' => $product['price'],
				'quantity' => $product['quantity'],
			);

			$hamper_contents[] = stripslashes($product['title']) . ' ( x' . $product['quantity'] .' )';

		}

		// Add basket as well
		$cart_item_data['_hamper_items'][] = array(
			'id' => $state['basket']['id'],
			'title' => stripslashes($state['basket']['title']),
			'price' => $state['basket']['price'],
			'quantity' => 1,
		);

		$cart_item_data['hamper_contents'] = implode('<br />', $hamper_contents);

		WC()->cart->add_to_cart($state['basket']['id'], 1, 0, array(), $cart_item_data);

		$result = array('success' => true);

	} else {

		$result = array('success' => false);

	}

	echo json_encode($result);
	exit();
    
}

add_action( 'wp_ajax_vini_add_set_hamper_row', 			'vini_add_set_hamper_row' );
add_action( 'wp_ajax_nopriv_vini_add_set_hamper_row', 	'vini_add_set_hamper_row' );

function vini_add_set_hamper_row() {

	global $wpdb;

	$state_id = $_POST['state_id'];

	if(isset($_POST['state'])){
		$state = json_encode(str_replace( "\\", '', $_POST['state']));
	} else {
		$state = '{}';
	}

	// check if item exists
	$row = $wpdb->get_row( "SELECT * FROM `wp_vec_custom_hampers` WHERE id = '".$state_id."'" );

	if($row == NULL){
		$wpdb->insert( 
			'wp_vec_custom_hampers', 
			array( 
				'id' => $state_id, 
				'state' => $state
			), 
			array( 
				'%s', 
				'%s' 
			) 
		);
	} else {
		$wpdb->update( 
			'wp_vec_custom_hampers', 
			array( 
				'state' => $state
			), 
			array( 'id' => $state_id ),
			array( 
				'%s' 
			) 
		);
	}

	

	wp_die();
}

add_action( 'wp_ajax_vini_get_hamper_row', 			'vini_get_hamper_row' );
add_action( 'wp_ajax_nopriv_vini_get_hamper_row', 	'vini_get_hamper_row' );

function vini_get_hamper_row() {

	global $wpdb;

	$state_id = $_POST['state_id'];

	$row = $wpdb->get_row( "SELECT * FROM `wp_vec_custom_hampers` WHERE id = '".$state_id."'" );
	echo json_encode($row);

	wp_die();
}

add_action( 'wp_ajax_vini_remove_hamper_row', 		'vini_remove_hamper_row' );
add_action( 'wp_ajax_nopriv_vini_remove_hamper_row', 	'vini_remove_hamper_row' );

function vini_remove_hamper_row() {

	global $wpdb;

	$state_id = $_POST['state_id'];
	
	$wpdb->delete( 'wp_vec_custom_hampers', array( 'id' => $state_id ) );
	echo 'Deleted...';

	wp_die();
}