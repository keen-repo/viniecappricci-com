<?php

$endeavour_settings = get_option( 'woocommerce_endeavour_settings' );

$redirect_page_id = $endeavour_settings['redirect_page_id'];

$remote_addr = $_SERVER['HTTP_X_REAL_IP'];
		
$identifier = $_REQUEST['Identifier'];

//$headers = 'From: Developer <developer@keen.com.mt>' . "\r\n";
wp_mail('developer@keen-advertising.com', 'VINI_END_RESP', 'running response<br />' . json_encode($_SERVER) . '<br />' . json_encode($_REQUEST) );

global $woocommerce;

$order = new WC_Order($identifier);

//if (isset($_REQUEST['AcceptReject'])) {
if (strpos($remote_addr, '213.95.27') !== false) {

	$transauthorised = false;
	
	wp_mail('developer@keen-advertising.com', 'first Server Call', json_encode($_REQUEST), $headers );

	if($order->status !=='completed'){

		if($_REQUEST['AcceptReject'] == 'T'){

			$transauthorised = true;

			$message = "Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon.";
			$class = 'woocommerce_message';

			if($order->status == 'processing'){

			}else{
				
				//wp_mail('developer@keen-advertising.com', 'complete Server Call', 'booking' . $_REQUEST['Identifier'] . ' ' . $_REQUEST['AcceptReject'] . ' IP ' . $remote_addr, $headers );
				$order->payment_complete($_REQUEST['ReceiptCode']);
				$order->add_order_note('Endeavour payment successful<br/>Unnique Id from Endeavour: '.$_REQUEST['ReceiptCode']);
				$order->add_order_note($message);
				
			}
		} else {
			$class = 'woocommerce_error';
			$message = "Thank you for shopping with us. However, the transaction has been declined.";
			$order->add_order_note('Transaction Declined');
			//Here you need to put in the routines for a failed
			//transaction such as sending an email to customer
			//setting database status etc etc
		}

		 if($transauthorised==false){
			$order->update_status('failed');
			$order->add_order_note('Failed');
			$order->add_order_note($message);
		}

	}

	//wp_mail('developer@keen-advertising.com', 'Server Call', 'booking' . $_REQUEST['Identifier'] . ' ' . $_REQUEST['AcceptReject'] . ' IP ' . $remote_addr, $headers );

} else {

	if($_REQUEST['AcceptReject'] == 'T'){
		
		$woocommerce->cart->empty_cart();

		$message = "Thank you for shopping with us. Your account has been charged and your transaction is successful. We will be shipping your order to you soon.";
		$class = 'woocommerce_message';

	} else {
		
		$class = 'woocommerce_error';
		$message = 'Thank you for shopping with us. However, the transaction has been declined. <a href="javascript:;" onclick="window.top.location.reload(true);">click here to retry.</a>';

	}

}
?>
<!doctype html>
<html lang="en">
<head>
	<link rel='stylesheet' id='vec-philosopher-css'  href='https://fonts.googleapis.com/css?family=Philosopher%3A400%2C700%2C400italic%2C700italic&#038;subset=latin%2Ccyrillic&#038;ver=3.5.2' type='text/css' media='all' />

	<meta charset="utf-8">
	<meta name="robots" content="noindex, follow">
	<title>Payment Form</title>
	
	<style type="text/css">
		body {
			font: 15px 'Philosopher',sans-serif;
		}
		
		a {
			color: #8D0E3A;
			text-decoration: none;
		}
		
		a:hover {
			text-decoration: underline;
		}
	</style>

	<?php if($_REQUEST['AcceptReject'] == 'T'){ ?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-NXR429M');</script>
		<!-- End Google Tag Manager -->
		<script>
			window.dataLayer = window.dataLayer || [];
				dataLayer.push({
				'transactionId': $order->id,
				'transactionTotal': $order->get_total()
			});
		</script>
	<?php } ?>

</head>
<body>
	<?php
	if($_REQUEST['AcceptReject'] == 'T') {

		$redirect_url = get_permalink(14);

		?>
		<script type="text/javascript">
			window.top.location.href = '<?php echo $redirect_url; ?>'; 
		</script>
		<?php
		exit();

	}
	?>
	<span><?php echo $message; ?></span>
</body>
</html>