jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyBJke8awiiax4MfVDhap0N1eW-4vG2gIoo&callback=setCurrentTab";
    //script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyB-IR0wNdQ7AD7G7k96ww9CE0iXMd7C4KE&callback=setCurrentTab";
    document.body.appendChild(script);


});

function imageExists(image_url){

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}


function initialize(mapCanvasId, locations, infos, category) {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        styles: [{
            featureType: "all",
            stylers: [{
                saturation: -100
            }]
        }]
    };

    // Display a map on the page
    map = new google.maps.Map(document.getElementById(mapCanvasId), mapOptions);
    map.setTilt(45);

    // Multiple Markers
    var markers = [];
    jQuery.each(locations, function(index, location) {
        var loc = location.split('|');
        markers.push(new Array(loc[0], loc[1], loc[2]));
    });

    // Info Window Content
    var infoWindowContent = [];

    jQuery.each(infos, function(index, info) {
        infoWindowContent.push(new Array(info));
    });


    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(),
        marker, i;

    if (category) {
        var iconMarker = template_dir.stylesheet_uri + '/assets/build/images/map-marker-' + category + '.png';

        if(!imageExists(iconMarker)){
            iconMarker = template_dir.stylesheet_uri + '/assets/build/images/map-marker-yellow.png';
        }
    } else {
        var iconMarker = template_dir.stylesheet_uri + '/assets/build/images/map-marker-yellow.png';
    }

    // Loop through our array of markers & place each one on the map  
    for (i = 0; i < markers.length; i++) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            icon: iconMarker,
            // animation: google.maps.Animation.BOUNCE
        });

        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                marker.setAnimation(null);
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
    }

    if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
       var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.1, bounds.getNorthEast().lng() + 0.1);
       var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.1, bounds.getNorthEast().lng() - 0.1);
       bounds.extend(extendPoint1);
       bounds.extend(extendPoint2);
    }

    map.fitBounds(bounds);


    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    // var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    //     this.setZoom(3);
    //     google.maps.event.trigger(map, 'resize');
    //     google.maps.event.removeListener(boundsListener);
    // });

}

function setCurrentTab() {
    jQuery('ul.tabs li:first-of-type').trigger('click');
}


jQuery('ul.tabs li').on('click', function() {
    jQuery('ul li.current').removeClass('current');
    jQuery(this).closest('li').addClass('current');

    var tabId = jQuery('li.current').data('tab');
    var catId = jQuery('li.current').data('id');
    var category = jQuery('li.current').data('category');
    if (category) {
        var clickCategory = category;
    }

    var mapCanvasId = 'map_canvas_' + catId;
    var locations = [];
    var infos = [];

    jQuery('.tab-content').removeClass('current fade-in');
    jQuery('#' + tabId).addClass('current fade-in');

    jQuery('.tab-content.current .locations').each(function() {
        var address = jQuery(this).data('address');
        var latLong = jQuery(this).data('latlng').split(',');
        var info = jQuery(this).html();
        var location = address + '|' + latLong[0] + '|' + latLong[1];
        locations.push(location);
        infos.push(info);
    });

    initialize(mapCanvasId, locations, infos, clickCategory);

});