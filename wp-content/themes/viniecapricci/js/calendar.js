jQuery(document).ready(function() {

    var events = [];

    jQuery.ajax({
        url: calendar_ajax_object.ajaxurl,
        type: 'post',
        dataType: 'json',
        data: {
            action: 'ajax_request',
        },
        success: function(response) {

            jQuery.each(response, function(index, value) {
                events.push(value);
            });

            /**
             * Show Calendar
             */
            document.addEventListener('DOMContentLoaded', showcalendar(events));
        }
    });
});

function showcalendar(events) {
    // console.log(events);

    // var date = new Date();
    // var starDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        header: {
            left: 'title',
            right: 'prev,next',
        },
        plugins: ['dayGrid'],
        defaultView: 'dayGridMonth',
        fixedWeekCount: false,
        showNonCurrentDates: false,
        contentHeight: 'auto',
        nextDayThreshold: '09:00:00',
        displayEventTime: false,
        events: events,
    });

    calendar.render();
}