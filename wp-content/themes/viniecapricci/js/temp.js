jQuery(document).ready(function() {

    var viniCookies = Cookies.noConflict();

    var viniHamperBuilder = {
        state_key: 'vini_hamper_builder',
        current_state_id: '',
        default_state: {
            in_hamper_mode: false,
            basket: {},
            products: {},
            message: '',
        },
        state: {},
        errors: {
            no_space_left: 'Sorry can not fit anymore items in hamper. Please Upgrade your hamper.'
        },
        init: function() {

            var state_id = viniCookies.get('vini_state_id');

            if (_.isUndefined(state_id)) {
                this.current_state_id = this.saveInitialStateToDB();
            } else {
                this.current_state_id = state_id;
            }

            this.getDBState();

            var state = viniCookies.getJSON(this.state_key);

            if (!_.isUndefined(state)) {
                this.state = state;
            } else {
                this.state = this.default_state;
            }

            // console.log(this.state);

            if (jQuery('body').hasClass('home')) return;

            // Add to Cart Loop Fix
            if (this.state.in_hamper_mode || jQuery('body').hasClass('in_hamper_mode')) {

                // Show Section Banner
                jQuery('.section-hamper-sequence').addClass('fade-in').show();

                jQuery('.add_to_cart_button').removeClass('add_to_cart_button').addClass('hamper_add_to_cart');

                // make sure required hamper legends is visible
                if (jQuery('body').hasClass('post-type-archive-product') || jQuery('body').hasClass('single-product')) {
                    jQuery('.hamper-step-2').show();
                    jQuery('.hamper-bar-wrapper').show();
                    jQuery('.box-floating').hide();

                    jQuery('.hamper_add_to_cart').text('Select');
                }

                if (jQuery('body').hasClass('tax-product_cat')) {
                    jQuery('.hamper-step-3, .hamper-bar-wrapper').show();
                    jQuery('.box-floating').hide();

                    jQuery('.hamper_add_to_cart').text('Select');
                }

                if (jQuery('body').hasClass('step-4')) {
                    jQuery('.hamper-step-4').show();
                    jQuery('.box-floating').hide();

                    this.populateHamperMessage();
                }

                if (jQuery('body').hasClass('step-5')) {
                    jQuery('.hamper-step-5').show();
                    jQuery('.box-floating').hide();

                    this.renderHamperPreview();

                }

                if (jQuery('body').hasClass('step-1')) {
                    jQuery('.section.section-banner').hide();

                }

            }

            // Set Correct Basket Size
            if (!_.isEmpty(this.state.basket)) {

                var $selectedBasket = jQuery('.hamper_add_to_cart[data-product_id="' + this.state.basket.id + '"]');

                $selectedBasket.attr('data-original-text', $selectedBasket.text()).text('Selected');
                $selectedBasket.addClass('added');

                this.renderEditModeBasket();

            }

            var usage_and_costs = this.calculateUsageAndCosts();

            this.renderUsageAndCosts(usage_and_costs);

            this.renderEditModeItems();

        },
        saveInitialStateToDB: function() {
            var state_id = new Date().getTime() + (+new Date).toString(36).slice(-8);
            viniCookies.set('vini_state_id', state_id);

            jQuery.post(viniAjaxURL, {
                action: 'vini_add_set_hamper_row',
                state_id: state_id,
            }, function(data) {
                // console.log(data);
            });

            return state_id;
        },
        setDBState: function() {

            state_id = this.current_state_id;

            jQuery.post(viniAjaxURL, {
                action: 'vini_add_set_hamper_row',
                state_id: state_id,
                state: this.state,
            }, function(data) {
                // console.log('set state in function', data);
            });
        },
        getDBState: function() {

            var _this = this;

            jQuery.post(viniAjaxURL, {
                action: 'vini_get_hamper_row',
                state_id: this.current_state_id,
            }, function(data) {
                // console.log('data from db', data);
                _this.setLocalState(data);
            }, 'json');

        },
        setLocalState: function(data) {

            // console.log(data);

            if (data.id !== undefined) {
                this.state = JSON.parse(data.state);
            } else {
                this.state = this.default_state;
            }

            if (jQuery('body').hasClass('home')) return;

            // console.log('state in set local state', this.state.in_hamper_mode);

            // Add to Cart Loop Fix
            if (this.state.in_hamper_mode || jQuery('body').hasClass('in_hamper_mode')) {

                // Show Section Banner
                jQuery('.section-hamper-sequence').addClass('fade-in').show();

                jQuery('.add_to_cart_button').removeClass('add_to_cart_button').addClass('hamper_add_to_cart');

                // make sure required hamper legends is visible
                if (jQuery('body').hasClass('post-type-archive-product') || jQuery('body').hasClass('single-product')) {
                    jQuery('.hamper-step-2, .hamper-bar-wrapper').show();
                    jQuery('.box-floating').hide();

                    jQuery('.hamper_add_to_cart').text('Select');
                }

                if (jQuery('body').hasClass('tax-product_cat')) {
                    jQuery('.hamper-step-3, .hamper-bar-wrapper').show();
                    jQuery('.box-floating').hide();

                    jQuery('.hamper_add_to_cart').text('Select');
                }

                if (jQuery('body').hasClass('step-4')) {
                    jQuery('.hamper-step-4').show();
                    jQuery('.box-floating').hide();

                    this.populateHamperMessage();
                }

                if (jQuery('body').hasClass('step-5')) {
                    jQuery('.hamper-step-5').show();
                    jQuery('.box-floating').hide();

                    this.renderHamperPreview();

                }

                if (jQuery('body').hasClass('step-1')) {
                    jQuery('.section.section-banner').hide();

                }

            }

            // Set Correct Basket Size
            if (!_.isEmpty(this.state.basket)) {

                var $selectedBasket = jQuery('.hamper_add_to_cart[data-product_id="' + this.state.basket.id + '"]');

                $selectedBasket.attr('data-original-text', $selectedBasket.text()).text('Selected');
                $selectedBasket.addClass('added');

                this.renderEditModeBasket();

            }

            var usage_and_costs = this.calculateUsageAndCosts();

            this.renderUsageAndCosts(usage_and_costs);

            this.renderEditModeItems();
        },
        saveState: function() {
            // viniCookies.set(this.state_key, this.state);
            this.setDBState();
            // console.log('set state', this.state_key, this.state);
        },
        getState: function() {
            return this.state;
        },
        destroyState: function() {
            viniCookies.remove('vini_state_id');

            state_id = this.current_state_id;

            jQuery.post(viniAjaxURL, {
                action: 'vini_remove_hamper_row',
                state_id: state_id,
            }, function(data) {
                // console.log('removed');
            });
            this.state = this.default_state;
        },
        setBasket: function(basket) {
            this.state.basket = basket;
            this.state.in_hamper_mode = true;
            this.saveState();
        },
        getBasket: function() {
            return (!_.isEmpty(this.state.basket)) ? this.state.basket : false;
        },
        getProduct: function(product_id) {
            return (!_.isUndefined(this.state.products[product_id])) ? this.state.products[product_id] : false;
        },
        setProductQuantity: function(product_id, quantity) {
            this.state.products[product_id].quantity = quantity;
            this.saveState();
        },
        canAddProductToBasket: function(product, quantity) {

            var basket = this.getBasket();

            if (!basket) return;

            var usage_and_costs = this.calculateUsageAndCosts();

            var new_space_used = usage_and_costs.used_space + (product.item_space * quantity);

            return (new_space_used > basket.basket_space) ? false : true;

        },
        addProductToBasket: function(product, quantity) {

            if (this.canAddProductToBasket(product, quantity)) {

                // console.log('product', product);
                // console.log('products', this.state.products);

                if (_.isUndefined(this.state.products)) {
                    this.state.products = {};
                }

                if (_.isUndefined(this.state.products[product.id])) {
                    product.quantity = quantity;
                    this.state.products[product.id] = product;
                } else {
                    this.state.products[product.id].quantity += quantity;
                }

                this.saveState();

                var usage_and_costs = this.calculateUsageAndCosts();

                this.renderUsageAndCosts(usage_and_costs);

                this.renderEditModeItems();

                this.renderHamperPreview();

            } else {
                alert(this.errors.no_space_left);
            }

        },
        updateProductQuantity: function(product_id, quantity) {

            var product = this.getProduct(product_id);

            if (!product) return;

            var allow_quantity_adjustment = false;

            if (product.quantity > quantity) {

                /* reducing quantity - we will be ok */
                allow_quantity_adjustment = true;

            } else if (product.quantity < quantity) {

                /* adding quantity */
                var quantity_diff = quantity - product.quantity;

                // console.log(quantity_diff);

                if (this.canAddProductToBasket(product, quantity_diff)) {
                    allow_quantity_adjustment = true;
                } else {
                    alert(this.errors.no_space_left);
                }

            } else {
                /* same quantity do nothing */
            }

            if (allow_quantity_adjustment) {

                this.setProductQuantity(product_id, quantity);

                var usage_and_costs = this.calculateUsageAndCosts();

                this.renderUsageAndCosts(usage_and_costs);

                this.renderHamperPreview();

            } else {
                /* we do this as there might be exceeding quantity values */
                this.renderEditModeItems();

                this.renderHamperPreview();
            }

        },
        deleteProductFromBasket: function(product_id) {

            if (!_.isUndefined(this.state.products[product_id])) {

                delete this.state.products[product_id];

                this.saveState();

                var usage_and_costs = this.calculateUsageAndCosts();

                this.renderUsageAndCosts(usage_and_costs);

                this.renderEditModeItems();

                this.renderHamperPreview();

            }

        },
        calculateUsageAndCosts: function() {

            var basket = this.getBasket();

            // console.log('cal basket cost', basket);

            if (!basket) return;

            var used_space = 0;
            var cost = parseFloat(basket.price);

            // console.log('cost', cost);

            _.each(this.state.products, function(product) {
                used_space += (product.item_space * product.quantity);
                cost += parseFloat(product.price) * product.quantity;
            });

            var pct = Math.round((used_space * 100) / basket.basket_space);

            return {
                used_space: used_space,
                pct: pct,
                cost: cost.toFixed(2)
            };

        },
        renderUsageAndCosts: function(usage_and_costs) {

            /* Hamper Bar */
            /*
            jQuery('.hamper-bar .bar').css({
                width: usage_and_costs.pct + '%'
            });
            jQuery('.hamper-bar .total_pct').text(usage_and_costs.pct + '%');
            */

            // console.log('calc price', usage_and_costs);

            if (usage_and_costs == undefined) {
                jQuery('.hamper-bar .total_price span').text('0');

                /* Edit Mode */
                jQuery('.hamper-bar-edit-mode .total .amount').text('0');

            } else {
                jQuery('.hamper-bar .total_price span').text(usage_and_costs.cost);

                /* Edit Mode */
                jQuery('.hamper-bar-edit-mode .total .amount').text(usage_and_costs.cost);
            }

        },
        renderEditModeBasket: function() {

            jQuery('.hamper-bar-edit-mode .basket img').attr('src', 'https://www.viniecapricci.com/wp-content/uploads/' + this.state.basket.thumbnail);
            jQuery('.hamper-bar-edit-mode .basket .title').text(this.state.basket.title);

        },
        renderEditModeItems: function() {

            var template = _.template(jQuery("#template_hamper_product_item").html());

            var $product_list = jQuery('.hamper-bar-edit-mode .hamper-items');

            $product_list.empty();

            _.each(this.state.products, function(product) {

                var html = template(product);

                $product_list.append(html);

            });

        },
        renderHamperPreview: function() {

            if (jQuery('.hamper-preview').length) {

                var $hamper_preview = jQuery('.hamper-preview');

                var template = _.template(jQuery("#template_hamper_preview").html());

                var usage_and_costs = this.calculateUsageAndCosts();

                var html = template({
                    state: this.state,
                    usage_and_costs: usage_and_costs
                });

                $hamper_preview.html(html);


                /* Add Hamper to Cart */
                jQuery('.section-hamper-preview div').on('click', ' #add_hamper_to_cart', function(e) {

                    var $button = jQuery(this);

                    if ($button.hasClass('adding')) return;

                    $button.addClass('adding');

                    var checkoutURL = $button.data('checkoutUrl');

                    var state = viniHamperBuilder.getState();

                    jQuery.post(viniAjaxURL, {
                        action: 'vini_add_hamper_to_cart',
                        state: state
                    }, function(data) {

                        if (data.success) {

                            viniHamperBuilder.destroyState();

                            jQuery('.hamper-preview').hide();
                            jQuery('.hamper-success-message').show();

                            setTimeout(function() {
                                window.location.href = checkoutURL;
                            }, 3000);

                        } else {
                            alert('There was an error adding the hamper to the cart. Please try again or contact an administrator.');
                        }

                    }, "json");
                });

            }

        },
        doesHamperHaveGifts: function() {

            var matchedProducts = _.size(_.where(this.state.products, {
                is_gift: true
            }));

            // console.log(matchedProducts);

            return matchedProducts;

        },
        setHamperMessage: function(message) {

            this.state.message = message;

            this.saveState();

        },
        populateHamperMessage: function() {
            jQuery('#hamper_message').val(this.state.message);
        }
    };

    var hamperModal = jQuery('[data-remodal-id=hamper_modal]').remodal();

    /* Initialize */

    viniHamperBuilder.init();

    /* Update Add to Cart Quantity */
    jQuery('body').on('click', '.btn-plus-minus.decrease', function(e) {

        var $el = jQuery('.summary.entry-summary').find('.hamper_add_to_cart');

        var qty = parseInt($el.data('quantity'));

        if (qty === 1) return;

        var newQTY = qty - 1;

        $el.data('quantity', newQTY);

    });

    jQuery('body').on('click', '.btn-plus-minus.increase', function(e) {

        var $el = jQuery('.summary.entry-summary').find('.hamper_add_to_cart');

        var qty = parseInt($el.data('quantity'));

        var newQTY = qty + 1;

        $el.data('quantity', newQTY);

    });

    /* This action is only triggered on loop items - not single pages */
    jQuery('body').on('click', '.hamper_add_to_cart', function(e) {

        e.preventDefault();

        /* Determine Product ID */
        var productID = jQuery(this).data('product_id');
        var quantity = parseInt(jQuery(this).data('quantity'));

        var itemData = window['productHamperData_' + productID];

        // console.log(itemData);

        if (!_.isUndefined(itemData)) {

            if (itemData.is_basket) {

                jQuery('.hamper_add_to_cart[data-original-text]').each(function() {
                    var originalText = jQuery(this).data('originalText');
                    // console.log('originalText', originalText);
                    jQuery(this).text(originalText);

                    jQuery(this).removeClass('added');
                })

                var $selectedBasket = jQuery('.hamper_add_to_cart[data-product_id="' + itemData.id + '"]');

                $selectedBasket.attr('data-original-text', $selectedBasket.text()).text('Selected');
                $selectedBasket.addClass('added');

                viniHamperBuilder.setBasket(itemData);

            } else {

                viniHamperBuilder.addProductToBasket(itemData, quantity);

                jQuery(this).attr('data-original-text', 'Selected');
                jQuery(this).addClass('added');

            }

        }

    });

    /* Hamper Manager */

    jQuery('.hamper-bar .checkout, .hamper-bar-edit-mode .checkout').click(function(e) {

        e.preventDefault();

        if (!viniHamperBuilder.doesHamperHaveGifts()) {
            hamperModal.open();
        } else {
            window.location.href = jQuery(this).data('checkoutUrl');
        }

    });

    jQuery('.hamper-bar .edit').click(function(e) {

        e.preventDefault();

        if (jQuery(this).hasClass('in_edit_mode')) {
            jQuery('.hamper-bar-edit-mode').hide();
            jQuery(this).text('Edit');
        } else {
            jQuery('.hamper-bar-edit-mode').show();
            jQuery(this).text('Close');
        }

        jQuery(this).toggleClass('in_edit_mode');

    });

    /* Step 4 */
    jQuery('#hamper_go_to_step_5').click(function(e) {

        e.preventDefault();

        var message = jQuery('#hamper_message').val();

        viniHamperBuilder.setHamperMessage(message);

        window.location.href = jQuery(this).data('checkoutUrl');

    });

    /* Bin a product from a hamper */
    jQuery('.hamper-items').on('click', '.wrap .bin', function(e) {

        e.preventDefault();

        var product_id = jQuery(this).parents('.product_in_hamper').attr('id').split('_').pop();

        viniHamperBuilder.deleteProductFromBasket(product_id);

    });

    //Update Quantity
    jQuery('.hamper-items').on('change', '.wrap input', function(e) {

        var new_quantity = parseInt(jQuery(this).val());

        var productID = jQuery(this).parents('.product_in_hamper').attr('id').split('_').pop();

        if (new_quantity) {
            viniHamperBuilder.updateProductQuantity(productID, new_quantity);
        } else {
            alert('Please type in a number greater than 0.');
        }

    });

    jQuery('.hamper-items').on('click', '.wrap .plus', function() {

        var qty = parseInt(jQuery(this).prev().val());

        jQuery(this).prev().val(qty + 1).trigger('change');

    });

    jQuery('.hamper-items').on('click', '.wrap .minus', function() {

        var qty = parseInt(jQuery(this).next().val());

        if (qty === 1) return;

        jQuery(this).next().val(qty - 1).trigger('change');

    });

    /* Validate if we have correctly completed step 2 */
    jQuery('#step_1_continue').click(function(e) {

        e.preventDefault();

        // var basket = viniHamperBuilder.getBasket();
        var basket = true;

        console.log(basket + "...........");

        if (basket === false) {
            alert('Please select a gift basket first before continuing.');
        } else {
            window.location.href = jQuery(this).attr('href');
        }

    });



    jQuery(window).on('load', function() {
        jQuery('.site-nav').find('ul#primary-menu li.cart a').remove();
        var cartCount = jQuery('.site-nav').find(' > div.cart').html()
        jQuery('.site-nav').find('ul#primary-menu li.cart').append(cartCount);
    });


    // 20-02-2017 - TICKETS EVENTS ON VINI CAPPRICI //  



    jQuery('.wrapper-button.subtract').click(function() {
        var tickets = jQuery('#quantityTickets').val();
        var max = jQuery('.buttonAddTickets').attr('data-maximum_tickets');
        max = parseInt(max);
        tickets = parseInt(tickets);
        if ((tickets <= max) && (tickets >= 2)) {
            tickets = tickets - 1;
            jQuery('#quantityTickets').val('');
            jQuery('#quantityTickets').val(tickets);
            jQuery('#quantityTickets').trigger("change");
        }
    });

    jQuery('.wrapper-button.add').click(function() {
        var tickets = jQuery('#quantityTickets').val();
        // var type = jQuery('.buttonAddTickets').data('product-type');
        // alert(type);
        var max = jQuery('.buttonAddTickets').attr('data-maximum_tickets');
        tickets = parseInt(tickets);
        if (tickets < max) {
            tickets = tickets + 1;
            jQuery('#quantityTickets').val();
            jQuery('#quantityTickets').val(tickets);
            jQuery('#quantityTickets').trigger("change");

        }
    });

    jQuery('#quantityTickets').change(function() {
        var value = jQuery('#quantityTickets').val();
        var max = jQuery('.buttonAddTickets').attr('data-maximum_tickets');
        value = parseInt(value);
        if (value > max) {
            jQuery('#quantityTickets').val(max);
            value = max
        }
        if (value < 1) {
            jQuery('#quantityTickets').val('0');
            value = 0;
        }
        jQuery('.buttonAddTickets').attr('data-quantity', value);

    });

    jQuery('.purchase-ticket .buttonAddTickets').click(function(e) {

        e.preventDefault();

        var quantity = jQuery('.buttonAddTickets').attr('data-quantity');
        var value = jQuery('.cart-contents-count').html();
        var product = jQuery('.buttonAddTickets').attr('data-product_id');

        var type = jQuery('.buttonAddTickets').data('product-type');


        quantity = parseInt(quantity);


        if (type == 'variable') {
            var url = jQuery(this).attr('href');
            window.location = url + '&quantity=' + quantity;
            jQuery('#alertQuanitityAdded').html('You have added ' + quantity + ' ticket/s to the cart');
            jQuery('#eventAlert').fadeIn();
            setTimeout(function() {
                jQuery('#eventAlert').fadeOut();
            }, 15000);

        } else {
            if (value == undefined) {
                value = 0;

            }

            value = parseInt(value);
            value = parseInt(value + quantity);

            jQuery.ajax({
                type: "post",
                dataType: "json",
                url: viniAjaxURL,
                data: {
                    action: "vini_add_event_to_cart",
                    quantity: quantity,
                    product_id: product
                },
                success: function(response) {
                    if (response == true) {
                        jQuery('#alertQuanitityAdded').html('You have added ' + quantity + ' ticket/s to the cart');
                        jQuery('#eventAlert').fadeIn();
                        setTimeout(function() {
                            jQuery('#eventAlert').fadeOut();
                        }, 5000);
                        value = value.toString();
                        if (jQuery('span').hasClass(".cart-contents-count")) {
                            jQuery('.cart-contents-count').remove();
                            jQuery('.cart-contents-count').html(value);
                        } else {
                            jQuery('.cart-contents-count').remove();
                            jQuery('.cart-contents').append('<span class="cart-contents-count">' + value + '</span>');
                        }
                    } else {
                        jQuery('.single-two-cols #alertStock').fadeIn();
                        jQuery('.single-two-cols ul#alertStock span').html('You can not add ' + quantity + ' tickets to the cart because we don`t have enough tickets in stock.');
                        setTimeout(function() {
                            jQuery('.single-two-cols #alertStock').fadeOut();
                        }, 5000);
                        return false;
                    }
                }
            });
        }

        return false;

    });

    jQuery('#eventAlert #closeAlert').click(function() {
        jQuery('#eventAlert').fadeOut();
    });


    //Show events list when year is clicked
    jQuery('.box-year').on('click', 'h2.year', function() {
        jQuery(this).siblings('.box-months').fadeIn(500);
    });


    jQuery('#partners #ls-partners').slick({
        arrows: true,
        dots: false,
        centerMode: false,
        infinite: true,
        centerPadding: '40px',
        slidesToShow: 5,
        slidesToScroll: 1,
        speed: 300,
        variableWidth: false,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            }, {
                breakpoint: 620,
                settings: {
                    slidesToShow: 1,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
    });

    jQuery('#partners #ls-partners').addClass('fade-in').css({
        'visibility': 'visible'
    });

    jQuery('#partners #ls-partners').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        console.log('beforeChange', currentSlide, nextSlide);
    });
    jQuery('#partners #ls-partners').on('afterChange', function(event, slick, currentSlide) {
        console.log('afterChange', currentSlide);
    });

});