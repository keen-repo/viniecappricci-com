<?php

if (!isset($_SESSION['hamper_mode'])) {
	
	$_SESSION['hamper_mode'] = true;
	
}

if (!isset($_SESSION['hampers']))
	$_SESSION['hampers'] = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	// generate new hamper key
	
	
	if (!empty($_POST['hamper_key'])) {
		
		$key = $_POST['hamper_key'];
		
		$used_space = 0;
		
		foreach ($_SESSION['hampers'][$key]['items'] as $item) {
			
			$used_space += $item['hamper_space'] * $item['quantity'];
			
		}
		
		$box = $_POST['box_size'];
		
		$total_possible_items = get_post_meta($box, 'total_possible_items', true);
		
		$used_space_pct = round(($used_space * 100) / $total_possible_items);
		
		$_SESSION['hampers'][$key]['box']		= $box;
		$_SESSION['hampers'][$key]['percent']	= $used_space_pct;
		
	} else {
		
		$key = mt_rand();
	
		$_SESSION['hampers'][$key] = array(
			'box' => $_POST['box_size'],
			'percent' => 0,
			'items' => array()
		);
		
	}
	
	// Set active hamper
	$_SESSION['active_hamper'] = $key;
	
	wp_redirect(site_url('/shop/'));
	
	exit();
	
}


if (isset($_GET['destroy'])) {
	
	$_SESSION = array();
	session_destroy();
	
	
}

$the_hamper = '';
$selected_box = '';

if (isset($_SESSION['active_hamper']) && !isset($_GET['create'])) {
	
	// this means that we should load the hamper we need to edit.
	
	$the_hamper = $_SESSION['active_hamper'];
	
	$selected_box = $_SESSION['hampers'][$the_hamper]['box'];
	
} else {
	
	// we create a new one
	
}

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="wrapper">
			<main id="main" class="site-main" role="main">
				
				<div class="hamper_steps">
					<h3>Build your own <strong>Hamper</strong></h3>
					<div class="steps">
						<div class="large_step">
							<div class="step_text">Step</div>
							<div class="no">1</div>
							<div class="desc">Select Your Box Size</div>
							<div class="clear"></div>
						</div>
						<div class="small_step"><strong>Step 2.</strong> Add Items</div>
						<div class="small_step"><strong>Step 3.</strong> Select Gift</div>
						<div class="small_step last"><strong>Step 4.</strong> Add a message</div>
					</div>
					<img src="<?php echo get_template_directory_uri(); ?>/images/build_hamper_step_1.png" alt="" class="bottom_right" />
				</div>

				<form method="post" id="step_1_hamper_form" action="<?php the_permalink(); ?>">
					<input type="hidden" name="box_size" id="box_size" value="<?php echo $selected_box; ?>" />
					
					<input type="hidden" name="hamper_key" id="hamper_key" value="<?php echo $the_hamper; ?>" />

					<?php while ( have_posts() ) : the_post(); ?>

					<div class="three_column">
						<?php echo do_shortcode('[product_category category="hamper-gift-boxes"]'); ?>
					</div>

					<?php endwhile; // end of the loop. ?>
					
					<input type="submit" name="next_step" id="next_step" value="Next Step &raquo;" />
				</form>
			</main><!-- #main -->
		</div>
	</div><!-- #primary -->
	<script type="text/javascript">
		jQuery(document).ready(function() {
			
			jQuery('#step_1_hamper_form').submit(function(e) {
				
				var box_size = jQuery('#box_size').val();
				
				if (box_size === '') {
					
					e.preventDefault();
					
					alert('Please select a hamper box before continuing');
					
				}
				
			});
			
			var box_size = jQuery('#box_size').val();
			
			if (box_size !== '') {
				
				jQuery('ul.products li.post-' + box_size + ' a.add_to_cart_button').addClass('added');
				
			}
			
		});
	</script>
<?php get_footer(); ?>
