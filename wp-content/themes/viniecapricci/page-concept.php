<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main new-concept" role="main">

		

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

			<?php
				global $post;
					/**
					 * Get subpages and create a navigation
					 * 
					 */
					$args = array(
					    'post_type'      => 'page',
					    'posts_per_page' => -1,
					    'post_parent'    => $post->ID,
					    'order'          => 'ASC',
					    'orderby'        => 'ID'
					 );

					$parent = new WP_Query( $args );

					if ( $parent->have_posts() ) : ?>
						<section class="subpage-navigation">
							<div class="container wrapper-1400">
								<div class="inner">
									 <ul id="parent-<?php the_ID(); ?>" class="parent-page list-unstyled">
									<?php while ( $parent->have_posts() ) : $parent->the_post(); 

						    	/**
						    	 * Get the sub pages slug and used it as the anchor ID
						    	 * @var [type]
						    	 */
						    	
						    		$post = get_post( get_the_ID() );
						    		$slug = $post->post_name;
						    	?>
						        
				            <li class="<?php echo $slug;  ?>" style="text-align: center;"><a href="#<?php echo $slug;  ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				            </li>

						    <?php endwhile; ?>
						        </ul>

								</div>
							</div>

						</section>
					<?php endif; 
					
					wp_reset_postdata(); ?>

			<!-- Get Subpages content -->

			<?php
					/**
					 * Show Sub-pages content
					 * 
					 */
					$args = array(
					    'post_type'      => 'page',
					    'posts_per_page' => -1,
					    'post_parent'    => $post->ID,
					    'order'          => 'ASC',
					    'orderby'        => 'ID'
					 );

					$parent = new WP_Query( $args );

					if ( $parent->have_posts() ) : ?>
						
									<?php while ( $parent->have_posts() ) : $parent->the_post(); 

						    	/**
						    	 * Get the sub pages slug and used it as the anchor ID
						    	 * @var [type]
						    	 */
						    	
						    		$post = get_post( get_the_ID() );
						    		$slug = $post->post_name;
						    	?>
						        
				          <section id="<?php echo $slug; ?>" class="clear bg-black-2 text-white">
				          	<header class="section-header">
				          		<div class="wrapper-1400">
				          			<h2 class="text-white"><?php echo get_the_title(); ?></h2>
				          		</div>
				          	</header>
				          	<div class="subpage-content">
				          		<div class="wrapper-1400">
				          			<div class="inner">
				          				<?php echo apply_filters( 'the_content' , get_the_content() ); ?>
				          			</div>
				          		</div>

				          		<!-- Show Gallery Images if not empty -->
				          		<?php 
				          			$images = rwmb_meta( 'concept_gallery', array( 'size' => 'full' ) );
												
				          		?>

				          		<?php if (!empty( $images)): ?>
				          			<div id="gallery-<?php echo $slug; ?>" class="slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 1}'>

				          				<?php 
				          				foreach ( $images as $image ) :  ?>
				          					<div class="item">
					          					<figure style="margin: 5px;">
					          						<img src="<?php echo $image['url'] ?>">
					          					</figure>
					          				</div>
												 <?php endforeach; ?>

				          			</div>
				          		<?php endif; ?>

				          	</div>
				          </section>

						    <?php endwhile; ?>

					<?php endif; 
					
					wp_reset_postdata(); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	
<?php get_footer(); ?>

<script>
		jQuery(document).ready(function($){

			// function doSlick(elem) {
				jQuery('.slider').slick({
					responsive: [{
					breakpoint: 1024,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},{
					breakpoint: 600,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
							centerMode: true
						}
					}, ]
				});
			// }

		});

	</script>

