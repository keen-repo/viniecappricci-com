<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>
<?php 
	$membership = rwmb_meta( '_vini_fm_membership', 'type=textarea', 10439);
	$delivery = rwmb_meta( '_vini_fm_delivery', 'type=textarea', 10439);
	$call = rwmb_meta( '_vini_fm_call', 'type=textarea', 10439);
	$insta = rwmb_meta( '_vini_fm_insta','type=url', 10439);
	$fb = rwmb_meta( '_vini_fm_facebook','type=url', 10439);

?>

<script>
	jQuery( document ).ready(function() {
		jQuery('.banner .image').css("height","672px");
		jQuery('.banner .image .content .text').css("top","20%");
		jQuery('.banner .image .content .text h1').css("text-align","center");
		jQuery('.banner .image .content .text h1').append("<br><p class='py-5'>Choose card you wish to subscribe for.</p>");
	
		var url = new URL(location.href);
		var c = url.searchParams.get("card");
		if(c=="loyaltyrewards"){
			card=document.getElementsByClassName('card1')[0];
			cardform=document.getElementsByClassName('card1-form')[0];
			card.classList.add("application-tab-active");
			cardform.classList.add("cardform-active");
		}else{
			card=document.getElementsByClassName('card2')[0];
			cardform=document.getElementsByClassName('card2-form')[0];
			card.classList.add("application-tab-active");
			cardform.classList.add("cardform-active");
		}

		var element = document.getElementsByClassName("wapf-checkbox-label")[0];
		element.classList.add("frm_switch_block");
		element.innerHTML='<label class="frm_switch_block wapf-checkbox-label" for="64405"><span class="frm_off_label frm_switch_opt">Pick up from Vini e Capricci</span><input id="64405" type="checkbox" name="wapf[field_62fb8be3fd1d4]" value="Send via post" data-off="Pick up from Vini e Capricci" class="wapf-input" data-is-required="" data-field-id="62fb8be3fd1d4"><span class="frm_switch" tabindex="0" role="switch" aria-labelledby="field_cwgtt_label" aria-checked="false"><span class="frm_slider"></span></span><span class="frm_on_label frm_switch_opt">Send via post</span></label>';

		document.getElementsByName('wapf[field_62fb8b761bc48]')[0].options[0].innerHTML = "Gender";

		document.getElementsByName('wapf[field_62fb8b4f65704]')[0].onfocusin= function() { this.type='date' };
		document.getElementsByName('wapf[field_62fb8b4f65704]')[0].onfocusout= function() { this.type='text' };
		
		document.getElementsByClassName('tcform')[0].innerHTML = '<div class="wapf-field-container wapf-field-true-false tcform" style="width:100%;" for="62fb8c722adaf"><div class="wapf-field-label wapf--above"><label><span>Terms &amp; Conditions</span> <abbr class="required" title="required">*</abbr></label></div><div class="wapf-field-input"><div class="wapf-checkable"><input type="hidden" value="0" name="wapf[field_62fb8c722adaf]"><label class="wapf-checkbox-label" for="23004"><input id="23004" type="checkbox" value="1" name="wapf[field_62fb8c722adaf]" class="wapf-input" data-is-required="1" data-field-id="62fb8c722adaf" required=""><span class="wapf-label-text">I agree to the <a href=/terms-conditions>Terms & Conditions</a> provided.</span></label></div></div></div>';
	});

	function togglecard(element,card,cardform,cardformoff){
		card=document.getElementsByClassName(card)[0];
		cardform=document.getElementsByClassName(cardform)[0];
		cardformoff=document.getElementsByClassName(cardformoff)[0];
		element.classList.add("application-tab-active");
		cardform.classList.add("cardform-active");
		cardformoff.classList.remove("cardform-active");
		card.classList.remove("application-tab-active");
	}
</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main loyalty-schemes" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

			<!-- <?php echo FrmFormsController::get_form_shortcode( array( 'id' =>	12, 'title' => true, 'description' => true ) ); ?> -->

			<section class="loyalty-schemes-application py-5" >
				<div class="container pb-5" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/build/images/vini-grey-pattern-background2.png');background-repeat: repeat;background-size: contain;">
                    <div class="row">
                        <div class="col-6 text-end">
							<div class="text-center application-tab ms-auto card1" onclick="togglecard(this,'card2','card1-form','card2-form')">
								<svg xmlns="http://www.w3.org/2000/svg" width="50.554" height="62.576" viewBox="0 0 50.554 62.576">
								<g id="Group_663" data-name="Group 663" transform="translate(-1184.86 8.407)">
									<g id="Group_658" data-name="Group 658" transform="translate(1213.441 28.726)" opacity="0.5">
									<path id="Path_407" data-name="Path 407" d="M1203.8,16.607l2.46-.407a13.4,13.4,0,0,1-2.173.815Z" transform="translate(-1203.8 -16.2)" fill="#e9dde2"/>
									<path id="Path_408" data-name="Path 408" d="M1218.575,31.174l-10.216,7.183-1.539-3.3.936-.664,4.3-3a1.636,1.636,0,0,0,.407-2.248l-3.35-4.829-1.071-1.539-1.056-1.524,2.807.905a.828.828,0,0,0,.679-.075.75.75,0,0,0,.257-.226.878.878,0,0,0,.151-.317l.241-1.1L1212,21.7l.785,1.132Z" transform="translate(-1202.263 -14.047)" fill="#e9dde2"/>
									</g>
									<g id="Group_659" data-name="Group 659" transform="translate(1190.67 28.681)" opacity="0.5">
									<path id="Path_409" data-name="Path 409" d="M1199.53,34.338l.936.664-1.539,3.3-10.216-7.183,5.719-8.239,1.645-2.354.211.951a.736.736,0,0,0,.226.407.471.471,0,0,0,.166.136.828.828,0,0,0,.679.075l3-.966-2.128,3.063-3.4,4.889a1.619,1.619,0,0,0,.392,2.248Z" transform="translate(-1188.71 -13.951)" fill="#e9dde2"/>
									<path id="Path_410" data-name="Path 410" d="M1200.441,16.623l-.3.438a12.044,12.044,0,0,1-2.4-.89Z" transform="translate(-1184.113 -16.17)" fill="#e9dde2"/>
									</g>
									<g id="Group_660" data-name="Group 660" transform="translate(1206.092 12.242)" opacity="0.5">
									<path id="Path_411" data-name="Path 411" d="M1223.407,13.757a1.989,1.989,0,0,0-.558,2.054l1.3,4.088a1.971,1.971,0,0,1-1.449,2.535l-4.2.907a1.939,1.939,0,0,0-1.494,1.509l-.573,2.609-.332,1.509-.015.075a2.01,2.01,0,0,1-2.52,1.45l-1.419-.453-2.671-.845a1.961,1.961,0,0,0-2.052.543l-.724.8-1.66,1.824-.513.558a1.966,1.966,0,0,1-2.912,0l-.422-.451-1.645-1.827-.619-.678a1.936,1.936,0,0,0,.875.119,1.739,1.739,0,0,0,.619-.149,1.786,1.786,0,0,0,.649-.468l1.735-1.9,1.162-1.268a.527.527,0,0,1,.166-.152,1.123,1.123,0,0,1,.392-.27,1.975,1.975,0,0,1,1.494-.137l4.029,1.3.06.015a1.976,1.976,0,0,0,2.445-1.224,1.121,1.121,0,0,0,.076-.24l.271-1.222.332-1.526.317-1.447a1.966,1.966,0,0,1,1.509-1.5l4.18-.921a1.974,1.974,0,0,0,1.464-2.519l-1.3-4.091a1.933,1.933,0,0,1,.543-2.051l3.169-2.9a1.786,1.786,0,0,0,.468-.649,1.906,1.906,0,0,0,.03-1.5l2.958,2.688a1.966,1.966,0,0,1,0,2.912Z" transform="translate(-1198.93 -5.276)" fill="#e9dde2"/>
									</g>
									<g id="Group_661" data-name="Group 661" transform="translate(1198.244 8.227)" opacity="0.5">
									<path id="Path_412" data-name="Path 412" d="M1214.08,2.616a12.023,12.023,0,0,1,.172,1.708A12.073,12.073,0,0,1,1202.18,16.4a12.023,12.023,0,0,1-8.451-3.465A12.047,12.047,0,1,0,1214.08,2.616Z" transform="translate(-1193.729 -2.616)" fill="#e9dde2"/>
									</g>
									<g id="Group_662" data-name="Group 662" transform="translate(1184.86 -8.408)">
									<path id="Path_413" data-name="Path 413" d="M1234.4,19.114a3.031,3.031,0,0,0,1.011-2.294,3.109,3.109,0,0,0-1.011-2.294l-3.169-2.882a.835.835,0,0,1-.226-.875l1.3-4.09a3.1,3.1,0,0,0-2.294-3.969l-4.2-.921a.827.827,0,0,1-.634-.634l-.921-4.2a3.036,3.036,0,0,0-1.479-2.022,3.065,3.065,0,0,0-2.49-.272l-4.089,1.3a.848.848,0,0,1-.875-.226l-2.9-3.184a3.2,3.2,0,0,0-4.572,0l-2.9,3.184a.889.889,0,0,1-.875.226L1200-5.333a3.066,3.066,0,0,0-2.49.272,3.071,3.071,0,0,0-1.479,2.022l-.92,4.18a.824.824,0,0,1-.634.649l-4.2.921a3.1,3.1,0,0,0-2.294,3.969l1.3,4.09a.834.834,0,0,1-.226.875l-3.169,2.882a3.156,3.156,0,0,0-1.026,2.294,3.1,3.1,0,0,0,1.026,2.294l3.169,2.9a.819.819,0,0,1,.226.875l-1.3,4.09a3.1,3.1,0,0,0,2.294,3.969l4.2.921a.806.806,0,0,1,.634.634l.83,3.8-6.187,8.918a1.121,1.121,0,0,0,.272,1.569l10.216,7.183a1.13,1.13,0,0,0,.649.2,2.047,2.047,0,0,0,.3-.03,1.2,1.2,0,0,0,.74-.619l5.825-12.54.09.106a3.11,3.11,0,0,0,2.294,1.011,3.061,3.061,0,0,0,2.294-1.011l.2-.211,5.885,12.646a1.166,1.166,0,0,0,.739.619,1.85,1.85,0,0,0,.287.03,1.066,1.066,0,0,0,.649-.2l10.231-7.183a1.134,1.134,0,0,0,.272-1.569l-6.308-9.084.8-3.637a.806.806,0,0,1,.634-.634l4.2-.921a3.1,3.1,0,0,0,2.294-3.969l-1.3-4.09a.82.82,0,0,1,.226-.875ZM1201.49,49.068l-1.056,2.264-8.194-5.749,4.935-7.093a1.611,1.611,0,0,0,.332.226,3.126,3.126,0,0,0,2.49.272l.2-.06,3.893-1.253a.863.863,0,0,1,.875.241l1.132,1.253Zm18.8-10.08a3.126,3.126,0,0,0,2.49-.272,2.7,2.7,0,0,0,.423-.3l4.98,7.168-8.194,5.749-1.057-2.264-4.648-10,1.041-1.147a.863.863,0,0,1,.875-.241Zm8.556-15.422,1.3,4.09a.828.828,0,0,1-.076.679.8.8,0,0,1-.543.392l-4.2.921a3.119,3.119,0,0,0-2.369,2.369l-.347,1.569-.317,1.524-.241,1.1a.88.88,0,0,1-.151.317.751.751,0,0,1-.257.226.828.828,0,0,1-.679.075l-2.807-.905-1.283-.407a3.123,3.123,0,0,0-3.229.875l-.407.438-1.645,1.826-.83.905a.858.858,0,0,1-1.253,0l-.724-.8-1.3-1.419-.362-.407-.5-.543a3.135,3.135,0,0,0-2.294-1.011,3.264,3.264,0,0,0-.936.136l-1.086.347-3,.966a.828.828,0,0,1-.679-.075.471.471,0,0,1-.166-.136.736.736,0,0,1-.226-.407l-.211-.951-.332-1.509-.377-1.735a3.119,3.119,0,0,0-2.369-2.369l-4.2-.921a.8.8,0,0,1-.543-.392.828.828,0,0,1-.075-.679l1.3-4.09a3.087,3.087,0,0,0-.86-3.229l-3.169-2.9a.8.8,0,0,1-.287-.619.828.828,0,0,1,.287-.619l3.169-2.882a3.1,3.1,0,0,0,.86-3.244l-1.3-4.09a.794.794,0,0,1,.075-.664.869.869,0,0,1,.543-.407l4.2-.921a3.1,3.1,0,0,0,2.369-2.354l.92-4.2a.739.739,0,0,1,.392-.543.822.822,0,0,1,.679-.075l4.074,1.3a3.133,3.133,0,0,0,3.245-.86l2.882-3.184a.884.884,0,0,1,1.253,0l2.882,3.184a3.116,3.116,0,0,0,3.229.86l4.09-1.3a.828.828,0,0,1,.679.075.739.739,0,0,1,.392.543l.921,4.2a3.1,3.1,0,0,0,2.369,2.354l4.2.921a.869.869,0,0,1,.543.407.793.793,0,0,1,.076.664l-1.3,4.09a3.1,3.1,0,0,0,.86,3.244l.951.86,2.219,2.022a.813.813,0,0,1,.271.619.791.791,0,0,1-.271.619l-3.169,2.9A3.087,3.087,0,0,0,1228.849,23.565Z" transform="translate(-1184.86 8.407)" fill="#fff"/>
									<path id="Path_414" data-name="Path 414" d="M1206.064-.43a13.2,13.2,0,0,0-7.877,23.8,12.723,12.723,0,0,0,2.037,1.253,12.044,12.044,0,0,0,2.4.89,6.914,6.914,0,0,0,.966.226,12.184,12.184,0,0,0,2.475.241,12.5,12.5,0,0,0,2.626-.272c.332-.06.649-.136.966-.226a13.407,13.407,0,0,0,2.173-.815A14.512,14.512,0,0,0,1214,23.322,13.2,13.2,0,0,0,1206.064-.43Zm0,24.145a10.933,10.933,0,0,1-10.548-8.1,10.385,10.385,0,0,1-.392-2.837,10.941,10.941,0,1,1,10.941,10.941Z" transform="translate(-1180.788 12.468)" fill="#fff"/>
									</g>
								</g>
								</svg>
								<h4 class="text-center">Loyalty<br>Rewards Card</h4>
							</div>
						</div>
						<div class="col-6">
							<div class="text-center application-tab me-auto card2" onclick="togglecard(this,'card1','card2-form','card1-form')">
							<svg xmlns="http://www.w3.org/2000/svg" width="60.116" height="57.296" viewBox="0 0 60.116 57.296">
								<g id="Group_656" data-name="Group 656" transform="translate(-869.131 -246.589)">
									<g id="Group_656-2" data-name="Group 656" transform="translate(904.839 267.359)" opacity="0.5">
									<path id="Path_405" data-name="Path 405" d="M888.854,268.4l13.866-13.516-10.987-1.6-9.569,9.33a5.027,5.027,0,0,0-1.452,4.462l2.715,15.833,8.7,4.573Z" transform="translate(-880.639 -253.283)" fill="#e9dde2"/>
									</g>
									<g id="Group_657" data-name="Group 657" transform="translate(869.131 246.589)">
									<path id="Path_406" data-name="Path 406" d="M916.327,303.885a2.347,2.347,0,0,1-1.083-.267l-16.057-8.443-16.057,8.443a2.327,2.327,0,0,1-3.373-2.454l3.066-17.878-12.991-12.662a2.328,2.328,0,0,1,1.291-3.968l17.953-2.609,8.024-16.265a2.426,2.426,0,0,1,4.177,0l8.027,16.265,17.949,2.609a2.326,2.326,0,0,1,1.291,3.968l-12.988,12.662,3.065,17.878a2.333,2.333,0,0,1-2.293,2.721Zm-17.14-13.668a2.33,2.33,0,0,1,1.083.267l12.966,6.817-2.476-14.434a2.328,2.328,0,0,1,.67-2.06l10.488-10.227-14.493-2.107a2.32,2.32,0,0,1-1.756-1.272l-6.482-13.134L892.706,267.2a2.314,2.314,0,0,1-1.753,1.272l-14.5,2.107,10.49,10.227a2.327,2.327,0,0,1,.67,2.06L885.142,297.3l12.966-6.817A2.317,2.317,0,0,1,899.188,290.217Z" transform="translate(-869.131 -246.589)" fill="#fff"/>
									</g>
								</g>
							</svg>
								<h4 class="text-center">Exclusive<br>Premium Card</h4>
							</div>
						</div>
						<h2 class="text-center py-5">Enter all details requested below.</h2>
						<div class="card1-form">
							<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 12, 'title' => true, 'description' => true ) ); ?>
						</div>
						<div class="card2-form mx-auto">
						<?php
							$product = get_product('77831');
							wc_print_notices();
							do_action( 'woocommerce_single_product_summary', 'vini_woocommerce_template_single_add_to_cart', 30 ); ?>
						</div>
					</div>
				</div>
			</section>

			<section class="loyalty-schemes-qs py-5" >
				<div class="container">
					<div class="row">
                    	<div class="col-sm-0 col-md-2">
						</div>
						<div class="col-sm-12 col-md-8 text-center pt-5">
							<h2>Got a question for us?</h2>
							<h5>Take a look at some of our most frequently asked questions <a href="/loyalty-schemes/#faq">here</a>.</h5>
							<p class="pt-3">Loyalty scheme is compliant with the GDPR law. Read regulations <a href="/gdpr-regulations">here</a>.</p>
						</div>
						<div class="col-sm-0 col-md-2">
						</div>
					</div>
				</div>
			</section>
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>

<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'vini' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->

<!-- <div class="footer-meta">
	<div class="wrapper-1400">
		<ul class="flexbox clear">
			<li class="span_1_of_4 flexbox">
				<a href="/loyalty-schemes" style="display: flex;text-decoration: none;">
					<span class="x-icon icon-membership-card-icon"></span>
					<h1>Loyalty Schemes
					<span>Start earning rewards</span></h1>
				</a>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-delivery-truck-icon"></span>
				<?php echo $delivery; ?>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-telephone-icon"></span>
				<?php echo $call; ?>
			</li>
			<li class="span_1_of_4 flexbox social">
				<h1><?php _e('Connect with Us ' , 'vini'); ?></h1>
				<a href="<?php echo $fb; ?>" target="_blank">
					<span class="x-icon icon-facebook-icon"></span>
				</a>
				<a href="<?php echo $insta; ?>" target="_blank">
				<span class="x-icon icon-instagram-icon"></span>
				</a>
			</li>
		</ul>
	</div>
</div> -->

<?php get_footer(); ?>
