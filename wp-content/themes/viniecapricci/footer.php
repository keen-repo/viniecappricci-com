<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package abraham
 */

$membership = rwmb_meta( '_vini_fm_membership', 'type=textarea', 10439);
	$delivery = rwmb_meta( '_vini_fm_delivery', 'type=textarea', 10439);
	$call = rwmb_meta( '_vini_fm_call', 'type=textarea', 10439);
	$insta = rwmb_meta( '_vini_fm_insta','type=url', 10439);
	$fb = rwmb_meta( '_vini_fm_facebook','type=url', 10439);
?>

	</div><!-- #content -->
<div class="footer-meta">
			<div class="wrapper-1400">
				<ul class="flexbox clear">
					<li class="span_1_of_4 flexbox">
						<a href="/loyalty-schemes" style="display: flex;text-decoration: none;">
							<span class="x-icon icon-membership-card-icon"></span>
							<h1>Loyalty Schemes
							<span>Start earning rewards</span></h1>
						</a>
					</li>
					<li class="span_1_of_4 flexbox">
						<span class="x-icon icon-delivery-truck-icon"></span>
						<?php echo $delivery; ?>
					</li>
					<li class="span_1_of_4 flexbox">
						<span class="x-icon icon-telephone-icon"></span>
						<?php echo $call; ?>
					</li>
					<li class="span_1_of_4 flexbox social">
						<h1><?php _e('Connect with Us ' , 'vini'); ?></h1>
						<a href="<?php echo $fb; ?>" target="_blank">
							<span class="x-icon icon-facebook-icon"></span>
						</a>
						<a href="<?php echo $insta; ?>" target="_blank">
						<span class="x-icon icon-instagram-icon"></span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	
	<?php if (is_front_page()): ?>
		<!-- Meta Data Home Page -->
			<script type="application/ld+json">
			 { "@context": "http://schema.org",
			 "@type": "Organization",
			 "name": "Vini e Capricci",			 
			 "url": "https://www.viniecapricci.com/",			 
			 "address": {
				 "@type": "PostalAddress",
				 "streetAddress": "Gozitano Agricultural Village, Mgarr Road",
				 "addressLocality": "Xewkija",
				 "addressRegion": "Gozo",				 
				 "addressCountry": "MALTA"
				}
			 }
			</script>
	<?php	
		endif;
	?>
	<footer id="colophon" class="site-footer" role="contentinfo">
	
		<?php get_template_part( 'static/wrapper', 'widget-footer' ); ?>

		<div class="site-info flexbox section group wrapper-1400">
			<?php if ( is_active_sidebar( 'footer_2_left' ) ) : ?>
				<div id="footer-2-left" class="widget-main-area span_1_of_2" role="complementary">
					<?php dynamic_sidebar( 'footer_2_left' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'footer_2_right' ) ) : ?>
				<div id="footer-2-right" class="widget-main-area span_1_of_2" role="complementary">
					<?php dynamic_sidebar( 'footer_2_right' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<div class="remodal" data-remodal-id="hamper_modal" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
  <h1>
    You have skipped step 3 and have not chosen a gift.
  </h1>
  <br>
  <a href="<?php echo get_term_link(5495, 'product_cat'); ?>" class="cancel btn btn-red-border">Choose Gift</a>
  <a href="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>" class="confirm btn btn-red-border">Proceed to Checkout</a>
</div>

<script type="text/html" id="template_hamper_product_item">
	<div id="hamper_product_<%= id %>" class="product_in_hamper">
		<div class="wrap">
			<a class="bin" href="#">x</a>
			<div class="image" style="background-image: url('https://www.viniecapricci.com/wp-content/uploads/<%= thumbnail %>')">
				
			</div>
			<div class="meta">
				<span class="title"><%= title %></span>
				<div class="plus-minus-qty">
					<span class="minus">-</span>
					<input type="text" maxlength="2" value="<%= quantity %>" />
					<span class="plus">+</span>
				</div>
				<div class="wrap-qty-price">
					<!-- <span class="quantity">&nbsp;</span> -->
					<span class="price">&euro;<%= price %></span>
				</div>
			</div>
		</div>
	</div>
</script>

<script type="text/html" id="template_hamper_preview">
	<img src="https://www.viniecapricci.com/wp-content/uploads/<%= state.basket.thumbnail %>" alt="" />
	<div class="title"><%= state.basket.title %></div>
	<div class="total">
		Total: <span class="currency">&euro;</span>
		<span class="amount"><%= usage_and_costs.cost %></span>
	</div>

	<h2>Your Hamper consists of:</h2>


	<ul class="products">
	<% 

	_.each(state.products, function(product) { 

	%> 
		<li class="product">
			<a href="" class="woocommerce-LoopProduct-link">
				<div class="wrap-product-thumbnail" style="background-image: url('https://www.viniecapricci.com/wp-content/uploads/<%= product.thumbnail %>')">
				</div><h3><%= product.title %></h3>

			<span class="price">
				<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">€</span>
				<%= parseFloat(product.price * product.quantity).toFixed(2) %></span>
			</span>

			<span class="quantity">
				Qty: <%= product.quantity %>
			</span>
		</a>
	</li>
	<% }); %>

	</ul>
	<!-- Your Message -->
	<!-- <textarea id="hamper_message" name="message" readonly><%= state.message %></textarea><br /> -->
</script>

<?php wp_footer(); ?>

</body>
</html>
