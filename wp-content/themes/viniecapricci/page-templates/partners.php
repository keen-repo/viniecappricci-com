<?php
/**
 * Template Name: Partners
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header();

	$categories = get_categories();


?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
			$terms = get_terms( 'ab_partner_cats' , array(
				'hide_empty' => true,
				'orderby' 	 => 'name',
				'order'		 => 'ASC'

			) );
		?>

			<div class="section group partner-list">
				<div class="col partner-label">
					<span>
						<?php _e( 'Select the product range below to discover where our products are from:' , 'vini' ); ?>
					</span>
				</div>

				<div class="action-buttons">
					<a href="" class="previous"><</a>
					<a href="" class="next">></a>
				</div>


				<ul class="tabs col partner-prod-list">
					<?php
						foreach ( $terms as $term ) {
						    printf( '<li class="tab-link %3$s" data-tab="tab-%1$s" data-id="%1$s" data-category="%3$s">%2$s</li>',
						        $term->term_id,
						        esc_html( $term->name ),
						        $term->slug
						    );
						}
					?>
				</ul>
			</div>

			<div id="map"></div>

			<?php

				foreach ($terms as $term) {

					echo '<pre>';
					//var_dump($term);
					echo '</pre>';

					$termID= $term->term_id;

					/*echo '<div id="tab-'.$termID.'" class="tab-content">';
						echo '<div id="map_wrapper" class="'.$term->slug.'">
    							<div id="map_canvas_' . $termID . '" class="mapping"></div>
							  </div>';*/

						$args = array(
						'post_type' => 'partners',
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'tax_query' => array(
						    array(
							    'taxonomy' => 'ab_partner_cats',
							    'field' => 'slug',
							    'terms' => $term->slug,
							    'operator' => 'IN'
						     )
						  )
						);
						$query = new WP_Query( $args );

						$markers = [];
						$index = 0;
						$counter = 1;
						if ( $query->have_posts() ) :
						$LatLngArr = [];
							while ($query->have_posts()) : $query->the_post();

								$address = get_post_meta( get_the_ID(), 'mb_partner_address', true );
								$location = get_post_meta( get_the_ID(), 'mb_partner_map', true );
								if ( empty( $location )) {
									$location = '36.03672, 14.26183';
								}
								$url = get_post_meta( get_the_ID(), 'mb_partner_url', true );
								$product_link = get_post_meta( get_the_ID(), 'mb_partner_product_link', true );

								$markers = explode(',', $location);
								//$LatLngArr[] = [get_the_title(), (float) $markers[0], (float) $markers[1], $markers[2], $address, $product_link, $url];
								$arr = [get_the_title(), (float) $markers[0], (float) $markers[1], $markers[2], $address, $product_link, $url];
								array_push($LatLngArr, $arr);
								$index++;
								echo '<pre>';
								echo 'term id', get_queried_object()->term_id;
								var_dump($LatLngArr);
								echo '</pre>';
				 	//$LatLngArr_to_JS = json_encode($LatLngArr);

								echo '<div class="locations"  data-address="' . $address .'" data-latlng="' . $location . '">
									<div class="info-window"><h2><a href="' . $product_link  . '">' . get_the_title() . '</a></h2>
									<p class="address">'. $address.'</p><p><a href="' . $url . '" target="_blank">'. $url .'</a></p><p><a href="' . $product_link . '" target="_blank">'. __('View products', THEMELANG ) .'</a></p>
									</div>
									</div>';
							endwhile;
						endif;
				 	//echo '</div>';

				 } // foreach ?>
				 <?php
								?>

				<script>
					jQuery(document).ready(function() {
						var planes = <?= $LatLngArr_to_JS; ?>;

						console.log(planes);

						var map = L.map('map').setView([35.9415768, 14.4113351], 11);
						var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';

						L.tileLayer(
							'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
								attribution: '&copy; ' + mapLink + ' Contributors',
								minZoom: 11,
								maxZoom: 11,
							}).addTo(map);

						L.tileLayer.provider('Esri.WorldImagery').addTo(map);

						var agIcon = L.icon({
							iconUrl: '<?= $iconsDir; ?>/images/icon-map-marker.png',
						});


						for (var i = 0; i < planes.length; i++) {
							let image = "<img src='" + planes[i][3] + "'>";
							let title = planes[i][4];
							let desc = planes[i][5];
							let perm = planes[i][6];

							let popup = image
							+ '<div class="info"><h4 class="black nomargin smaller">' + title + '</h4>'
							+ '<p class="grey-blue">' + desc + '<a class="orange" href="' + perm + '">Read More</a></p></div>';

							marker = new L.marker([planes[i][1],planes[i][2]], {icon: agIcon})
							.bindPopup(popup)
							.addTo(map);
						}
					});
				</script>
				<!-- MAP - End -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
