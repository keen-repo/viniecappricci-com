<?php
/*
 * Template Name: Wine Packager
 *
*/

$wine_packages = WC()->session->get('wine_packages', array());

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$task		= $_POST['task'];

	$wineData 	= json_decode(stripslashes($_POST['wineData']));

	foreach ($wine_packages as $k=>$cart_item_key) {
		/* all the items in our cart we clear so we can start and add fresh */
		WC()->cart->set_quantity($cart_item_key, 0);
		unset($wine_packages[$k]);
	}

	foreach ($wineData as $wd) {

		/* uqk is required because sometimes we can create a custom package which features the same wine, think multiple quantities. */

		$cart_item_key = WC()->cart->add_to_cart($wd->id, 1, '', array(
			'uqk' => mt_rand(), 
			'slots' => implode(',', $wd->slots)
		));

		$wine_packages[] = $cart_item_key;
	}

	WC()->session->set('wine_packages', $wine_packages);

	if ($task == 'continue_to_cart') {

		wp_redirect(site_url('/cart/'));

	} else {

		wp_redirect(site_url('product-category/winespirit/'));

	}

	exit();

	
}

/**
 * The template for displaying The Concept.
 *
 * @package Vini e Capricci
 */

get_header(); 



$cart_items = WC()->cart->get_cart();

$allowed_product_categories = array(14); // wine

$wine_list = array();

$preset_boxes = array();

$taken_products = array();

$bi = 1;

foreach($cart_items as $cart_item_key=>$cart_data) {

	if (in_array($cart_item_key, $wine_packages)) {

		$products_in_slots = explode(',', $cart_data['variation']['slots']);
		foreach ($products_in_slots as $pslot) {
			if (!isset($taken_products[$pslot])) $taken_products[$pslot] = 0;
			$taken_products[$pslot]++;
		}


		/* this is a box lets do something */
		$preset_boxes[$cart_item_key] = array(
			'index' => $bi,
			'id' => $cart_data['product_id'],
			'slots' => $products_in_slots
		);

		$bi++;

	}

 	$product_cats = wp_get_post_terms( $cart_data['product_id'], 'product_cat' );

 	foreach ($product_cats as $product_cat) {

 		if (in_array($product_cat->term_id, $allowed_product_categories)) {
 			for ($i = 1;$i <= $cart_data['quantity'];$i++)
 				$wine_list[] = $cart_data['data'];
 		}
 	}

}

$args = array(
	'post_type' 	=> 'product',
	'numberposts' 	=> -1,
	'meta_key' 		=> 'ab_wine_pack_capacity',
	'meta_value' 	=> '0',
	'orderby' 		=> 'meta_value',
	'order' 		=> 'ASC',
	'meta_compare' 	=> '>'
);

$box_list = array();

$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
	while ( $loop->have_posts() ) : $loop->the_post();
		
		global $product, $post;

		$box_cats = wp_get_post_terms($product->id, 'wine_packaging');

		if (!isset($box_list[$box_cats[0]->name])) {
			$box_list[$box_cats[0]->name] = array();
		}

		$box_list[$box_cats[0]->name][$product->id] = array(
			'id'		=> $product->id,
			'material'	=> $box_cats[0]->name,
			'title' 	=> $product->get_title(),
			'price' 	=> $product->get_price_html(),
			'image' 	=> $product->get_image('shop_single'),
			'capacity' 	=> get_post_meta($product->id, 'ab_wine_pack_capacity', true)
		);

	endwhile;
} else {
	
}

wp_reset_postdata();

// echo '<pre>';
// print_r($taken_products);
// echo '</pre>';


?>

	<div id="primary" class="content-area">
		<div class="wrapper">
			<main id="main" class="site-main" role="main">

			<div class="wine-packager oflow">
				<div class="wine-list">
					<h3>Your Wine</h3>
					<?php 
						foreach ($wine_list as $wine_item) { 

							/*
							if (isset($taken_products[$wine_item->id])) {
								if ($taken_products[$wine_item->id] > 0) {
									$taken_products[$wine_item->id]--;
									continue;
								}
							}
							*/
		
					?>
						<div class="wine" data-id="<?php echo $wine_item->id; ?>" data-previous-box-id="">
							<div class="image"><?php echo $wine_item->get_image('shop_catalog', array('title' => esc_attr($wine_item->get_title()))); ?></div>
							<div class="large_image"><?php echo $wine_item->get_image('shop_single', array('title' => esc_attr($wine_item->get_title()))); ?></div>
							<div class="title"><?php echo $wine_item->get_title(); ?></div>

							<label>
								Add to Box: 
								<select class="add-to-box">
									<option value="">Select a box</option>
									<?php foreach ($preset_boxes as $cart_item_key=>$cartData) { ?>
										<option value="<?php echo $cartData['index']; ?>">Box <?php echo $cartData['index']; ?></option>
									<?php } ?>
								</select>
							</label>
							<a class="close" href="#">x</a>
						</div>
					<?php } ?>
				</div>

				<form method="post" action="" class="save-packaging">
					<input type="hidden" name="wineData" class="wineData" value="" />
					<input type="hidden" name="task" value="add_more_wine" />
					<input type="submit" class="package-btn" value="Add More Wine" />
				</form>

				<div id="box-grid" class="box-builder">
					<h3>Your Boxes</h3>
					<div class="oflow">
						<?php 
							
							foreach ($preset_boxes as $cart_item_key=>$cartData) { 

								$box_cats = wp_get_post_terms($cartData['id'], 'wine_packaging');

								$the_box = $box_list[$box_cats[0]->name][$cartData['id']];
						?>
							<div class="box is-wine-cellar" data-capacity="<?php echo $the_box['capacity']; ?>" data-id="<?php echo $cartData['id']; ?>" data-preload="<?php echo esc_attr(json_encode($cartData['slots'])); ?>">
								<div class="box-options">
									<a class="edit_box" href="#">Change Material or Capacity</a> | <a class="delete_box" href="#">Delete Box</a>
								</div>
								<div class="info">
									<div class="image"><?php echo $the_box['image']; ?></div>
									<div class="meta-info">
										<div class="metaWrap">
											<h5></h5>
											<span class="material">Material: <span class="value"><?php echo $box_cats[0]->name; ?></span></span><br />
											<span class="capacity">Capacity: <span class="value"><?php echo $the_box['capacity']; ?></span></span><br />
											<span class="price">Price: <span class="value"><?php echo $the_box['price']; ?></span></span><br />
										</div>
									</div>
								</div>
								<?php
								
								for ($i = 1;$i <= $the_box['capacity'];$i++) {
									?>
									<div class="slot empty"></div>
									<?php
								}
								
								?>
							</div>
						<?php }  ?>
						
						<?php 
							/*
							foreach ($preset_boxes as $cart_item_key=>$cartData) { 

								$box_cats = wp_get_post_terms($cartData['id'], 'wine_packaging');

								$the_box = $box_list[$box_cats[0]->name][$cartData['id']];
						?>
							<div class="box is-wine-cellar" data-capacity="<?php echo $the_box['capacity']; ?>" data-id="<?php echo $cartData['id']; ?>">
								<div class="box-options">
									<a class="edit_box" href="#">Change Material or Capacity</a> | <a class="delete_box" href="#">Delete Box</a>
								</div>
								<div class="info">
									<div class="image"><?php echo $the_box['image']; ?></div>
									<div class="meta-info">
										<div class="metaWrap">
											<h5></h5>
											<span class="material">Material: <span class="value"><?php echo $box_cats[0]->name; ?></span></span><br />
											<span class="capacity">Capacity: <span class="value"><?php echo $the_box['capacity']; ?></span></span><br />
											<span class="price">Price: <span class="value"><?php echo $the_box['price']; ?></span></span><br />
										</div>
									</div>
								</div>
								<?php
								
								for ($i = 1;$i <= $the_box['capacity'];$i++) {

									if (isset($cartData['slots'][($i-1)])) {

										foreach ($wine_list as $wlik=>$wli) {
											if ($wli->id == $cartData['slots'][($i-1)]) {
												$wineProduct = $wine_list[$wlik];
											}
										}
										?>
										<div class="slot">
											<div class="wine" data-id="<?php echo $wineProduct->id; ?>">
												<div class="image"><?php echo $wineProduct->get_image('shop_catalog', array('title' => esc_attr($wineProduct->get_title()))); ?></div>
												<div class="large_image"><?php echo $wineProduct->get_image('shop_single', array('title' => esc_attr($wineProduct->get_title()))); ?></div>
												<div class="title"><?php echo $wineProduct->get_title(); ?></div>
												<a class="close" href="#">x</a>
											</div>
										</div>
										<?php
									} else {
										?>
										<div class="slot"></div>
										<?php
									}
								}
								
								?>
							</div>
						<?php } */ ?>
						<div class="box add-box">
							<span>CLICK HERE TO</span><br/>
							Add a Box
						</div>
					</div>
				</div>

				<div class="oflow">
					<form method="post" action="" class="save-packaging">
						<input type="hidden" name="wineData" class="wineData" value="" />
						<input type="hidden" name="task" value="continue_to_cart" />
						<input type="submit" class="package-btn" value="Save &amp; Continue to Cart" />
					</form>
				</div>
			</div>
			</main><!-- #main -->
		</div>
	</div><!-- #primary -->

	<div class="remodal" data-remodal-id="modal">
	  <button data-remodal-action="close" class="remodal-close"></button>
	  
	  <div id="box-editor">
			<?php 

			foreach ($box_list as $box_type=>$boxes) {
			?>
			<h3><?php echo $box_type; ?></h3>
				<div class="oflow">
					<?php
					foreach ($boxes as $box) { 

					?>
					<div class="box-preview" data-capacity="<?php echo $box['capacity']; ?>">
						<div class="image"><?php echo $box['image']; ?></div>
						<div class="info">
							<span class="capacity"><strong>Capacity:</strong> <?php echo $box['capacity']; ?></span><br/>
							<span class="price"><strong>Price:</strong> <?php echo $box['price']; ?></span>
							
							<br/>
							<input type="hidden" value="<?php echo esc_attr(json_encode($box)); ?>" />
							<a href="#" class="select-box button">Select Box</a>
						</div>
					</div>
					<?php } ?>
				</div>
			<?php
			}
			?>
		</div>
	</div>

	<script type="text/html" id="box_template">
		<div class="box is-wine-cellar" data-capacity="" data-id="">
			<div class="box-options">
				<a class="edit_box" href="#">Change Material or Capacity</a> | <a class="delete_box" href="#">Delete Box</a>
			</div>
			<div class="info">
				<div class="image"></div>
				<div class="meta-info">
					<div class="metaWrap">
						<h5></h5>
						<span class="material">Material: <span class="value"></span></span><br />
						<span class="capacity">Capacity: <span class="value"></span></span><br />
						<span class="price">Price: <span class="value"></span></span><br />
					</div>
				</div>
			</div>
		</div>
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function() {

			var edit_mode = false,
				$boxEditing,
				min_capacity = 0;

			var boxEditor = jQuery('[data-remodal-id=modal]').remodal({
				hashTracking : false
			});


			jQuery('.save-packaging').submit(function(e) {

				var $form = jQuery(this);

				var task = $form.find('input[name="task"]').val();

				var empty_cellar_posts = false;

				var formData = [];


				jQuery('.is-wine-cellar').each(function() {

					var capacity = jQuery(this).attr('data-capacity');

					var $wine_list = jQuery(this).find('.wine');

					var wineids = [];

					$wine_list.each(function() {
						wineids.push(jQuery(this).attr('data-id'));
					});

					formData.push({
						id 		: jQuery(this).attr('data-id'),
						slots 	: wineids
					});



					var totalSlotsFilled = $wine_list.length;

					if (capacity != totalSlotsFilled) {
						empty_cellar_posts = true;
					}

				});

				$form.find('.wineData').val(JSON.stringify(formData));

				if (empty_cellar_posts && task == 'continue_to_cart') {

					alert('Please fill up all empty slots on your box packages. If you require smaller boxes, please delete the offending box and create a new one.');
					e.preventDefault();
				}

			});

			jQuery('#box-grid').on('click', '.add-box', function() {

				edit_mode = false;

				boxEditor.open();

			});

			jQuery('#box-grid').on('click', '.edit_box', function() {

				edit_mode = true;
			
				$boxEditing = jQuery(this).parentsUntil('.box').parent();

				min_capacity = parseInt($boxEditing.attr('data-capacity'));

				boxEditor.open();

			});

			jQuery('#box-grid').on('click', '.delete_box', function() {

				var $parent = jQuery(this).parentsUntil('.box').parent();

		        var boxIndex = $parent.index();

		        console.log(boxIndex);

		        var boxID = boxIndex+1;

		         console.log(boxIndex, boxID);

				$parent.find('.close').each(function() {

					jQuery(this).trigger('click');

				});

				jQuery('.wine').each(function() {

					var previousBoxId = jQuery(this).data('previousBoxId');

					if (previousBoxId !== '') {

						previousBoxId = parseInt(previousBoxId);

						if (previousBoxId > boxID) {
							jQuery(this).data('previousBoxId', previousBoxId-1);
						}

					}

					jQuery(this).find('select option').each(function() {

						var val = jQuery(this).val();

						if (val === '') return;

						var valBoxId = parseInt(val);

						//console.log(valBoxId, boxID);

						if (valBoxId === boxID) {
							jQuery(this).remove();
						}

						if (valBoxId > boxID) {
							var lowerBoxID = valBoxId-1;
							jQuery(this).val(lowerBoxID).text('Box ' + lowerBoxID);

						}


					});

					



				});

				$parent.fadeOut(400, function() { $parent.remove(); });


			});

			jQuery('.select-box').click(function(e) {

				e.preventDefault();

				var new_capacity = jQuery(this).parentsUntil('.box-preview').parent().attr('data-capacity');

				var box_data = JSON.parse(jQuery(this).prev().val());

				console.log(box_data);

				if (edit_mode === true) {

					console.log($boxEditing);

					var currentCapacity = $boxEditing.attr('data-capacity');

					$boxEditing.attr('data-capacity', new_capacity);

					$boxEditing.attr('data-id', box_data.id);
					$boxEditing.find('.info .image').html(box_data.image);
					$boxEditing.find('.info .meta-info h5').html(box_data.title);
					$boxEditing.find('.info .meta-info .material .value').html(box_data.material);
					$boxEditing.find('.info .meta-info .capacity .value').html(box_data.capacity);
					$boxEditing.find('.info .meta-info .price .value').html(box_data.price);

					var diffCapacity = new_capacity - currentCapacity;

					for (i = 1;i <= diffCapacity;i++) {
						$boxEditing.append('<div class="slot empty"></div>');
					}


				} else {

					var $newEl = jQuery(jQuery('#box_template').html());

					$newEl.attr('data-capacity', new_capacity);

					$newEl.attr('data-id', box_data.id);
					$newEl.find('.info .image').html(box_data.image);
					$newEl.find('.info .meta-info h5').html(box_data.title);
					$newEl.find('.info .meta-info .material .value').html(box_data.material);
					$newEl.find('.info .meta-info .capacity .value').html(box_data.capacity);
					$newEl.find('.info .meta-info .price .value').html(box_data.price);

					for (i = 1;i <= new_capacity;i++) {
						$newEl.append('<div class="slot empty"></div>');
					}

					jQuery('.add-box').before($newEl);

				}

				boxEditor.close();

				var count = 0;

				var options = [];

				jQuery('.is-wine-cellar').each(function() {

					count++;
					options.push(count);

				});

				jQuery('.add-to-box').each(function() {

					var selectedValue = jQuery(this).find('option:selected').val() || false;

					var html = '<option value="">Select a box</option>';
					var selected;

					for (var i in options) {

						selected = '';

						if (options[i] == selectedValue) 
							selected = 'selected="selected"';

						html += '<option ' + selected + ' value="' + options[i] + '">Box ' + options[i] + '</option>';

					}

					jQuery(this).html(html);

				})

			});

			jQuery(document).on('opened', '.remodal', function () {

			  	console.log('Modal is opened');

			  	var $bep = jQuery('#box-editor').find('.box-preview');

				$bep.removeClass('hidden');

				console.log(edit_mode, min_capacity);

				if (edit_mode == true) {

					$bep.each(function() {

						var capacity = jQuery(this).attr('data-capacity');

						if (min_capacity <= capacity) {
							jQuery(this).removeClass('hidden');
						} else {
							jQuery(this).addClass('hidden');
						}

					})

				}

			});


		    jQuery(".wine-packager").on('click', '.close', function(e){

		    	e.preventDefault();

		        var $item = jQuery(this).closest('.wine');
		        var $box = jQuery(this).closest('.box');

		        var boxIndex = $box.index();
		        var wineId = $item.data('id');

		        jQuery('.wine.in_box[data-id="' + wineId + '"]').each(function() {
		        	var selectedBox = jQuery(this).find('select').val();

		        	if (selectedBox !== '') {
		        		selectedBox = parseInt(selectedBox)-1;
		        		if (boxIndex === selectedBox) {
		        			jQuery(this).find('select').val('').trigger('change');
		        		}
		        	}
		        });

		    });

		    jQuery('.add-to-box').change(function(e) {

		    	var boxID = jQuery(this).val();

		    	var $wine = jQuery(this).parentsUntil('.wine').parent();

		    	var previousBoxId = $wine.data('previousBoxId');

		    	//console.log('previousBoxId', boxID, previousBoxId);

		    	/* if new box is empty return 0 */
	    		if (boxID === '') {
	    			$wine.removeClass('in_box').data('previousBoxId', '');
	    		} else {

		    		boxID = parseInt(boxID);

			    	var $box = jQuery('.box.is-wine-cellar').eq(boxID-1);

			    	var emptySlots = $box.find('.slot.empty').length;

			    	if (emptySlots) {

			    		$wine.addClass('in_box').data('previousBoxId', boxID);

			    		var $slot = jQuery('<div>' + $wine.html() + '</div>');
			    		$slot.find('label').remove();
			    		$box.find('.slot.empty').first().removeClass('empty').html('<div class="wine" data-id="' + $wine.data('id') + '">' + $slot.html() + '</div>');
			    	} else {
			    		
			    		//console.log('goBackToPreviousValue', previousBoxId);

			    		jQuery(this).find('option[value="' + previousBoxId + '"]').attr('selected', 'selected').siblings().removeAttr('selected');
			    		alert('This box has no empty slots');
			    		return;
			    	}

			    }

			    if (previousBoxId !== '') {
		    		/* remove wine from a previous box */

		    		previousBoxId = parseInt(previousBoxId);

		    		var $previousBox = jQuery('.box.is-wine-cellar').eq(previousBoxId-1);

		    		$previousBox.find('.wine[data-id="' + $wine.data('id') + '"]:first').parent().addClass('empty').html('');
		    		
		    	}

		    });


		    /* Preload */
		    jQuery('.box.is-wine-cellar').each(function() {

		    	var slots = jQuery(this).data('preload');

		    	var $boxIndex = jQuery(this).index();

		    	console.log('boxIndex', $boxIndex);

		    	for (var i in slots) {

		    		var pid = slots[i];

		    		var boxId = $boxIndex+1;

		    		jQuery('.wine[data-id="' + pid + '"]').not('.in_box').first().find('option[value="' + boxId + '"]').attr('selected', 'selected').trigger('change');

		    	}

		    })


		});
	</script>
<?php get_footer(); ?>