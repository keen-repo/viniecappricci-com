<?php
/**
 * The template for displaying all pages.
 *
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

<?php 
	$title = rwmb_meta( '_vini_about_section_title');
	$text = rwmb_meta( '_vini_about_intro_text');
	$videos = rwmb_meta( '_vini_about_video', array( 'limit' => 1 ) );
	$media = rwmb_meta( '_vini_about_video_url');

	$video = reset( $videos );

	$membership = rwmb_meta( '_vini_fm_membership', 'type=textarea');
	$delivery = rwmb_meta( '_vini_fm_delivery', 'type=textarea');
	$call = rwmb_meta( '_vini_fm_call', 'type=textarea');
	$insta = rwmb_meta( '_vini_fm_insta','type=url');
	$fb = rwmb_meta( '_vini_fm_facebook','type=url');

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="entry-content">
					<div class="home section-product group">
						<?php 

						echo do_shortcode('[recent_products per_page="12" columns="6" operator="NOT IN" category="hamper-gift-boxes"]'); 

						echo do_shortcode('[button url="./shop" class="btn-secondary" target="self"] Show me more products [/button]');
						?>
					</div>

					<div class="clear">
						<?php 
						//the_content(); 
						get_sidebar('main');
					?>
					
					</div>
					
					
					<div class="home section-1">
						<div class="wrapper-1400 group flexbox">
						<?php if (!empty($media)): ?>
						<div class="span_1_of_2 video clear flex-item">
							<!-- <video src="<?php echo $video_url; ?>" type="video/mp4"> -->
							<?php echo $media; ?>
						</div>
						<?php endif; ?>
							
						
						<div class="<?php echo $cssClass = !empty($media) ? 'span_1_of_2 flex-item' : 'span_2_of_2'?>">
							<h1 style="text-align: center;">
								<?php echo $title; ?>
							</h1>
							<div class="content">
								<?php echo $text; ?>
							</div>
							<div class="button-link" style="text-align: center;">
							<?php echo do_shortcode('[button url="./concept" class="btn-secondary" target="self"] Read More [/button]'); ?>
								
							</div>
						</div>
					</div>
					</div>

					<div id="testimonials" class="home section-2">
						<h1 class="section-title">
							<?php _e('Testimonials' , 'vini'); ?>
						</h1>
						<div id="ls-testimonial">
							<?php

							global $post;
							$args = array( 'posts_per_page' => -1 , 'post_type' => 's_testimonial');

							$posts = get_posts( $args );
							foreach ( $posts as $post ) : setup_postdata( $post ); ?>
								<div class="item">
									<div class="item-content">
										<!-- <a href="<?php //the_permalink(); ?>"> -->
											<h3><?php the_title(); ?></h3>
											<?php the_content(); ?>
										<!-- </a> -->
									</div>

								</div>
							<?php endforeach; 
							wp_reset_postdata();?>

						</div>

					</div>

					<div id="partners" class="home section-3">
						<h1 class="section-title">
							<?php _e('Our Partners' , 'vini'); ?>
						</h1>
						<div id="ls-partners">
							<?php

							global $post;
							$args = array( 
								'numberposts' => -1, 
								'post_type' => 'partners',
								'orderby'            => 'rand',
							);

							$posts = get_posts( $args );
							foreach ( $posts as $post ) : setup_postdata( $post ); ?>
								<?php  $featured_img_url = get_the_post_thumbnail_url($post->ID, 'medium');  ?>
								<?php if (!empty($featured_img_url)): ?>
								<div class="item">
									<div class="item-content">
										<!-- <a href="<?php echo get_permalink( $post->ID ); ?>" class="block"> -->
											<img src="<?php echo $featured_img_url; ?>" alt="">
											<!-- <div class="image" style="background-image: url('<?php //echo $featured_img_url; ?>');"> -->
											<!-- </div> -->
										<!-- </a> -->
									</div>

								</div>
							<?php endif; ?>
							<?php endforeach; 
							wp_reset_postdata();?>

						</div>
						<div class="button-link" style="text-align: center;">
							<?php echo do_shortcode('[button url="./partners" class="btn-secondary" target="self"] View All Partners [/button]'); ?>
								
							</div>
					</div>

					<!-- <div id="partners" class="home section-3">
						<div class="wrapper-1400">
							<div class="group">
								<h1 class="section-title">
									<?php _e('Our Partners' , 'vini'); ?>
								</h1>
								<?php //echo do_shortcode('[rpwe limit="150" thumb="true" post_type="partners" post_status="publish" order="DESC" orderby="date" thumb_height="90" thumb_width="90"] '); ?>

									<div class="button-link" style="text-align: center;">
									<?php //echo do_shortcode('[button url="./partners" class="btn-secondary" target="self"] View All Partners [/button]'); ?>
										
									</div>
								</div>
							</div>
					</div> -->
 
					
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'vini' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<div class="footer-meta">
	<div class="wrapper-1400">
		<ul class="flexbox clear">
			<li class="span_1_of_4 flexbox">
				<a href="/loyalty-schemes" style="display: flex;text-decoration: none;">
					<span class="x-icon icon-membership-card-icon"></span>
					<h1>Loyalty Schemes
					<span>Start earning rewards</span></h1>
				</a>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-delivery-truck-icon"></span>
				<?php echo $delivery; ?>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-telephone-icon"></span>
				<?php echo $call; ?>
			</li>
			<li class="span_1_of_4 flexbox social">
				<h1><?php _e('Connect with Us ' , 'vini'); ?></h1>
				<a href="<?php echo $fb; ?>" target="_blank">
					<span class="x-icon icon-facebook-icon"></span>
				</a>
				<a href="<?php echo $insta; ?>" target="_blank">
				<span class="x-icon icon-instagram-icon"></span>
				</a>
			</li>
		</ul>
	</div>
</div>
<?php get_footer(); ?>