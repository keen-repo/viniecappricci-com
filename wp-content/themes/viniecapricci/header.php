<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vini
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NXR429M');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>


<?php
if (is_cart()) { 

	$allow_continue = true;

	$allowed_product_categories = array(14); // wine

	$total_wines = 0;
	$total_wines_in_packages = 0;
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_data ) {

		if (isset($cart_data['variation']['slots'])) {

			$capacity = get_post_meta($cart_data['product_id'], 'ab_wine_pack_capacity', true);

			$pslots = explode(',', $cart_data['variation']['slots']);

			$total = count($pslots);

			$total_wines_in_packages += $total;

			if ($total < $capacity) {
				$allow_continue = false;
				$error_msg = 'You have not completed your wine packaging. Please be sure that all slots are filled in a package. <a href="' . site_url('/package-your-wine/') . '">click here to complete this process.</a>';
				wc_add_notice($error_msg, 'error');
			}


		} else {

			$product_cats = wp_get_post_terms( $cart_data['product_id'], 'product_cat' );

		 	foreach ($product_cats as $product_cat) {

		 		if (in_array($product_cat->term_id, $allowed_product_categories)) {
		 			for ($i = 1;$i <= $cart_data['quantity'];$i++)
		 				$total_wines++;
		 		}

		 	}

		}

	}

	$show_wine_popup = false;

	$wine_popup_shown = WC()->session->get( 'wine_popup_shown', 0 );

	if ($wine_popup_shown) {
		/* lets check if there */

		if ($total_wines > $total_wines_in_packages) {
			$msg = 'You currently have some wines that have not be placed in special packaging. Would you like to <a href="' . site_url('/package-your-wine/') . '">specially package your wine?</a>';
			wc_add_notice($msg);
		}
	} else {

		if ($total_wines > $total_wines_in_packages) {

			WC()->session->set( 'wine_popup_shown', 1 );

			$show_wine_popup = true;

		}
	}

} 
?>

<script type="text/javascript">
	var viniAjaxURL = '<?php echo admin_url('admin-ajax.php'); ?>';
	<?php //if ($show_wine_popup) { ?>
		jQuery('.checkout-button').click(function(e) {

			e.preventDefault();

			alert('Do you want to add some wine?');

		});
	<?php //} ?>
</script>
	
	
	<script>
function nextTab(){
	if(jQuery('ul li.current').is(':last-child')){
	}else{
	var tab=jQuery('[data-tab="'+(jQuery('ul li.current').next().attr("data-tab"))+'"]');
    jQuery('ul li.current').removeClass('current');
    tab.closest('li').addClass('current');
	contTab();
	}
}
	
function prevTab(){
	if(jQuery('ul li.current').is(':first-child')){
	}else{
	var tab=jQuery('[data-tab="'+(jQuery('ul li.current').prev().attr("data-tab"))+'"]');
    jQuery('ul li.current').removeClass('current');
    tab.closest('li').addClass('current');
	contTab();
	}
}

function contTab(){
	var tabId = jQuery('li.current').data('tab');
    var catId = jQuery('li.current').data('id');
    var category = jQuery('li.current').data('category');
    if (category) {
        var clickCategory = category;
    }

    var mapCanvasId = 'map_canvas_' + catId;
    var locations = [];
    var infos = [];

    jQuery('.tab-content').removeClass('current fade-in');
    jQuery('#' + tabId).addClass('current fade-in');

    jQuery('.tab-content.current .locations').each(function() {
        var address = jQuery(this).data('address');
        var latLong = jQuery(this).data('latlng').split(',');
        var info = jQuery(this).html();
        var location = address + '|' + latLong[0] + '|' + latLong[1];
        locations.push(location);
        infos.push(info);
    });

    initialize(mapCanvasId, locations, infos, clickCategory);
	window.dispatchEvent(new Event('resize'));
}
</script>
	
	
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXR429M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php /*
	<div class="sitewide-note">
		Hampers ordered by <strong>Friday 15th December</strong> are guaranteed to be delivered before Christmas. Orders placed afterwards will be delivered by end of year 2017.
	</div>
*/ ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'vini' ); ?></a>

	<header class="site-woo-header" style="display: none;">
		<div class="clear">
			<?php $cssClass = get_theme_mod( 'themeslug_logo' ) ? 'site-logo' : 'site-branding' ?>
			<div class="col span_1_of_5 flex-item <?php echo $cssClass; ?>">
				<?php get_template_part('static/wrapper', 'logo' ); ?>
			</div>
		</div>
	</header>

	

	<header id="masthead" class="site-header" role="banner">
		<section class="section group flexbox wrapper-1400">
			<?php $cssClass = get_theme_mod( 'themeslug_logo' ) ? 'site-logo' : 'site-branding' ?>
			<div class="span_1_of_5 flex-item <?php echo $cssClass; ?>">
				<?php get_template_part('static/wrapper', 'logo' ); ?>
			</div>
			<div class="col span_4_of_5 flex-item site-nav">
				<?php 
					$del = rwmb_meta( '_vini_fm_delivery_header', 'type=textarea' ,10439);
				?>
				<?php if (!empty($del)): ?>
					<div class="block-1">
						<span class="x-icon icon icon-delivery-truck-icon"></span>
						<?php _e( $del , 'vini') ?>
					</div>
				<?php endif; ?>
				
				<?php get_template_part('static/wrapper', 'nav-main' ); ?>

				<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
			    $count = WC()->cart->cart_contents_count;
			    ?><div class="cart"><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
			    if ( $count > 0 ) {
			        ?>
			        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
			        <?php
			    }
			        ?></a></div>
				<?php } ?>	

			</div>
			<div class="toggle-menu">
				<a class="shiftnav-toggle shiftnav-toggle-button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i></a>
			</div>

		</section>
	</header><!-- #masthead -->

	<!-- Banner Text Ads -->
	<?php 
		$home_id = get_option('page_on_front');
		$text_ads = rwmb_meta( '_vini_text_ads', 'type=textarea', $home_id); 
		// var_dump( get_post_meta($home_id) ); 
	?>

	<?php if (!empty($text_ads)): ?>
		<div class="wrapper-text-ads">
			<div class="inner">
				<?php echo apply_filters('the_content', $text_ads ); ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="wrap-search">
		<?php get_search_form(); ?>
	</div>
	
	<?php 
		global $post;
		if ( is_home() ) {
			$id = get_option( 'page_for_posts' );
		}
		else{
			$id = $post->ID;
		}

		if (!is_page('hamper')) {
	?>
		<?php if ( is_page('home') ) : ?>
			<div class="home-banner">
		<?php endif; ?>
			<?php echo do_shortcode('[show_banner size="post-thumbnail-large" id="' . $id . '"]'); ?>

		<?php if ( is_page('home') ) : ?>
			<div class="box-featured-products">
				<?php echo  do_shortcode('[featured_products per_page="3" columns="3"]'); ?>
			</div>
		</div>
		<?php endif; ?>
	<?php } ?>
	

	<div class="section group section-hamper-sequence">
		<?php 
			$page = get_page_by_path( 'hamper' );
			$bg_image = rwmb_meta( '_vini_banner_hamper_image', 'type=image_advanced', $page->ID);
		?>
		<?php if( !empty( $bg_image ) ) : ?>

			<div class="section section-banner">
				<div class="wrap">
				<?php foreach ( $bg_image as $key =>  $img ) { ?>
					<div class="image fade-in" style="background-image:url('<?php echo $bg_image[$key]['full_url']; ?>');">

						<!-- Hamper Step 2 -->
						<div class="hamper-step hamper-step-2">
							<h3>2. Fill with items</h3>
							<a href="<?php echo site_url('/hamper/'); ?>" class="btn btn-white-text-border prev">Previous Step</a>
							<a href="<?php echo get_term_link(5495, 'product_cat'); ?>" class="btn btn-white-text-border next">Next Step</a>
						</div>

						<!-- Hamper Step 3 -->
						<div class="hamper-step hamper-step-3">
							<h3>3. Choose a gift</h3>
							<a href="<?php echo site_url('/shop/'); ?>" class="btn btn-white-text-border prev">Previous Step</a>
							<a href="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>" class="btn btn-white-text-border next">Next Step</a>
						</div>
						
						<!-- Hamper Step 4 -->
						<div class="hamper-step hamper-step-4">
							<h3>4. Add a message</h3>
							<a href="<?php echo get_term_link(5495, 'product_cat'); ?>" class="btn btn-white-text-border prev">Previous Step</a>
							<a href="<?php echo site_url('/hamper/hamper-creator/step-5/'); ?>" class="btn btn-white-text-border next">Next Step</a>
						</div>

						<!-- Hamper Step 5 -->
						<div class="hamper-step hamper-step-5">
							<h3>5. Hamper overview</h3>
							<a href="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>" class="btn btn-white-text-border prev">Previous Step</a>
						</div>
					</div>
				<?php } ?>
					
				</div>
			</div>
		<?php endif; ?>

		

		
		<!-- Hamper Progress Bar -->
		<div class="hamper-bar-wrapper">
			<div class="hamper-bar">
				<div class="progress">
					<div class="bar" style="width: 0%;"></div>
					<div class="total_pct">Total Price:</div>
					<div class="total_price">
						&euro;<span>0</span>
					</div>
				</div>

				<div>
					<a class="edit btn btn-white-text-border" href="#">Edit</a>
				</div>

				<div>
					<a class="checkout btn btn-white-text-border" href="#" data-checkout-url="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>">Checkout</a>
				</div>

			</div>
		</div>
		
		<!-- Hamper Edit Mode -->
		<div class="hamper-bar-edit-mode section group">
			<div class="hamper-items col span_3_of_4">
				<!-- Hamper items here -->
			</div>
			<div class="hamper-meta col span_1_of_4">
				<div class="wrap">
					<div class="basket">
						<img src="" alt="" />
						<span class="title"></span>
					</div>
					<div class="total">Total: <span class="currency">&euro;</span><span class="amount"></span></div>
					<a href="<?php echo site_url('/hamper/'); ?>" class="btn btn-red-border">Change Basket</a>
					<a class="checkout btn btn-red-border" href="#" data-checkout-url="<?php echo site_url('/hamper/hamper-creator/step-4/'); ?>">Checkout</a>
				</div>
			</div>
		</div>
	</div>


	<div id="content" class="site-content">
