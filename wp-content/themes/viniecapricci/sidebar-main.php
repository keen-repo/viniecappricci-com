<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vini
 */

if ( ! is_active_sidebar( 'home_main_widget' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area-full" role="complementary">
	<?php dynamic_sidebar( 'home_main_widget' ); ?>
</div><!-- #secondary -->
