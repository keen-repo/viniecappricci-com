<div class="wrap-widget">
	<div class="wrapper-1400">
		<div class="footer-widget flexbox section group wrapper-1400">
			<?php if ( is_active_sidebar( 'footer_left' ) ) : ?>
				<div id="footer-left" class="widget-main-area span_1_of_2 flexbox" role="complementary">
					<?php dynamic_sidebar( 'footer_left' ); ?>
					<div class="payment-method">
						<div> <?php _e( 'Payment Methods' , 'vini' ); ?></div>
						<div class="payment-logo"></div>
					</div>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>

			

			<?php if ( is_active_sidebar( 'footer_right' ) ) : ?>
				<div id="footer-right" class="widget-main-area span_1_of_2 flexbox" role="complementary">
					<?php dynamic_sidebar( 'footer_right' ); ?>
				</div><!-- #primary-sidebar -->
			<?php endif; ?>
		</div>
	</div>
</div>

