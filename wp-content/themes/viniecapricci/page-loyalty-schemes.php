<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

<?php 
	$membership = rwmb_meta( '_vini_fm_membership', 'type=textarea', 10439);
	$delivery = rwmb_meta( '_vini_fm_delivery', 'type=textarea', 10439);
	$call = rwmb_meta( '_vini_fm_call', 'type=textarea', 10439);
	$insta = rwmb_meta( '_vini_fm_insta','type=url', 10439);
	$fb = rwmb_meta( '_vini_fm_facebook','type=url', 10439);

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main loyalty-schemes" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

			<section class="loyalty-schemes-icons py-5">
				<div class="container">
                    <div class="row">
                        <div class="col-12">
							<h2 class="text-center item-title pb-5">
								<?php the_field('icons_title'); ?>
							</h2>
						</div>
						<?php if( have_rows('icons') ):  ?>
                        	<?php while( have_rows('icons') ): the_row();  ?>
						<div class="col-sm-12 col-md-4">
							<div class="item">
								<div class="item-info text-center">
									<img class="pb-4" src="<?php the_sub_field('image'); ?>">
									<h4 class="item-title">
										<?php the_sub_field('title'); ?>
									</h4>
									<p class="item-content">
										<?php the_sub_field('content'); ?>
									</p>
								</div>
							</div>
						</div>
						<?php endwhile;
                        	endif;?>
					</div>
				</div>
			</section>

			<section class="loyalty-schemes-cards py-5">
				<div class="container py-5">
                    <div class="row">
						<?php if( have_rows('cards') ):  ?>
                        	<?php while( have_rows('cards') ): the_row();  ?>
						<div class="col-sm-12 col-md-6">
							<div class="item">
								<div class="item-info">
									<img class="w-75 pb-5" src="<?php the_sub_field('image'); ?>">
									<h4 class="item-title pb-5">
										<?php the_sub_field('title'); ?>
									</h4>
									<p class="item-subtitle pb-4">
										<?php the_sub_field('sub_title'); ?>
									</p>
									<ul>
									<?php if( have_rows('bullet_points') ):  ?>
										<?php while( have_rows('bullet_points') ): the_row();  ?>
										<li class="pb-2">
										<?php the_sub_field('point'); ?>
										</li>
									<?php endwhile;
										endif;?>
									</ul>
									<a href="<?php the_sub_field('button_link'); ?>" class="btn btn-secondary" target="_self">
										<?php the_sub_field('button_text'); ?>
									</a>
								</div>
							</div>
						</div>
						<?php endwhile;
                        	endif;?>
					</div>
				</div>
			</section>

			<section class="loyalty-schemes-questions  py-5" id="faq">
				<div class="item">
                    <div class="container p-5">
						<div class="row">
							<div class="col-12">
								<div class="item-info">
									<h4 class="item-title text-center">
										<?php the_field('questions_title'); ?>
									</h4>
									<p class="item-subtitle text-center w-50 mx-auto">
										<?php the_field('questions_sub_title'); ?>
									</p>
								</div>
							</div>
						</div>
						<div class="accordion accordion-flush pt-3" id="accordionFlush">
							<div class="row">
								<?php $i=1;
								if( have_rows('questions') ):  ?>
									<?php while( have_rows('questions') ): the_row();  ?>
									<div class="col-sm-12 col-md-6">
										<div class="accordion-item">
											<h2 class="accordion-header" id="flush-heading<?php echo $i?>">
												<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse<?php echo $i?>" aria-expanded="false" aria-controls="flush-collapse<?php echo $i?>">
													<?php the_sub_field('question'); ?>
												</button>
											</h2>
											<div id="flush-collapse<?php echo $i?>" class="accordion-collapse collapse" aria-labelledby="flush-heading<?php echo $i?>" data-bs-parent="#accordionFlush">
												<div class="accordion-body"><?php the_sub_field('answer'); ?></div>
											</div>
										</div>
									</div>
								<?php $i++;
								endwhile;
									endif;?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="loyalty-schemes-tc py-5" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/build/images/vini-grey-pattern-background.png')">
				<div class="item">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<h4 class="item-title text-center pb-5">
									Terms & Conditions
								</h4>
							</div>
						</div>
						<ul class="numbered-list">
							<div class="row">
								<?php if( have_rows('terms_and_conditions') ):  ?>
									<?php while( have_rows('terms_and_conditions') ): the_row();  ?>
								<div class="col-sm-12 col-md-6">
									<li class="pb-2">
										<?php the_sub_field('term__condition'); ?>
									</li>
								</div>
								<?php endwhile;
									endif;?>
								<div class="col-sm-0 col-md-6">
								</div>
								<div class="col-sm-12 col-md-6 pt-5">
									<p>Loyalty scheme is compliant with the GDPR law. Read regulations <a href="/gdpr-regulations">here</a>.</p>
								</div>
							</div>
						</ul>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>

<footer class="entry-footer">
					<?php
						edit_post_link(
							sprintf(
								/* translators: %s: Name of current post */
								esc_html__( 'Edit %s', 'vini' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							),
							'<span class="edit-link">',
							'</span>'
						);
					?>
				</footer><!-- .entry-footer -->

<!-- <div class="footer-meta">
	<div class="wrapper-1400">
		<ul class="flexbox clear">
			<li class="span_1_of_4 flexbox">
				<a href="/loyalty-schemes" style="display: flex;text-decoration: none;">
					<span class="x-icon icon-membership-card-icon"></span>
					<h1>Loyalty Schemes
					<span>Start earning rewards</span></h1>
				</a>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-delivery-truck-icon"></span>
				<?php echo $delivery; ?>
			</li>
			<li class="span_1_of_4 flexbox">
				<span class="x-icon icon-telephone-icon"></span>
				<?php echo $call; ?>
			</li>
			<li class="span_1_of_4 flexbox social">
				<h1><?php _e('Connect with Us ' , 'vini'); ?></h1>
				<a href="<?php echo $fb; ?>" target="_blank">
					<span class="x-icon icon-facebook-icon"></span>
				</a>
				<a href="<?php echo $insta; ?>" target="_blank">
				<span class="x-icon icon-instagram-icon"></span>
				</a>
			</li>
		</ul>
	</div>
</div> -->

<?php get_footer(); ?>
