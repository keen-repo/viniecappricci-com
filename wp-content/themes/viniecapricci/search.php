<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package vini
 */

get_header(); ?>

	<section id="primary" class="content-area woocommerce">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'vini' ), '<span>' . get_search_query() . '</span>' ); ?> (<?php global $wp_query; echo $wp_query->found_posts;  ?>)</h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>

			<!-- Show search result -->
			<ul class="products section group">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>
					

				<?php endwhile; ?>
			</ul>
			<!-- End Show Result -->
			    <?php				  
				  previous_posts_link( ' << Back ' );
				  next_posts_link( 'Next >>  ', $the_query->max_num_pages );
				?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
