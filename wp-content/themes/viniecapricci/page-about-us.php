<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; // End of the loop. ?>

			<section class="our-team">
				<header class="section-header">
					<div class="wrapper-1400">
						<h2 class="text-center"><?php _e('Our Team' ,'_s'); ?></h2>
					</div>
				</header>
				<div class="wrapper-1400">

					<?php 
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'vc-member' ),
						'post_status'            => array( 'publish' ),
						'posts_per_page'         => '-1',
					);

					// The Query
					$query = new WP_Query( $args );
					?>
					<?php 
					if ( $query->have_posts() ) :  ?>

						<div class="box-team clear">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							
							<?php $image_url = get_the_post_thumbnail_url( get_the_ID() , array( 290, 338 ) ); ?>

							<div class="item">
								<div class="item-info">
									<div class="image" style="background-image: url('<?php echo $image_url; ?>')"></div>
									<h4 class="item-title">
										<?php echo get_the_title(); ?>
									</h4>
									<p class="item-content">
										<?php echo get_the_content(); ?>
									</p>
								</div>
							</div>

						<?php endwhile; ?>
						</div>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>

				<div class="wrapper-link">
					<p style="text-align: center;">
						<a href="<?php echo esc_url( home_url( '/the-vacancy' ) );?>" class="btn btn-secondary" target="_self">
							<?php _e('Are you interested in joining our team?','vini') ?>
						</a>
					</p>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
