<?php

if (!isset($_SESSION['hamper_mode'])) {
	
	wp_redirect(site_url('/hamper/step-1/'));
	exit();
	
}

$active_hamper = $_SESSION['active_hamper'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$task = $_POST['task'];
	
	if ($task == 'update_hamper') {

		// get the forms active hamper, just incase the active hamper was switched in another tab.
		$active_hamper = $_POST['active_hamper'];
		
		$the_hamper = $_SESSION['hampers'][$active_hamper];
		
		$used_space = 0;
		
		// total possible items that can fit in this box
		$total_possible_items = get_post_meta($the_hamper['box'], 'total_possible_items', true);
		
		// loop through all hamper items
		foreach ($the_hamper['items'] as $k=>$item) {
			
			// if this item should be removed we unset it from hamper session
			if (in_array($item['id'], $_POST['remove'])) {
				
				// lets update the percent full
				// remove item from cart array
				unset($_SESSION['hampers'][$active_hamper]['items'][$k]);
				
				continue;

			} else {
			
				/* quantity code hidden
					// new item quantity
					$item['quantity'] = $_POST['quantity'][$item['id']];

					// set item quantity to session
					$_SESSION['hampers'][$active_hamper][$k]['quantity'] = $item['quantity'];
				 * 
				 */
				
			}

			// get new used space.
			$used_space += $item['hamper_space'] * $item['quantity'];
			
		}
		
		$used_space_pct = round(($used_space * 100) / $total_possible_items);
			
		$_SESSION['hampers'][$active_hamper]['percent'] = $used_space_pct;
		
	}

	if ($task == 'finish_hamper') {
		
		$active_hamper = $_POST['active_hamper'];

		$_SESSION['hampers'][$active_hamper]['message'] = $_POST['message'];
		
		
		$the_hamper = $_SESSION['hampers'][$active_hamper];
		
		
		$sku = strtolower(sha1(time() . mt_rand()));
		
		$hamper_contents = array();
		
		$hamper_contents[] = 'Hamper Box: ' . get_the_title($the_hamper['box']);
		
		$hamper_contents[] = '';
		
		$total_price = get_post_meta($the_hamper['box'], '_price', true);
		
		foreach ($the_hamper['items'] as $item) {
			
			$total_price += get_post_meta($item['id'], '_price', true) * $item['quantity'];
			
			$hamper_contents[] = get_the_title($item['id']) . '<br /><strong>SKU:</strong>' . get_post_meta($item['id'], '_sku', true) . '<br /><strong>Quantity:</strong> ' . $item['quantity'] . '<br /><br />';
			
		}

		$product_id = wp_insert_post(array(
			'post_title'	=> get_the_title($the_hamper['box']),
			'post_name'		=> $sku,
			'post_content'	=> implode('<br />', $hamper_contents),
			'post_status'	=> 'publish',
			'post_author'	=> 1,
			'post_type'		=> 'product'
		));

		if (is_wp_error($product_id)) {

			$error = $product_id->get_error_message();

		} else {
			
			$success = true;
			
			$_SESSION['hampers'][$active_hamper]['product_link'] = $product_id;
			
			/* Lets clear the current hamper since we dont need it anymore */
			unset($_SESSION['hampers'][$active_hamper]);
			
			if (empty($_SESSION['hampers'])) {
				// no more hampers lets get out of hamper mode
				unset($_SESSION['active_hamper'], $_SESSION['hamper_mode'], $_SESSION['hampers']);
				
			} else {
				// we still have some hampers so lets find one and set it to active.
				foreach ($_SESSION['hampers'] as $key=>$items) {
					
					$_SESSION['active_hamper'] = $key;
					break;
					
				}
		
			}
			
			
			$box_image = get_post_thumbnail_id($the_hamper['box']);
			
			set_post_thumbnail( $product_id, $box_image );

			// we set this product to be a hamper, another authenticity test
			update_post_meta($product_id, '_hamper', 1);
			
			// used so that if i need to delete this product i delete the attached session too
			// this will make a clean delete.
			update_post_meta($product_id, '_session_link', $active_hamper);

			update_post_meta($product_id, 'hamper_message', $_POST['message']);

			update_post_meta($product_id, '_regular_price', $total_price);
			update_post_meta($product_id, '_price', $total_price);
			update_post_meta($product_id, '_sku', $sku);

			update_post_meta($product_id, 'total_sales', 0);
			update_post_meta($product_id, '_tax_class', '');

			update_post_meta($product_id, '_purchase_note', '');
			update_post_meta($product_id, '_weight', '');
			update_post_meta($product_id, '_length', '');
			update_post_meta($product_id, '_width', '');
			update_post_meta($product_id, '_height', '');
			update_post_meta($product_id, '_sale_price_dates_from', '');
			update_post_meta($product_id, '_sale_price_dates_to', '');
			update_post_meta($product_id, '_sold_individually', '');
			update_post_meta($product_id, '_stock', '');
			update_post_meta($product_id, '_product_image_gallery', '');

			update_post_meta($product_id, '_product_attributes', array());

			update_post_meta($product_id, '_sale_price', '');
			update_post_meta($product_id, '_featured', 'no');
			update_post_meta($product_id, '_visibility', 'hidden');
			update_post_meta($product_id, '_stock_status', 'instock');
			update_post_meta($product_id, '_downloadable', 'no');
			update_post_meta($product_id, '_virtual', 'no');
			update_post_meta($product_id, '_backorders', 'no');
			update_post_meta($product_id, '_manage_stock', 'no');
			update_post_meta($product_id, '_tax_status', 'taxable');
			
			$woocommerce->cart->maybe_set_cart_cookies();

			$response = $woocommerce->cart->add_to_cart($product_id); 

		}
		
		
		
		
		
		
	}
	
	/* Lets connect to Woocommerce API to add a dynamic product */
	
}


$the_hamper = $_SESSION['hampers'][$active_hamper];

$the_message = $_SESSION['hampers'][$active_hamper]['message'];

get_header(); ?>

	<div id="primary" class="content-area">
		<div class="wrapper">
			<main id="main" class="site-main" role="main">
				<?php
					if (isset($_SESSION['hampers'])) {
					foreach ($_SESSION['hampers'] as $key=>$hamper) { 

						$class = '';

						if ($key == $_SESSION['active_hamper']) {

							$class = 'active';

						}
				?>

					<div class="hamper_bar <?php echo $class; ?>" id="hamper_bar_<?php echo $key; ?>">
						<div class="how_fill">How filled is my hamper?</div> 
						<div class="progress">

							<?php 

							$bar_style = 'display: none;';
							$outside_style = '';
							if ($hamper['percent'] > 90) { 

								$bar_style = '';
								$outside_style = 'display: none;';

							}

							?>
							<div class="bar" style="width: <?php echo $hamper['percent']; ?>%;"><span style="<?php echo $bar_style; ?>"><?php echo $hamper['percent']; ?>%</span></div>
							<span class="outside" style="<?php echo $outside_style; ?>">Total Price:</span>
							<div class="clear"></div>
						</div>

						<?php if ($class == 'active') { ?>
							<a href="<?php echo site_url('/hamper/step-1/'); ?>">Upgrade Basket</a>
						<?php } else { ?>
							<a class="hamper_make_active" href="#hamper-<?php echo $key; ?>">Make Active</a>
						<?php } ?>
							<div class="clear"></div>
					</div>
					<?php
						}
					}
				?>
				
				<?php if (isset($success)) { ?>
				
					<div class="woocommerce-message">
						<a class="button" href="<?php echo site_url('/cart/'); ?>">View Cart</a> 
						Hamper was successfully added to your cart.
					</div>
					
					<div class="hamper_step_4">
						<div class="right">
							<a class="button" href="<?php echo site_url('/hamper/step-1/?create=1'); ?>">Create New Hamper</a>

							<input type="button" value="Go to Checkout" onclick="document.location.href='<?php echo site_url('/cart/'); ?>'" />
						</div>
						<div class="clear"></div>
					</div>
					
				<?php } else { ?>
						
				
				
					<div class="hamper_steps">
						<h3>Build your own <strong>Hamper</strong></h3>
						<div class="steps">
							<div class="small_step"><strong>Step 1.</strong> Select Your Box Size</div>
							<div class="small_step"><strong>Step 2.</strong> Add Items</div>
							<div class="small_step"><strong>Step 3.</strong> Select Gift</div>
							<div class="large_step last">
								<div class="step_text">Step</div>
								<div class="no">4</div>
								<div class="desc">Add a message</div>
								<div class="clear"></div>
							</div>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/images/build_hamper_step_4.png" alt="" class="bottom_right" />
					</div>
				
					<?php if (isset($error)) { ?>
						<?php echo $error; ?>
					<?php } ?>
				
				
				
					<form method="post" action="<?php the_permalink(); ?>">
						<input type="hidden" name="task" value="update_hamper" />
						<input type="hidden" name="active_hamper" value="<?php echo $active_hamper; ?>" />

						<strong style="font-size: 22px;"><?php echo get_the_title($the_hamper['box']); ?></strong><br />

						<table>
							<tr>
								<th colspan="2">Product</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total</th>
								<th>Remove</th>
							</tr>
						<?php foreach($the_hamper['items'] as $item) { ?>

							<tr>
								<td>

								</td>
								<td><?php echo get_the_title($item['id']); ?></td>
								<td>&euro; <?php echo $item['price']; ?></td>
								<td><?php echo $item['quantity']; ?></td>
								<td>&euro;  <?php echo number_format(($item['price'] * $item['quantity']),2); ?></td>
								<td align="center"><input type="checkbox" name="remove[]" value="<?php echo $item['id']; ?>" /></td>
							</tr>

						<?php } ?>

						</table>

						<input type="submit" value="Update Hamper" />
					</form>
					&#160;<br /><br /><br />
					<form method="post" action="<?php the_permalink(); ?>">
						<input type="hidden" name="task" value="finish_hamper" />
						<input type="hidden" name="active_hamper" value="<?php echo $active_hamper; ?>" />

						Please enter your message below
						<textarea id="hamper_message" name="message"><?php echo $the_message; ?></textarea><br />

						<div class="center">
							<input type="submit" name="next_step" id="next_step" value="Add Hamper to Cart" />
						</div>
					</form>
					<div class="hamper_step_4">
						<div class="left">
							<input type="button" value="Back" onclick="document.location.href='<?php echo site_url('/product-category/acces/'); ?>'" />
						</div>
						<div class="clear"></div>
					</div>
				<?php } ?>
			</main><!-- #main -->
		</div>
	</div><!-- #primary -->
	<script type="text/javascript">
		jQuery(document).ready(function() {
	
			jQuery('#hamper_message').maxlength({max: 20});

		});
	</script>
<?php get_footer(); ?>
