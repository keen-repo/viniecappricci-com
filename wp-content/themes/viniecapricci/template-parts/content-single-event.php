<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */



	$images = rwmb_meta( 'mb_banner_post_images','type=image_advanced' );
	
	if ( !empty($images) ){
		foreach ($images as $image) {
			echo '<div class="image" style="background-image: url(\''.wp_get_attachment_image_url( $image[0], 'post-thumbnail-large' ) .'\')"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
		}
	}else{
		echo '<div class="image"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
	}
	$sellTicket = rwmb_meta( 'mb_event_sell','type=radio' );
	$date = rwmb_meta( 'mb_event_date' , 'type=date' );
	$end_date = rwmb_meta( 'mb_event_end_date' , 'type=date' );
	$startTime = rwmb_meta( 'mb_event_start_time' , 'type=time' );
	$endTime = rwmb_meta( 'mb_event_end_time' , 'type=time' );
	
	$isSellable = rwmb_meta( 'mb_event_sell' );	
	
	$current_url = home_url(add_query_arg(array(),$wp->request));
	
	$maximum_tickets = rwmb_meta( 'mb_event_stock' );
	
	$productWoocommerce = rwmb_meta( 'mb_event_product' );

	// VARIABLE PRODUCTS
	$product = wc_get_product($productWoocommerce);

	if($product){
		$attributes = array_keys($product->get_attributes() );
		if ($product->product_type == 'variable') {
			$variableProduct = new WC_Product_Variable( $productWoocommerce );
			$available_variations = $variableProduct->get_available_variations();
		}
	}

	 /* SOLD UNITS */
	$units_stock = get_post_meta( $productWoocommerce, '_stock', true );
	
	$soldOut = false;
	if ($units_stock <= 0){
		$soldOut = true;
	}

	$date = strtotime($date);
	$formattedDate = date('l, jS F Y', $date);
	$end_date = strtotime($end_date);
	$end_date = date('l, jS F Y', $end_date);



?>

<div class="single-two-cols">
	<article id="post-<?php the_ID(); ?>" <?php post_class('event-entry'); ?>>
		<div class="box-ticket">
		<?php 			
			if ($sellTicket): ?>
				
				<ul id="alertStock" class="woocommerce-error" >
					<li><a href="<?php echo site_url().'/cart'; ?>" class="button wc-forward">View Cart</a> <span></span></li>
				</ul>

					
					<?php if ( ( $isSellable == '1' ) && ( $soldOut == false ) ):?>
					<?php 								
									if ($units_stock > 10){
										$max = $units_stock;
									}
									else {
										$max = $units_stock;
									}
									?>
						<div class="interestedEvent"><?php _e( 'Interested in this event? Purchase a ticket now.' , 'vini' ) ?></div>
						<div class="isSellableRight">
		<div class="purchase-ticket">

		<?php if ($product->product_type == 'variable') : ?>
			<?php 


				// Loop through available variation data
			    foreach ( $available_variations as $key => $variation_data ) 
			    {
				    $url = '?add-to-cart=' . $productWoocommerce . '&variation_id=' .$variation_data['variation_id']; // The dynamic variation ID (URL)
				    // Loop through variation product attributes data
				    foreach ( $variation_data['attributes'] as $attr_key => $term_slug ) {
				    	
				            $button_text = __($term_slug . ' - &euro; ' . $variation_data['display_price'], "woocommerce");
				    		$url .= '&'.$attr_key.'='.$term_slug; // Adding the product attribute name with the term value (to Url)
				    }
				    // Displaying the custom variations add to cart buttons
		             echo '<a id="buttonAddTickets" href="'.$url.'" style="margin-bottom:10px;" class=" buttonAddTickets button alt" data-maximum_tickets="'.$max.'" data-product-type="'. $product->product_type .'" data-quantity="1" data-product_id="'.$productWoocommerce.'" >'.$button_text.'</a> ';
			    }//$availablevariations
			?>
		<?php else: ?>
			<a rel="nofollow" href="<?php  echo $current_url; ?>/?add-to-cart=<?php echo $productWoocommerce; ?>" data-quantity="1" data-product_id="<?php echo $productWoocommerce;?>" data-product_sku="" data-maximum_tickets="<?php echo $max; ?>" class="button product_type_simple add_to_cart_button ajax_add_to_cart added buttonAddTickets" id="buttonAddTickets">Purchase Ticket</a>

		<?php endif; ?>	
							</div>	

		<?php //if ($product->product_type != 'variable'): ?>	

		
			
		<span class="isSellable" style="float:right">
		
			<div class="qty2">QTY</div>
			<div class="wrapper-button subtract">
				<button class="custom-substract-product">-</button>
			</div>
				<input id="quantityTickets" type="number" step="1" min="1" max="<?php echo $max; ?>" name="quantity[89]" value="0" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">					
				
			<div class="wrapper-button add">
				<button class="custom-add-product">+</button>
			</div>
		</div>
		<div id="eventAlert" class="woocommerce" style="display:none">
			<div id="closeAlert">X</div>
			<h2 class="box-event">Success</h2>
			<div id="alertQuanitityAdded"></div>
				<a href="<?php echo site_url().'/cart'; ?>" class="button wc-forward">View Cart</a>
			</div>	
		</span>
	<?php //endif; ?>
	<?php else: ?>
			<span><?php _e( 'Tickets Sold Out' , 'vini' ) ?></span>					
	<?php endif; ?>

				</div>
				
		<?php endif; ?>
		<div class="entry-content">
			<style>
				span.days:not(:last-of-type){
					position: relative;
				}

				span.days:not(:last-of-type)::after {
					content: ",";
				}
			</style>
			<p class="event-schedule">
				<span><?php _e( 'Event Date: ', 'vini' ) ?></span>
				<?php echo $formattedDate; ?> - <?php echo $end_date; ?><br>

				<?php if (!empty($startTime) ) :?>
					<span><?php _e( 'Start Time: ', 'vini' ) ?></span><?php echo date('h:i A', strtotime($startTime)) ?>
				<?php endif; ?>

				<?php if (!empty($endTime) ) :?>
					|  <span><?php _e( 'End Time: ', 'vini' ) ?></span><?php echo date('h:i A', strtotime($endTime)) ?>
				<?php endif; ?>
				<br>

				<?php $days = rwmb_meta( 'mb_event_available_days' );
						if (!empty($days)) { ?>
				<span><?php _e( 'Every: ', 'vini' ); ?></span>
				
						<?php foreach ( $days as $day ) {  ?>
							   <span class="days" style="text-transform: capitalize; color:#221f1f; font-weight: normal; font-size: 14px;"><?php echo $day; ?></span>
				<?php }
						}
					?>


			</p> 
			<?php the_content( '<span>read more</span>', TRUE ); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
	<?php get_sidebar(); ?>
</div>