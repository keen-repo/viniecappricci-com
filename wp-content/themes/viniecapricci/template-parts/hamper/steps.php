<?php if (vini_is_hamper_creator() == 'step-1') { ?>
<div class="hamper_steps">
	<h3>Build your own <strong>Hamper</strong></h3>
	<div class="steps">
		<div class="large_step">
			<div class="step_text">Step</div>
			<div class="no">1</div>
			<div class="desc">Select Your Box Size</div>
			<div class="clear"></div>
		</div>
		<div class="small_step"><strong>Step 2.</strong> Add Items</div>
		<div class="small_step"><strong>Step 3.</strong> Select Gift</div>
		<div class="small_step last"><strong>Step 4.</strong> Add a message</div>
	</div>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/build_hamper_step_1.png" alt="" class="bottom_right" />
</div>
<?php } ?>

<?php if (is_shop()) { ?>
	<div class="hamper_steps">
		<h3>Build your own <strong>Hamper</strong></h3>
		<div class="steps">
			<div class="small_step"><strong>Step 1.</strong> Select Your Box Size</div>
			<div class="large_step">
				<div class="step_text">Step</div>
				<div class="no">2</div>
				<div class="desc">Add items <br /> to your hamper</div>
				<div class="clear"></div>
			</div>
			<div class="small_step"><strong>Step 3.</strong> Select Gift</div>
			<div class="small_step last"><strong>Step 4.</strong> Add a message</div>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/build_hamper_step_2.png" alt="" class="bottom_right" />
	</div>
<?php } ?>

<?php if (is_tax('product_cat', 5495)) { ?>
<div class="hamper_steps">
	<h3>Build your own <strong>Hamper</strong></h3>
	<div class="steps">
		<div class="small_step"><strong>Step 1.</strong> Select Your Box Size</div>
		<div class="small_step"><strong>Step 2.</strong> Add Items</div>
		<div class="large_step">
			<div class="step_text">Step</div>
			<div class="no">3</div>
			<div class="desc">Select your gift</div>
			<div class="clear"></div>
		</div>
		<div class="small_step last"><strong>Step 4.</strong> Add a message</div>
	</div>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/build_hamper_step_3.jpg" alt="" class="bottom_right" />
</div>
<?php } ?>

<?php if (vini_is_hamper_creator() == 'step-4') { ?>
<div class="hamper_steps">
	<h3>Build your own <strong>Hamper</strong></h3>
	<div class="steps">
		<div class="small_step"><strong>Step 1.</strong> Select Your Box Size</div>
		<div class="small_step"><strong>Step 2.</strong> Add Items</div>
		<div class="small_step"><strong>Step 3.</strong> Select Gift</div>
		<div class="large_step last">
			<div class="step_text">Step</div>
			<div class="no">4</div>
			<div class="desc">Add a message</div>
			<div class="clear"></div>
		</div>
	</div>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/build_hamper_step_4.png" alt="" class="bottom_right" />
</div>
<?php } ?>

<?php if (is_shop() || is_tax('product_cat', 5495) || vini_is_hamper_creator() == 'step-4') { ?>

	<div class="hamper_bar active" id="hamper_bar">
		<div class="how_fill">How filled is my hamper?</div> 
		<div class="progress">
			<div class="bar" style="width: 0%;"><span>0%</span></div>
			<span class="outside" style="">Total Price:</span>
			<div class="clear"></div>
		</div>

		<a href="<?php echo site_url('/hamper/hamper-creator/step-1/'); ?>">Upgrade Basket</a>
		
		<div class="clear"></div>

	</div>
<?php } ?>




