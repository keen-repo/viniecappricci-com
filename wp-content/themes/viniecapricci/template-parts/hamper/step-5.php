<?php get_header(); ?>

	<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="wrapper section-hamper-preview">

					<div class="hamper-success-message">Hamper successfully added to cart. You will be redirected to checkout now.</div>

					<div class="hamper-preview"></div>
					<div class="center">
						<input type="button" class="btn btn-red-border" id="add_hamper_to_cart" data-checkout-url="<?php echo WC()->cart->get_cart_url(); ?>" value="Add Hamper to Cart" />
					</div>
					<!-- <a href="<?php //echo site_url('/product-category/acces/'); ?>">Back</a> -->
				</div>

			</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>


