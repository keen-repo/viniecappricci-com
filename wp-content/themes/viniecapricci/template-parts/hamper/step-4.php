<?php get_header(); ?>

	<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="wrapper section-hamper-add-message">
					<h1><?php _e('Personalise your hamper with a message:', THEMELANG ); ?></h1>
					<textarea id="hamper_message" name="message" rows="10" placeholder="Message"></textarea><br />

					
				</div>
				<div class="center">
					<input type="button" class="btn btn-secondary" id="hamper_go_to_step_5" data-checkout-url="<?php echo site_url('/hamper/hamper-creator/step-5/'); ?>" value="Proceed to Checkout" />
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>


