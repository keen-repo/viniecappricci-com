<?php get_header(); ?>

	<div class="home-banner">
		<?php echo do_shortcode('[show_banner size="post-thumbnail-large" id="' . get_the_ID() . '"]'); ?>

		<div class="box-hamper-steps">
			<div class="woocommerce columns-4">
			
				<ul class="products">
					<li class="product">
						<a href="#" class="woocommerce-LoopProduct-link">
							<div class="wrap-product-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/hamper_step_1_basket.jpg" alt="" /></div>
							<span class="step">1</span>
							<h3>Select a<br />Basket</h3>
						</a>
					</li>
					<li class="product">
						<a href="#" class="woocommerce-LoopProduct-link">
							<div class="wrap-product-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/hamper_step_2_products.jpg" alt="" /></div>
							<span class="step">2</span>
							<h3>Fill with<br />items</h3>
						</a>
					</li>
					<li class="product">
						<a href="#" class="woocommerce-LoopProduct-link">
							<div class="wrap-product-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/hamper_step_3_gifts.jpg" alt="" /></div>
							<span class="step">3</span>
							<h3>Choose a<br />Gift</h3>
						</a>
					</li>
					<li class="product">
						<a href="#" class="woocommerce-LoopProduct-link">
							<div class="wrap-product-thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/hamper_step_4_message.jpg" alt="" /></div>
							<span class="step">4</span>
							<h3>Add a<br />message</h3>
						</a>
					</li>
				</ul>

			
			</div>
		</div>
	</div>

	<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="section group section-hamper-steps">

						<h1><?php _e(' 1. Select your basket ' , THEMELANG ) ?></h1>

						<?php while ( have_posts() ) : the_post(); ?>

							<div class="three_column">
									<?php
								echo do_shortcode('[product_category category="hamper-gift-boxes" limit="3"]'); 
								?>
							</div>

						<?php endwhile; // end of the loop. ?>
						
				</div>
				<div class="box-btn-steps">
					<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="btn btn-secondary" id="step_1_continue"><?php _e('Next Step &raquo;', THEMELANG ); ?></a>
				</div>
				
			</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
