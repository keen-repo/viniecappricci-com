<?php
/**
 * Template part for displaying single partner.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

?>

<div id="tab-<?php the_ID(); ?>" class="tab-content">
	<?php  
		$partnername = get_the_title();
		$website = rwmb_meta( 'mb_partner_url','type=url', get_the_ID() );
		$address = rwmb_meta( 'mb_partner_address','type=text', get_the_ID() );

		$map_args = array(
		    'type'         => 'map',   // Required
		    'width'        => '100%', // Map width, default is 640px. Can be '%' or 'px'
		    'height'       => '540px', // Map height, default is 480px. Can be '%' or 'px'
		    'zoom'         => 14,      // Map zoom, default is the value set in admin, and if it's omitted - 14
		    'marker'       => true,    // Display marker? Default is 'true',
		    'marker_title' => '<h1>' . $partnername . '</h1><p>' . $website . '</p>',     // Marker title when hover
		    'marker_icon' =>  TEMPL_DIR .'/assets/build/images/map-marker-yellow.png',
		    'info_window'  => '<div class="map-info-window"><h1>' . $partnername . '</h1><p class="address">' . $address . '</p><p><a href="' . $website . '" target="_blank">' . $website . '</a></p></div>', // Info window content, can be anything. HTML allowed.
		    'styles'   => '[{featureType: "all",stylers: [{ saturation: -100 } ]}]',
		    'js_options'   => array(),
		    'api_key'      => 'AIzaSyBjl_h9cQ35lLb-LxHPQudWhP42uOit8x0',
		);

		$map = rwmb_meta( 'mb_partner_map', $map_args );
		echo $map;

	?>
</div>