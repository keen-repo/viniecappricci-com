<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

?>

<?php $product = wc_get_product( get_the_ID() );?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a href="<?php echo get_permalink(); ?>" rel="bookmark">

	<div class="wrap-product-thumbnail">

       <?php 
	       	if ( has_post_thumbnail() ) { 
	       		 $product_image_url =  wp_get_attachment_image_url( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
	       		 <div class="wrap-product-thumbnail" style="background-image: url('<?php echo $product_image_url; ?>');"></div>
	       <?php }else{
	            echo '<img src="'. get_bloginfo('template_directory').'/assets/build/images/vini-image-placeholder.png" alt="Vini e Cappricci" />';
	        }
        ?>
    </div>
		<header class="entry-header">
			<h2><?php the_title(); ?></h2>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<div class="price"><?php echo $product->get_price_html(); ?></div>
		</div><!-- .entry-summary -->
	</a>
</div><!-- #post-## -->

