<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

	$images = rwmb_meta( 'mb_banner_post_images','type=image_advanced' );
	
	if ( !empty($images) ){
		foreach ($images as $image) {
			echo '<div class="image" style="background-image: url(\''.wp_get_attachment_image_url( $image[0], 'post-thumbnail-large' ) .'\')"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
		}
	}else{
		echo '<div class="image"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
	}
	$sellTicket = rwmb_meta( 'mb_event_sell','type=radio' );
	$date = rwmb_meta( 'mb_event_date' , 'type=date' );
	$startTime = rwmb_meta( 'mb_event_start_time' , 'type=time' );
	$endTime = rwmb_meta( 'mb_event_end_time' , 'type=time' );
	
	$isSellable = rwmb_meta( 'mb_event_sell' );	
	
	$current_url = home_url(add_query_arg(array(),$wp->request));
	
	$maximum_tickets = rwmb_meta( 'mb_event_stock' );

	
	$productWoocommerce = rwmb_meta( 'mb_event_product' );
	
	
	/* SOLD UNITS */
	$units_sold = get_post_meta( $productWoocommerce, 'total_sales', true );
	$soldOut = false;
	if ($units_sold >= $maximum_tickets){
		$soldOut = true;
	}
	
	$date = strtotime($date);
	$formattedDate = date('l, jS F Y', $date);
?>

<div id="eventAlert" class="woocommerce" style="display:none">
	<div id="closeAlert">X</div>
	<h2 class="box-event">Success</h2>
	<div id="alertQuanitityAdded"></div>
	<a href="<?php echo site_url().'/cart'; ?>" class="button wc-forward">View Cart</a>
</div>

<div class="single-two-cols">

	<article id="post-<?php the_ID(); ?>" <?php post_class('event-entry'); ?>>
		<ul id="alertStock" class="woocommerce-error">
			<li>
				<a href="<?php echo site_url().'/cart'; ?>" class="button wc-forward">View Cart</a> <span></span>
			</li>
		</ul>
		<?php 			
			if ($sellTicket): ?>
				<div class="box-ticket">
				

					<?php if ( ( $isSellable == '1' ) && ( $soldOut == false ) ):?>
					<?php 								
									if ($maximum_tickets < 11){
										$max = $maximum_tickets-$units_sold;
									}
									else {
										$max = 10-$units_sold;
									}
									?>
						<div class="interestedEvent"><?php _e( 'Interested in this event? Purchase a ticket now.' , 'vini' ) ?></div>
						<div class="isSellableRight">
							<div class="purchase-ticket">
								<a rel="nofollow" href="<?php  echo $current_url; ?>/?add-to-cart=<?php echo $productWoocommerce; ?>" data-quantity="1" data-product_id="<?php echo $productWoocommerce;?>" data-product_sku="" data-maximum_tickets="<?php echo $max; ?>" class="button product_type_simple add_to_cart_button ajax_add_to_cart added" id="buttonAddTickets">Purchase Ticket</a>				
							</div>						
							<span class="isSellable" style="float:right">
							
								<div class="qty2">QTY</div>
								<div class="wrapper-button subtract">
									<button class="custom-substract-product">-</button>
								</div>
									<input id="quantityTickets" type="number" step="1" min="1" max="<?php echo $max; ?>" name="quantity[89]" value="0" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">					
									
								<div class="wrapper-button add">
									<button class="custom-add-product">+</button>
								</div>
							</div>
							
						</span>
					<?php else: ?>
							<span><?php _e( 'Tickets Sold Out' , 'vini' ) ?></span>					
					<?php endif; ?>

				</div>
				
		<?php endif; ?>
		<div class="entry-content">
			<p class="event-schedule">
				<span><?php _e( 'Event Date: ', 'vini' ) ?></span><?php echo $formattedDate; ?><br>
				<?php if (!empty($startTime) ) :?>
					<span><?php _e( 'Start Time: ', 'vini' ) ?></span><?php echo date('h:i A', strtotime($startTime)) ?>
				<?php endif; ?>

				<?php if (!empty($endTime) ) :?>
					|  <span><?php _e( 'End Time: ', 'vini' ) ?></span><?php echo date('h:i A', strtotime($endTime)) ?>
				<?php endif; ?>
			</p> 
			<?php the_content( '<span>read more</span>', TRUE ); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
	<?php get_sidebar(); ?>
</div>


