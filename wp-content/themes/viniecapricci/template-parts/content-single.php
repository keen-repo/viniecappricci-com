<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

	$images = rwmb_meta( 'mb_banner_post_images','type=image_advanced' );

	if ( !empty( $images)) {
		foreach ($images as $image) {
			echo '<div class="image" style="background-image: url(\''.wp_get_attachment_image_url( $image[0], 'post-thumbnail-large' ) .'\')"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
		}
	}else{
			echo '<div class="image"> <header class="entry-header"> <h1 class="entry-title">' . get_the_title() . '</h1></header></div>';
	}
	
	

?>
<div class="single-two-cols">
	<article id="post-<?php the_ID(); ?>" <?php post_class('news-entry'); ?>>
			<div class="entry-content">
				<?php the_content( '<span>read more</span>', TRUE ); ?>
			</div><!-- .entry-content -->
	</article><!-- #post-## -->
	<?php get_sidebar('news'); ?>
</div>

