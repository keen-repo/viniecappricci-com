<?php
/**
 * vini functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package vini
 */
DEFINE('TEMPL_DIR', get_template_directory_uri() );

if ( ! function_exists( 'vini_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vini_setup() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'vini' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support('woocommerce');

}
endif; // vini_setup
add_action( 'after_setup_theme', 'vini_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vini_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vini_content_width', 640 );
}
add_action( 'after_setup_theme', 'vini_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vini_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'vini' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Shop', 'vini' ),
		'id'            => 'sidebar-shop',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar News', 'vini' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	
	register_sidebar( array(
		'name'          => 'Home Main Widget Area',
		'id'            => 'home_main_widget',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer Left Area',
		'id'            => 'footer_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => 'Footer Right Area',
		'id'            => 'footer_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer 2 Left Area',
		'id'            => 'footer_2_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Footer 2 Right Area',
		'id'            => 'footer_2_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'vini_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vini_scripts() {

	
	wp_enqueue_style( 'vini-style', get_stylesheet_uri() );
	wp_enqueue_style( 'vini-css', TEMPL_DIR . '/assets/build/css/viniecapricci.css');
	wp_enqueue_style( 'vini-remodal', TEMPL_DIR . '/css/remodal.css');
	wp_enqueue_style( 'vini-remodal-theme', TEMPL_DIR . '/css/remodal-default-theme.css');
	wp_enqueue_style( 'vini-temp-css', TEMPL_DIR . '/css/temp.css');

	wp_enqueue_style( 'fccorecss', TEMPL_DIR . '/fullcalendar/core/main.css');
	wp_enqueue_style( 'fcdaygrid', TEMPL_DIR . '/fullcalendar/daygrid/main.css');

	// Event Calendar
	if ( is_home() || is_front_page() ) :
		wp_enqueue_script( 'fccore-js', TEMPL_DIR . '/fullcalendar/core/main.js', array(), false , true );
		wp_enqueue_script( 'fcday-js', TEMPL_DIR . '/fullcalendar/daygrid/main.js', array(), false , true );
		wp_enqueue_script( 'calendar-js', TEMPL_DIR . '/js/calendar.js', array('jquery'), false , true );
		wp_localize_script( 'calendar-js', 'calendar_ajax_object', 
			  	array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),) 
			  );
	endif;

	//Leaflet Map
	wp_enqueue_style( 'vjborg-leaflet-css', TEMPL_DIR . '/installs/leaflet/leaflet.css' );
	wp_enqueue_script( 'vjborg-leaflet-js', TEMPL_DIR . '/installs/leaflet/leaflet.js', false, '20190926', true );
	wp_enqueue_script( 'vjborg-leaflet-providers-js', TEMPL_DIR . '/installs/leaflet-providers/leaflet-providers.js', false, '20190926', true );

	wp_enqueue_script( 'plugin-js', TEMPL_DIR . '/assets/build/js/plugins.min.js', array('jquery'), false , true );
	wp_enqueue_script( 'main-js', TEMPL_DIR . '/assets/build/js/main.min.js', array('jquery','plugin-js'), false , true );
	wp_enqueue_script( 'cookie-js', TEMPL_DIR . '/js/js.cookie.js', array('jquery'), false , true );
	wp_enqueue_script( 'underscore' );
	wp_enqueue_script( 'remodal', TEMPL_DIR . '/js/remodal.min.js', array('jquery'), false , true );
	wp_enqueue_script( 'temp-js', TEMPL_DIR . '/js/temp.js', array('jquery'), false , true );

	
	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	//bootstrap
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/dist/bootstrap/js/bootstrap.min.js' );
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/dist/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'vini_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Theme Core Functionality.
 */
require get_template_directory() . '/inc/vini.php';

/**
 * Woocommerce Custom Functionality
 */
require get_template_directory() . '/inc/woocommerce.php';

add_filter( 'woocommerce_product_is_visible', 'filter_product_is_visible', 20, 2 );
function filter_product_is_visible( $is_visible, $product_id ){
    // HERE define your products IDs (or variation IDs) to be set as not visible in the array
    $targeted_ids = array(77831);

    if( in_array( $product_id, $targeted_ids ) )
        $is_visible = false;

    return $is_visible;
}

add_filter( 'woocommerce_cart_item_name', 'filter_cart_item_name', 20, 3 );
function filter_cart_item_name( $product_name, $cart_item, $cart_item_key ) {
    // HERE define your products IDs (or variation IDs) to be set as not visible in the array
    $targeted_ids = array(77831);

    if( in_array( $cart_item['data']->get_id(), $targeted_ids ) && is_cart() )
        return $cart_item['data']->get_name();

    return $product_name;
}

/**

* Add custom field to the checkout page

*/

add_action('woocommerce_after_order_notes', 'custom_checkout_field');

function custom_checkout_field($checkout){

echo '<div id="custom_checkout_field">';
woocommerce_form_field('rewards_loyalty_card_number', array(
	'type' => 'text',
	'class' => array(
	'my-field-class form-row-wide'
	) ,
	'label' => __('Rewards Loyalty Card Number') ,
	'placeholder' => __('') ,
	) ,
$checkout->get_value('rewards_loyalty_card_number'));

echo '</div>';

}

/**
 * Update the order meta with field value
 */