<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>
	<div class="wrapper">
		<div class="banner-slider">
			
		</div>
	</div>

	<div class="wrapper">
		<?php

		if (isset($_SESSION['hamper_mode']) && $_SESSION['hamper_mode'] === true) { 

		?>
		<div class="hamper_steps">
			<h3>Build your own <strong>Hamper</strong></h3>
			<div class="steps">
				<div class="small_step"><strong>Step 1.</strong> Select Your Box Size</div>
				<div class="small_step"><strong>Step 2.</strong> Add Items</div>
				<div class="large_step">
					<div class="step_text">Step</div>
					<div class="no">3</div>
					<div class="desc">Select your gift</div>
					<div class="clear"></div>
				</div>
				<div class="small_step last"><strong>Step 4.</strong> Add a message</div>
			</div>
			<img src="<?php echo get_template_directory_uri(); ?>/images/build_hamper_step_3.jpg" alt="" class="bottom_right" />
		</div>
		<?php

			foreach ($_SESSION['hampers'] as $key=>$hamper) { 
				
				$class = '';
				
				if ($key == $_SESSION['active_hamper']) {
					
					$class = 'active';
					
				}
		?>

				<div class="hamper_bar <?php echo $class; ?>" id="hamper_bar_<?php echo $key; ?>">
					<div class="how_fill">How filled is my hamper?</div> 
					<div class="progress">
						
						<?php 
						
						$bar_style = 'display: none;';
						$outside_style = '';
						if ($hamper['percent'] > 90) { 
							
							$bar_style = '';
							$outside_style = 'display: none;';
							
						}
						
						?>
						<div class="bar" style="width: <?php echo $hamper['percent']; ?>%;"><span style="<?php echo $bar_style; ?>"><?php echo $hamper['percent']; ?>%</span></div>
						<span class="outside" style="<?php echo $outside_style; ?>">Total Price:</span>
						<div class="clear"></div>
					</div>
					
					<?php if ($class == 'active') { ?>
						<a href="<?php echo site_url('/hamper/step-1/'); ?>">Upgrade Basket</a>
					<?php } else { ?>
						<a class="hamper_make_active" href="#hamper-<?php echo $key; ?>">Make Active</a>
					<?php } ?>
						<div class="clear"></div>
				</div>
		<?php
			}

		}
	?>
	</div>
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_before_main_content');
	?>
		<div class="archived-menus-columns">
			<ul>
			<?php
			$args = array(
				'orderby'    => 'id',
				'order'      => 'ASC',
				'hide_empty' => 'false',
				'include'    => 3361,
				'parent'	 => 0
			);

			$product_categories = get_terms( 'product_cat', $args );

			foreach( $product_categories as $cat ) {
				$term_id = $cat->term_id;
				$taxonomy_name = 'product_cat';
				$termchildren = get_term_children( $term_id, $taxonomy_name );

				echo "<li class='top-categ-menu".(empty($termchildren) ? " nochild" : "")."'>";
					echo "<div class='top-categ-link'>";
					echo "<a href='" . get_term_link( $cat->slug, 'product_cat') . "'>".$cat->name."</a>";
					echo "</div>";

						if(!empty($termchildren)){
							echo '<ul class="sub-categ-menu">';
							foreach ( $termchildren as $child ) {
								$term = get_term_by( 'id', $child, $taxonomy_name );
								//print_r($term);
								echo '<li><a href="' . get_term_link( $term->slug, $taxonomy_name ) . '">' . $term->name . '</a></li>';
							}
							echo '</ul>';
						}
				echo "</li>";
			}
			?>
			</ul>
		</div>
		<div class="archived-product-columns">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<!--h1 class="page-title"><?php woocommerce_page_title(); ?></h1-->

		<?php endif; ?>
		<?php do_action( 'woocommerce_archive_description' ); ?>
		<?php if ( have_posts() ) : ?>
		<div class="archived-product-options">
			<?php
				/**
				* woocommerce_before_shop_loop hook
				*
				* @hooked woocommerce_result_count - 20
				* @hooked woocommerce_catalog_ordering - 30
				*/
				do_action( 'woocommerce_before_shop_loop' );
			?>
		</div> <!-- .archived-product-options -->
			<?php woocommerce_product_loop_start(); ?>
				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>
		
				<?php if (isset($_SESSION['hamper_mode']) && $_SESSION['hamper_mode'] === true) {  ?>
					<a href="<?php echo site_url('/hamper/step-4/'); ?>" class="button">Next Step</a>
				<?php } ?>
			</div>
			<div class="clear"></div>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>
			


	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		// do_action('woocommerce_sidebar');
	?>

<?php get_footer('shop'); ?>