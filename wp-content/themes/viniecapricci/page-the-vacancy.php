<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vini
 */

get_header(); ?>

	<div id="primary" class="content-area section-vacancy">
		<main id="main" class="site-main" role="main">

			<section class="vacancies">
				<div class="wrapper-1400">

					<?php 
					// WP_Query arguments
					$args = array(
						'post_type'              => array( 'vacancy' ),
						'post_status'            => array( 'publish' ),
						'posts_per_page'         => '-1',
					);

					// The Query
					$query = new WP_Query( $args );
					?>
					<?php 
					if ( $query->have_posts() ) :  ?>
						<div class="box-vacancy clear">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<?php $info = rwmb_meta('mb_vacancy_description', 'type=text', get_the_ID()); ?>
							<div class="item">
								<div class="item-info">
									<p class="item-title">
										<?php echo get_the_title(); ?>
										<span><?php echo $info; ?></span>
									</p>
									<p class="item-content">
										<?php echo get_the_content(); ?>
									</p>
										<a href="#application-form" data-title="<?php echo $info .' '.get_the_title(); ?>"class="btn btn-secondary" target="_self"> Apply Now</a>
									</div>
								</div>

						<?php endwhile; ?>
						</div>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</section>

			<div id="application-form" class="clear">
				<div class="wrapper-1400">
					<h1 class="section-title">
						<?php _e('Thank you for your interest in this position', '_s'); ?>
					</h1>
					<?php echo do_shortcode('[formidable id=11 title=false description=true]'); ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
