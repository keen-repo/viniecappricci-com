<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         '3T3LWVYjXCod7GXWUNFunzD6JEvasdwRxCnRaBXYtXbJUUPIHx7HpDdzsBo17I3w');
define('SECURE_AUTH_KEY',  '9YBWKIqJLBXNItmLoIAJQiACWSP3MDnTcoNfgTgnrg6iQtfYafBUmcXV07mQHhgr');
define('LOGGED_IN_KEY',    '7nw3TH5VIqFft9y6S2NmwrsqYyyDdNsBT71qteNKPGBavnrXpvfPc3NYNLWT7TuV');
define('NONCE_KEY',        'GFBjjc7cesUaonmc39zXSzJcVtAh5qwapquJsCqYYi0JwpeKKjCL8T0qQHjURFyj');
define('AUTH_SALT',        'm9uUy3nJfae3LFsPrThRpRtVjdw4EFjJgcjT4riUijtrA9gjnGFshDoiYn7KFHpz');
define('SECURE_AUTH_SALT', 'E8xX00FnW0FspC4pIiwqvI9ooMLQRD3rXNioodvQPGaYpTV3CNMmSGpMqFeFX6b4');
define('LOGGED_IN_SALT',   'TWUdo2hop16qtCaYbnonC8QfirRdbH48XSjmxHKgvIjE4BBbfhDuMF4JGvy9ry4B');
define('NONCE_SALT',       'xppgcwBq0jFJvnohuMnxMG11cML6G33tgoQzuupmhraUpXstHcKqFDdWdrTegeE0');
